function p02b_epoch_clean_append(createWhichFiles, subj_id, ica, sss, ssp, outpath)

% preprocessing function
% reads in filtered continuous data, epochs data, adds trialinfo


%% init obob_ownft...
addpath('/mnt/obob/obob_ownft/');
obob_init_ft;

trialinfo_path = '/mnt/obob/staff/aweise/data/audifly/data/meg/trialinfo/sta_binaural/';
inpath_filt = '/mnt/obob/staff/aweise/data/audifly/data/meg/preproc/filtered';
artifact_path = '/mnt/obob/staff/aweise/data/audifly/data/meg/preproc/artifacts/';

%% testing the script? --> choose optionally!
doTest = 'no' % 'yes' or 'no'

if strcmp(doTest,'yes')
    clear all;
    trialinfo_path = '/mnt/obob/staff/aweise/data/audifly/data/meg/trialinfo/sta_binaural/';
    inpath_filt = '/mnt/obob/staff/aweise/data/audifly/data/meg/preproc/filtered';
    artifact_path = '/mnt/obob/staff/aweise/data/audifly/data/meg/preproc/artifacts/';

    ica = true; % false or true 
    ssp = false; % true or false
    sss = true; % false or true
    
    
    data_type = 'meg';
%     subj_ids = functions.get_subject_ids(data_type);
%     subj_id = subj_ids{1}; 
    subj_id = '19901106mrgb'; % '
    iFile =1
    fif_name = functions.get_fif_names( subj_id, iFile, sss);
    
    
    createWhichFiles = 'createAllFiles';
    if ica
        outpath = '/mnt/obob/staff/aweise/data/audifly/data/meg/preproc/clean_ica/';
    else
        outpath = '/mnt/obob/staff/aweise/data/audifly/data/meg/preproc/clean/';
    end
end

%% define variables
sound_trigger= [2,3,4,5,120,130,140,150]; % deviants and standards before deviants
sta_code = [120,130,140,150];
dev_code = [2,3,4,5];
n_iFiles = 7;

ica_path = ['/mnt/obob/staff/aweise/data/audifly/data/meg/preproc/ica/' subj_id '/'];

infile_trialinfo = [sprintf('%s_%s', subj_id, 'trialinfo.mat')]; %fullfile(inPath, [ subj '_' epoch '_centered.mat']);
load(fullfile(trialinfo_path, infile_trialinfo),  'trialinfo');


%%
for iFile =  1:n_iFiles
    
    if strcmp(subj_id, '19901106mrgb')
        if iFile == 4 % no maxfiltering possible for that file due to too many bad channels
            continue
        end
    end
    
    fif_name = functions.get_fif_names( subj_id, iFile, sss);
    
    
    % Select option which files to create if required
    %---------------------------------------
    if strcmp(createWhichFiles, 'createAllFiles')
        
        disp('create all files anew');
        
    elseif strcmp(createWhichFiles, 'createMissingFiles')
        
        disp('create only missing files');
        
        %  In case you need to run script again because cluster jobs were not successful
        %  run for each subj_id/condition but you want to keep files which were generated
        %  successfully (manually check date to not keep old ones based on a previous script that
        %  has been updated!)
        %--------------------------------------------------------------------------
        if sss== true
            finalFile = fullfile(outpath, [subj_id '_data_sound_clean_sss']);              
        elseif ssp
            finalFile = fullfile(outpath, [subj_id '_data_sound_clean_ssp']);
        else
            finalFile = fullfile(outpath, [subj_id '_data_sound_clean']);
        end
        
        checkFile = finalFile;
        if exist(checkFile)
            continue % if file exist already, continue with next file in the for loop
        end
        
    end
    
    %%
    % Load filtered data
    %----------------
    if sss
        infile_meg = sprintf('file_%02d_sss.mat', iFile);
    else
        infile_meg = sprintf('file_%02d.mat', iFile);
    end
    load(fullfile(inpath_filt, subj_id, infile_meg), 'data_filt');
    
    
    % Select only MEG channels
    %-----------------------
    cfg = [];
    cfg.channel = 'MEG';
    
    data_filt_meg = ft_selectdata(cfg, data_filt);
    
    if ica
        
        % Remove ica components
        %----------------------
        if sss
            
            load(fullfile(ica_path, ['file_0' num2str(iFile) '_sss' ]), 'ica_components');
            
            % load number of ica components of xiFile and xSubj to remove from the data
            load(fullfile(ica_path, ['file_0' num2str(iFile) '_sss_rej_comp']), 'rejected_components');
        
        
        else
            
            load(fullfile(ica_path, ['file_0' num2str(iFile)]), 'ica_components');
            
            % load number of ica components of xiFile and xSubj to remove from the data
            load(fullfile(ica_path, ['file_0' num2str(iFile) '_rej_comp']), 'rejected_components');
            
        end
        
        % reject ICA components
        cfg = [];
        cfg.component = rejected_components;
        
        data_ica_cleaned = ft_rejectcomponent(cfg, ica_components, data_filt_meg);
        
        data_preproc = data_ica_cleaned; clear data_ica_cleaned;
        
    else
        
        data_preproc = data_filt_meg; clear data_filt_meg;
        
        
    end
    
    clear data_filt;
    
    
    % Load MEG raw data / fif file
    %------------------------
    cfg = [];
    cfg.dataset = fif_name;
    cfg.trialdef.ntrials = 1;
    cfg.trialdef.triallength = Inf;
    cfg = ft_definetrial(cfg);
    
    cfg.channel = {'MEG' }; % read MEG channels minus all BIO channels (EOG and ECG) -->  'ECG*' 'EOG*'
    data = ft_preprocessing(cfg);
    
    
    % Define your trials based on trigger values
    %-----------------------------------------------------------------------
    cfg = [];
    cfg.dataset = fif_name;
    cfg.trialdef.eventtype = 'Trigger';
    cfg.trialdef.eventvalue =  sound_trigger;
    cfg.trialdef.prestim  = 2; % start of each trial = 1 sec before the trigger
    cfg.trialdef.poststim = 2; % end of each trial  = 2 sec after the trigger
    cfg_trials = ft_definetrial(cfg);
    
    %  Specify events to use by using the relevant samplepoint of the epoch onset of the relevant sound triggers
    %---------------------------------------------------------------------------------------------
    cfg_data = cfg_trials; % initilaze cfg to use further by copying relevant cfg-structure    
    
    % Make sure that the sampling points of your segments (specified above and stored in cfg_data.trl(:, 1:3)) match with the sampling points of the downsampled continuous filtered data
    % -------------------------------------------------------------------------------------------
    if mod(cfg_data.trl(1,2) - cfg_data.trl(1,1) , 2) == 1 % if sampling points of trial length is odd
        cfg_data.trl(:,2) = cfg_data.trl(:,2) + 1; % add sampling point so that trial length( i.e. difference between onset and end of trial) is even
        cfg_data.trl(:,1:3) = round(cfg_data.trl(:,1:3) / 4); % data were downsampled from 1000 to 250 Hz -> so sampling points should be devided by 4; it makes only sense to have integers, so do round the output!!!
        %         diff_test= diff(cfg_data.trl(:, 1:2)')';
    else
        cfg_data.trl(:,1:3) = round(cfg_data.trl(:,1:3) / 4); % data were downsampled from 1000 to 250 Hz -> so sampling points should be devided by 4; it makes only sense to have integers, so do round the output!!!
    end
    
    
    % Generate trialinfo for MEG data
    %---------------------------
    complete_trigger_info = (trialinfo{1,iFile}(:,:)); % stems from unfiltered MEG data, data were extracted and additional codes were added (i.e. code 666)
    
    
    sound_trial_indices = find(ismember(complete_trigger_info(:,5), [sound_trigger] )); % 666]));  % 666 is the code for a sound in a trial in which response was given before target and for which sound code is unknown
    disp(num2str(sound_trial_indices));
    if length(find(sound_trial_indices)) < 60
        error('check whether everything went well in deleting trials as less than 60 trials /64 left');
    end
    sound_trial_info = complete_trigger_info(sound_trial_indices,:);    
    
    if   ~isequal(sound_trial_info(:,5), cfg_data.trl(:,4))    % if there is a mismatch between trial/trigger numbers or trigger types in complete_trigger_info and behave
        
        if strcmp(subj_id, '19801021mrhl')
            if iFile == 1
                cfg_data.trl(1,:) = []; % not present in sound_trial_info
            end
            
        elseif strcmp(subj_id, '19920208dnkp')
            if iFile == 4
                cfg_data.trl(5,:) = []; % not present in sound_trial_info
                cfg_data.trl(5,:) = []; % not present in sound_trial_info
            elseif iFile == 7
                cfg_data.trl(64,:) = []; % not present in sound_trial_info
            end
            
            
        elseif strcmp(subj_id, '19830421rsur')
            if iFile == 1
                cfg_data.trl(1,:) = []; % not present in sound_trial_info
            end
            
        
            
        elseif strcmp(subj_id, '19901106mrgb')
            if iFile == 1
                cfg_data.trl(64,:) = []; % not present in sound_trial_info
            end
        end
        
        
        
        ind_dev_for_cfg = find(ismember(sound_trial_info(:,5), dev_code)); % find dev codes: 2,3,4,5
        rel_ind_dev_for_cfg = ind_dev_for_cfg(diff(ind_dev_for_cfg) ==1);
        ind_dev_to_delete = rel_ind_dev_for_cfg +1;
        
        ind_sta_for_cfg = find(ismember(sound_trial_info(:,5), sta_code)); % find sta codes: 120, 130, 140, 150
        rel_ind_sta_for_cfg = ind_sta_for_cfg(diff(ind_sta_for_cfg) ==1);
        ind_sta_to_delete = rel_ind_sta_for_cfg +1;
        
        ind_for_cfg_to_delete = [ind_dev_to_delete' ind_sta_to_delete' ];
        ind_for_cfg_to_delete = sort( ind_for_cfg_to_delete );
        
        for i = 1:length(ind_for_cfg_to_delete)
            cfg_data.trl( ind_for_cfg_to_delete(i),:) = [];
        end
        
        % after doing adjustments, re-check whether infos are identical and
        % give an error, if this is not the case!
        if   ~isequal(sound_trial_info(:,5), cfg_data.trl(:,4))    % if there is a mismatch between trial/trigger numbers or trigger types in complete_trigger_info and behave
            error('mismatch between sound trigger in trialinfo and sound trigger in cfg trl structure after adjusting the trigger info')
        else
            disp('GREAT! sound trigger in trialinfo and sound trigger in cfg trl structure match after adjusting trigger info');
        end
    end
    

    % to save and analyse only valid trials for meg data analysis (i.e. for which behavioral performance was correct
    % and in the pre-defined response window and no double button presses and no button press before the target!)
    % we now exlude those trials from the trial_matrix (before this stage they are collected for control purposes)
    rel_trial_indices = find(sound_trial_info(:,12) > 100 & sound_trial_info(:,12) < 800);
    
    sound_trial_info =  sound_trial_info(rel_trial_indices,:);
    
    cfg_data.trl = cfg_data.trl(rel_trial_indices,:);
    
    trl_info_final =  cfg_data.trl;
    if iFile == 1
        trial_indices = rel_trial_indices; % create trial indices with respect to the whole stimulation
    else
        trial_indices = ((iFile-1) * 160) + rel_trial_indices; % create trial indices with respect to the whole stimulation
    end
    
    trl_info_final =  [trl_info_final, trial_indices, sound_trial_info];
    
    if ~isequal(sound_trial_info(:,5), cfg_data.trl(:,4))
        error('');
    else
        disp('sound_trial_info(:,5) and cfg_data.trl(:,4) matches. all good :)!');
    end

        
   
    
    % Re-define trials / epoch of filtered data
    % ---------------------------
    data_epoch = ft_redefinetrial(cfg_data, data_preproc); % data are redefined by using the specified segments
    
    data_epoch.trialinfo = trl_info_final;
    data_epoch.trialinfo(:,1:3) = round(trl_info_final(:,1:3) /4); 
    data_epoch.trialinfo(:,9) = round(trl_info_final(:,9) /4); % in column 9 are sample points of sound onset; devide by 4 because data were downsampled from 1000 to 250 Hz
    data_epoch.trialinfo(:,19) = round(trl_info_final(:,19) /4); % in column 14 are sample points of target onset; devide by 4 because data were downsampled from 1000 to 250 Hz
    

    
    %% select smaller epochs for sound epochs and target epochs (to loose less data during cleaning)
    cfg = [];
    cfg.toilim    = [-1.5 1.5] ;
    
    data_sound_sel = ft_redefinetrial(cfg, data_epoch);
    
    
    % for all other participants for whom data cleaning was done in one step for gradiometers and magnetometers
    
    % Reject bad trials with artifacts based on previous detection
    % procedure for MEGMAGs and MEGGRADs
    %---------------------------------------------------------------------
    % Load previous artifact detection structure of MAGs and GRADs
    %---------------------------------------------------------------------
    if sss
        filename_artifacts = [sprintf('file0%d_%s', iFile, 'mag_grad_sss.mat')];
    else
        filename_artifacts = [sprintf('file0%d_%s', iFile, 'mag_grad.mat')];
    end
    load(fullfile(artifact_path, subj_id, filename_artifacts),  'all_art', 'bad_channels');
    
    % Remove bad MAG and GRAD channels
    %------------------------------------------
    channel_select  = functions.make_channel_reject( bad_channels );
    
    cfg = [];
    if sss == true
        cfg.channel = 'all'; % select all channels for maxfiltered-data incl. bad channels as they have (automatically) been interpolated!
    elseif sss == false % select all but the bad channels when data are non-maxfiltered!
        cfg.channel = channel_select;
    end
    
    sound_cleaned_mag_grad = ft_selectdata(cfg, data_sound_sel);
    
    % Remove bad trials  based on MAGs and GRADs
    %-----------------------------------------
    % NOTE all_art origins from downsampled filtered data (i.e. no need for
    % adjusting sampling points for artifacts!)
    cfg_art.artfctdef.reject = 'complete'; % Reject the complete trial if there is any artifact period inside this trial
    
    sound_cleaned_art = ft_rejectartifact(all_art, sound_cleaned_mag_grad);
    
    sound_cleaned{iFile} = ft_rejectartifact(all_art, sound_cleaned_art);
    sound_cleaned{iFile} = rmfield(sound_cleaned{iFile},'sampleinfo');  % delete sampleinfo field otherwise appenddata won't work properly as it seems to concatenate pieces of different epochs!
    
    if ssp
        
        cfg = [];
        cfg.inputfile =  fif_name;
        
        sound_cleaned{iFile}  = obob_apply_ssp(cfg, sound_cleaned{iFile} );
        
    end
    
    clear  'all_art'  'bad_channels';
    
    
end % iFile loop

if strcmp(subj_id,  '19901106mrgb')
    sound_cleaned(4)=[];
end

%% append data of different iFiles
cfg = [];
cfg.appenddim = 'rpt'; % to append epochs from different blocks

data_sound = ft_appenddata(cfg, sound_cleaned{:}); % (original) data with sound onset at time zero

% note that in case of different head positions the grad field gets lost in
% ft_appenddata (what is usually wanted); however, we re-store the grad
% structure of the first file in the appended data
grad = sound_cleaned{1} .grad;
data_sound.grad = grad;

%% create folder in which data will be stored if not existent
if ~exist(outpath, 'dir')
    mkdir(outpath)
end

if sss
    save(fullfile(outpath, [subj_id '_data_sound_clean_sss']), 'data_sound'); % , '-v7.3');
elseif ssp
    save(fullfile(outpath, [subj_id '_data_sound_clean_ssp']), 'data_sound'); % , '-v7.3');
else
    save(fullfile(outpath, [subj_id '_data_sound_clean']), 'data_sound'); % , '-v7.3');
end

%% check data again
runTest = 0;
if runTest == 1
    
%     load(['/mnt/obob/staff/aweise/data/audifly/data/meg/preproc/clean/' '19750206hlbn_data_sound_clean']);
    cfg = [];
    cfg.layout = 'neuromag306mag.lay';
    cfg.channel = 'MEGMAG'; % use all channels in combination with the appropriate scaling factor for magnetometers
%     cfg.magscale = 25; % scale magnetometers so that gradiometers and magnetometers are on similar scales
    cfg.gradscale = 0.04; % you can use this instead of cfg.magscale = 25;
    cfg.continuous = 'no'; % data are trial-based
    cfg.viewmode = 'butterfly'; % 'butterfly', 'vertical', 'component';
    cfg.plotevents = 'yes'; % 'no' or 'yes', whether to plot event markers. (default is 'yes')
    %     cfg.preproc.lpfilter = 'yes';
    %     cfg.preproc.lpfreq = 35;
    %     cfg.preproc.hpfilter = 'yes';
    %     cfg.preproc.hpfreq = 1;
    
    ft_databrowser(cfg, data_sound); % (1) data_lp , (2) data_resample, (3) data_hp
    
    cfg.continuous = 'no';
    cfg.plotevents = 'yes'; % 'no' or 'yes'
    ft_databrowser(cfg, data); % (4) data_alltrl, (5) data_epoch / or data
    
end

