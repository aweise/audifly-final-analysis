function s02_subjLeadfield(createWhichFiles, subj_id, ica, sss, chooseNormalizeOption, chooseSensors, chooseBrainOption, epoch)
   
% (C) Annekathrin Weise


%% init obob_ownft...
addpath('/mnt/obob/obob_ownft/');
cfg.package.svs = true;
cfg.package.gm2 = true; 
obob_init_ft (cfg);

addpath('/mnt/obob/staff/aweise/data/audifly/scripts/');
addpath('/mnt/obob/staff/aweise/templates/');
    
%% +++++++++++++++++++++   for testing only   ++++++++++++++++++++++++++
runTest = 'no'; % 'yes' or 'no'

if strcmp(runTest, 'yes')
    createWhichFiles = 'createAllFiles'; % 'createMissingFiles'
    ica = true, % true, false
    sss = true; % false or true
    chooseSensors = 'meggrad' % 'meggrad'
    chooseBrainOption =   'parcel' % 'parcel' or 'whole' or 'whole_grey'
    chooseNormalizeOption = 'no' % 'no' or 'yes' when interested in mvpa
    
    data_type = 'meg';
    subj_ids = functions.get_subject_ids(data_type);    
    subj_id = subj_ids{1}; 
    
    epoch = 'sound'  %   or 'target'  
    
end
% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

%% define paths
if ica
    inpath = '/mnt/obob/staff/aweise/data/audifly/data/meg/preproc/clean_ica/';
end
inPathGrid = ['/mnt/obob/staff/aweise/data/audifly/data/meg/source/' chooseBrainOption '/sourcemodel/' ];
coreg_path = '/mnt/obob/staff/aweise/data/audifly/data/coregistration/';
outpath = ['/mnt/obob/staff/aweise/data/audifly/data/meg/source/' chooseBrainOption '/leadfield/norm_' chooseNormalizeOption '/' chooseSensors '/'];
  
 %% load preprocessed MEG data
 if sss
     inputfile = fullfile(inpath, [ subj_id '_data_' epoch '_clean_sss.mat']);
 end
 
%% load input file
 load(inputfile);
 
 if strcmp(epoch,'sound')
     data = data_sound;
 end

%% define type of template grid
if strcmp(chooseBrainOption, 'parcel')    
    chooseGridType = 'grid_3mm_parcel';        
end

%% select option which files to create
if strcmp(createWhichFiles, 'createAllFiles')
    
    disp('create all files anew');
    
elseif strcmp(createWhichFiles, 'createMissingFiles')
    
    disp('create only missing files anew');
    
    %% in case you need to run script again because cluster jobs were successfully not for all but only for some files
    %  run for each subj/condition but you want to keep files which were generated
    %  successfully (manually check date to not keep old ones based on a previous script that
    %  has been updated!)
    
    if ica
        if sss
            checkFile = [outpath subj_id '_' epoch '_leadfield_' chooseGridType  '_sss_ica.mat'];
        end    
    end
    if exist(checkFile)
        return % if file exist already, do not run script
    end
    
end

%% load individual subject grid of predefined resolution
load(fullfile(inPathGrid, [subj_id '_' chooseGridType  '.mat']), 'subject_grid');

%% load subj specific headmodel / volume
load(fullfile(coreg_path, ['coregister_' subj_id]), 'hdm');

%% create leadfield
% computes the forward model for many dipole locations on a regular 2D or 3D grid
cfg = [];
cfg.grid = subject_grid;
cfg.vol = hdm; % subject specific headmodel /volume
if strcmp(chooseSensors, 'all_sensors')
    cfg.channel = 'MEG';
else
    cfg.channel = chooseSensors; % chooseSensors = 'meggrad' 
end

cfg.normalize = chooseNormalizeOption; % 'yes' or 'no'

subject_leadfield = ft_prepare_leadfield(cfg, data); 
  

%% create folder in which data will be stored if not existent
if ~exist(outpath, 'dir')
    mkdir(outpath)
end

if ica
    
    if sss
        save([outpath subj_id '_' epoch '_leadfield_' chooseGridType  '_sss_ica.mat'], 'subject_leadfield', '-v7.3');
    end    

end
