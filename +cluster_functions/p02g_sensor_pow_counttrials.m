function p02g_sensor_pow_counttrials(data_type, sss)

% (C) Annekathrin Weise

%% init obob_ownft...
addpath('/mnt/obob/obob_ownft/');
obob_init_ft;

addpath('/mnt/obob/staff/aweise/data/audifly/scripts/');

epoch = 'sound'; % 'target' or 'sound'
choose_sensors = 'all_sensors';

%% +++++++++++++++++++++   for testing only   ++++++++++++++++++++++++++
runTest = 'no'; % 'yes' or 'no'

if strcmp(runTest, 'yes')
    
    data_type = 'meg';
    
    sss = true;
    
    
end
% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

inpath = '/mnt/obob/staff/aweise/data/audifly/data/meg/preproc/clean_ica/';

outpath = '/mnt/obob/staff/aweise/data/audifly/data/meg/sensor/trial_count/';
if ~exist(outpath, 'dir')
    mkdir(outpath)
end

xConditions = {'dev_left', 'dev_right'};

subj_ids = functions.get_subject_ids(data_type);

for iSubj = 1:length(subj_ids)    
    
    subj_id = subj_ids{iSubj };
    subj_code = ['id_' subj_id];
    
    %% load preprocessed MEG data
    if sss
        inputfile = fullfile(inpath, [subj_id '_data_' epoch '_clean_sss']);
    end
    load(inputfile);
    
    if strcmp(epoch,'sound')
        data = data_sound;   
    end
    
    %% select the data of interest
    cfg = [];
    if strcmp(choose_sensors,'all_sensors')
        cfg.channel = 'MEG';
    else
        cfg.channel = choose_sensors; % irrelevant option on sensor level!
    end
    
    data_sel = ft_selectdata(cfg, data);
    
          
    %% get trials of all relevant conditions used for the analysis
           
    for iCond = 1:length( xConditions)
        cond_name = xConditions{iCond};
        cond_codes = functions.get_cond_codes(cond_name,epoch);
        
        %% get relevant trial indices
        
        ind_rel_trials.(subj_code).(cond_name) = find(ismember(data_sel.trialinfo(:,4),cond_codes));
        count_rel_trials.(subj_code).(cond_name) = length( ind_rel_trials.(subj_code).(cond_name) );
        
        count_rel_trials_calc.(cond_name)(:,iSubj)  = count_rel_trials.(subj_code).(cond_name);
                
    end%cond
    
    for iCond = 1:length( xConditions)
        cond_name = xConditions{iCond};
        mean_count_all_conds.(cond_name) = mean(count_rel_trials_calc.(cond_name));
        std_count_all_conds.(cond_name) = std(count_rel_trials_calc.(cond_name));
    end
    
end% subj

%% save individual trial numbers for both condition
if sss
    outputfile = fullfile(outpath, ['count_trialsN' num2str(length(subj_ids)) '_' epoch '_allConds_sss.mat'] );
else
    outputfile = fullfile(outpath, ['count_trialsN' num2str(length(subj_ids)) '_' epoch '_allConds.mat'] );
end
save(outputfile,  'ind_rel_trials', 'count_rel_trials', 'count_rel_trials_calc', 'mean_count_all_conds', 'std_count_all_conds');






