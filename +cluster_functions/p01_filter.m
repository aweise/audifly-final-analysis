function p01_filter(subj_id, block, create_which_files, sss, outpath)


%% init obob_ownft...
addpath('/mnt/obob/obob_ownft/');
obob_init_ft;

addpath('/mnt/obob/staff/aweise/data/audifly/scripts/');

%% for testing the script --> choose optionally!
doTest = 'no' % 'yes' or 'no'

if strcmp(doTest,'yes')
    
    block = 1;
    
    outpath = '/mnt/obob/staff/aweise/data/audifly/data/meg/preproc/filtered/'
    
    sss=true ; % false or true
    
    data_type = 'meg';
    subj_ids = functions.get_subject_ids(data_type);    
    subj_id = subj_ids{1};
    
    create_which_files = 'createAllFiles'; % 'createMissingFiles';
    
end

fif_name = functions.get_fif_names( subj_id, block, sss ) ;

%% select option which files to create
if strcmp(create_which_files, 'createAllFiles')
    
    disp('create all files anew');
    
elseif strcmp(create_which_files, 'createMissingFiles')
    
    disp('create only missing files');
    
    %% in case you need to run script again because cluster jobs were not successful
    %  run for each subj/block but you want to keep files which were generated
    %  successfully (manually check date to not keep old ones based on a previous script that
    %  has been updated!)
    outpath_spec = [outpath  subj_id '/']
    
    if sss
        final_file = fullfile(outpath_spec, ['file_0' num2str(block) '_sss.mat']);
    else
        final_file = fullfile(outpath_spec, ['file_0' num2str(block) '.mat']);
    end
    
    check_file = final_file;
    if exist(check_file)
        error('file already exists. no need for further processing otherwise file will be overwritten.') 
    end
    
end


%% Lowpass-filter continuous data (.fif)
%     Lowpass filtering data: onepass-zerophase, order 3624, kaiser-windowed sinc FIR
%     cutoff (-6 dB) 35 Hz
%     transition width 1.0 Hz, passband 0-34.5 Hz, stopband 35.5-500 Hz
%     max. passband deviation 0.0010 (0.10%), stopband attenuation -60 dB

%         Filter Type: lowpass
%         Window Type: Kaiser
%         Kaiser window beta – Estimate: Max passband deviation/ ripple: 0.001
%         Filter order – Estimate – Transition bandwidth: maximal das Doppelte vom highpass; Beachte: cut off frequency minus transition band width/2 darf nicht kleiner Null sein!!!!!!! (punkt rechnung vor strichrechnung ;)) -> d.h. für high pass filter von 0.1 Hz: 0.1 – 0.2 / 2
%
%         Filter order: transition band width ist nur bei Highpss wichtig!!! beim lowpass kann man die transition band width frei wählen (jedoch nicht zu groß), z.B. transition band width von 1

%     note: The cutoff frequency is passband edge +/- half the transition bandwidth.

% hdr = ft_read_header(fif_name); % for testing only
% data_test = ft_read_data(fif_name); % for testing only

cfg = [];
cfg.dataset = fif_name;
cfg.trialdef.triallength = Inf; % this is the trick: infinite trial length...
cfg.trialdef.ntrials = 1;       % ...for one trial!

cfg = ft_definetrial(cfg); % do definetrial for continuous data

cfg.channel = {'MEG'  'ECG*' 'EOG*'}; % read MEG channels + all BIO channels (EOG and ECG)
cfg.lpfilter = 'yes';
cfg.lpfreq = 35; % we are interested in low frequencies!; muscle artifacts > 40 Hz; line noise 50 Hz
cfg.lpfilttype = 'firws'; % kaiser-windowed sinc FIR
cfg.lpfiltdf = 1; % lowpass transition width
cfg.lpfiltwintype = 'kaiser';

data_lp = ft_preprocessing(cfg); % filter continuous data

%% convert grad field from cm (as stored in raw data) to m
data_lp.grad = ft_convert_units(data_lp.grad, 'm'); % use m!

%% downsample continuous, low-pass filtered data
% make sure to lowpass filter your data sufficiently before applying
% downsampling!
cfg = [];
cfg.resamplefs = 250;
cfg.detrend = 'no';
cfg.demean = 'no';
cfg.feedback = 'no';
cfg.resamplemethod = 'downsample'; %  using cfg.resamplemethod = 'downsample', only use this if you have applied an anti-aliasing filter
%  prior to downsampling!
data_resample = ft_resampledata(cfg, data_lp);

%%   apply hp-filter
%     Highpass filtering data: onepass-zerophase, order 9056, kaiser-windowed sinc FIR
%     cutoff (-6 dB) 0.1 Hz
%     transition width 0.1 Hz, stopband 0-0.1 Hz, passband 0.2-125 Hz
%     max. passband deviation 0.0010 (0.10%), stopband attenuation -60 dB
cfg = [];
cfg.hpfilter = 'yes';
cfg.hpfreq = 0.1;  % note most books recommend shorter than longer filters (so, this also speaks for first lp-filter data, downsampling and than applying hp as its length decreases with that procedure; filter length = filter order + 1; if not downsampling data: 0.1-hp filter here would be ~33222!!!)
cfg.hpfilttype = 'firws'; % kaiser-windowed sinc FIR
cfg.hpfiltdf = 0.1;
cfg.hpfiltwintype = 'kaiser';
%     cfg.plotfiltresp  = 'yes';

data_hp = ft_preprocessing(cfg, data_resample); % filter continuous data

%% create folder in which data will be stored if not existent
outpath_spec = [outpath '/' subj_id '/']

if ~exist(outpath_spec, 'dir')
    mkdir(outpath_spec)
end

data_filt = data_hp;

% make sure to save files in the correct format to not later end up
% with problems in sorting via dir (e.g. file 1, file 10, file 11, file 12, file 2, file 3, ...)
if sss
    save(fullfile(outpath_spec, ['file_0' num2str(block) '_sss']), 'data_filt', 'subj_id', '-v7.3');
else
    save(fullfile(outpath_spec, ['file_0' num2str(block)]), 'data_filt', 'subj_id', '-v7.3');
end



%% check data again
runTest = 0;
if runTest == 1
    
    cfg = [];
%     cfg.ylim = [-1e-9 1e-9 ];
    cfg.blocksize = 30; % show data of 30 sec
    cfg.layout = 'neuromag306mag.lay'; % shows the topo of only the magnetometers
    cfg.channel = 'MEG'; % use all channels in combination with the appropriate scaling factor for magnetometers
    cfg.continuous = 'yes';
    cfg.viewmode = 'vertical'; %'butterfly';
    cfg.megscale = 1e1;
    cfg.eogscale = 1e-7;
    cfg.ecgscale = 1e-8;
    cfg.gradscale = 0.04;
    cfg.plotevents  = 'no';
    cfg.showlabels = 'yes';
    ft_databrowser(cfg, data_filt); % (1) data_lp , (2) data_resample, (3) data_hp,
    
    
    cfg.continuous = 'no';
    cfg.plotevents = 'no'; % 'no' or 'yes'
    ft_databrowser(cfg, data_epoch); % (4) data_alltrl, (5) data_epoch / or data
    
end

end