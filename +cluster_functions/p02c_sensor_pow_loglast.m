function p02c_sensor_pow_loglast(subj_id, createWhichFiles,  sss,  chooseTFmethod, chooseCycle, choose_sensors,  epoch, choose_sglTrl, logtransform)

% (C) Annekathrin Weise

%% Steps to be performed in the script to analyse pow in sensor space
% load preprocessed MEG data (incl. ica cleaning)
% do time frequency analysis


%% init obob_ownft...
addpath('/mnt/obob/obob_ownft/');
obob_init_ft;

addpath('/mnt/obob/staff/aweise/data/audifly/scripts/');


%% +++++++++++++++++++++   for testing only   ++++++++++++++++++++++++++
runTest = 'no'; % 'yes' or 'no'

if strcmp(runTest, 'yes')
    logtransform = 'yes';
    
    choose_sglTrl = 'no'
    
    createWhichFiles = 'createAllFiles';
    
    chooseCycle = 5;
    
    choose_sensors =  'all_sensors'; % 'all_sensors' or  'meggrad'
    
    chooseTFmethod = 'wavelet'; % 'hanningTaper' or 'wavelet'
    
    data_type = 'meg';
    subj_ids = functions.get_subject_ids(data_type);
    subj_id = subj_ids{1};
    
    epoch = 'sound'; % 'target' or 'sound'
    
    sss = true;
end
% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
if strcmp(choose_sglTrl, 'yes')
    if strcmp(logtransform,'yes')
        outpath_pow = ['/mnt/obob/staff/aweise/data/audifly/data/meg/sensor/pow/' choose_sensors '/' chooseTFmethod '/' num2str(chooseCycle) 'cycles/logtrans_' logtransform  '/sglTrl/'   ]; 
    else
        outpath_pow = ['/mnt/obob/staff/aweise/data/audifly/data/meg/sensor/pow/' choose_sensors '/' chooseTFmethod '/sglTrl/'   ];
    end
elseif strcmp(choose_sglTrl, 'no')
    outpath_pow = ['/mnt/obob/staff/aweise/data/audifly/data/meg/sensor/pow/' choose_sensors '/' chooseTFmethod '/' num2str(chooseCycle) 'cycles/logtrans_' logtransform '/loglast/' ];
end

inpath = '/mnt/obob/staff/aweise/data/audifly/data/meg/preproc/clean_ica/';

%% select option which files to create
if strcmp(createWhichFiles, 'createAllFiles')
    
    disp('create all files anew');
    
elseif strcmp(createWhichFiles, 'createMissingFiles')
    
    disp('create only missing files anew');
    
    %% in case you need to run script again because cluster jobs were successfully not for all but only for some files
    %  run for each subj/condition but you want to keep files which were generated
    %  successfully (manually check date to not keep old ones based on a previous script that
    %  has been updated!)    
     
    if sss
        checkFile = fullfile(outpath_pow, [subj_id '_' epoch '_' xCondition '_sss.mat'] );
        
    else
        checkFile = fullfile(outpath_pow, [subj_id '_' epoch '_' xCondition '.mat'] );
    end
    if exist(checkFile)
        return % if file exist already, do not run script
    end
    
end


%% load preprocessed MEG data
if sss
    inputfile = fullfile(inpath, [subj_id '_data_' epoch '_clean_sss']);
else
    inputfile = fullfile(inpath, [subj_id '_data_' epoch '_clean']);
end
load(inputfile);

if strcmp(epoch,'sound')
    data = data_sound;
end

%% select smaller data set for testing only
if strcmp(runTest, 'yes')
    cfg = [];
    cfg.trials = 1:100;
    data = ft_selectdata(cfg,data);
end


%% select the data of interest
cfg = [];
if strcmp(choose_sensors,'all_sensors')
    cfg.channel = 'MEG';
else
    cfg.channel = choose_sensors; % irrelevant option on sensor level!
end

data_sel = ft_selectdata(cfg, data);

%% interpolate missing channels / channel repair
if sss ~= true
    
    load Cimec_MEG_neigh.mat; % load default neighbours - necessary for interpolating / repairing missing channels
    
    
    cfg = [];
    cfg.neighbours     = [neighbGradY neighbGradX neighbMag];
    cfg.missingchannel = setdiff(data_sel.grad.label, data_sel.label); % find missing channels
    data_sel = ft_channelrepair(cfg, data_sel);
    data_sel.misschan = cfg.missingchannel;
    
end

%% do the spectral analysis for the current stimulus type - but only for epoch type 'sound'
if strcmp(epoch, 'sound')
    
    %% detrend data to remove low frequency drift
    cfg = [];
    cfg.detrend = 'yes';
    
    data_detrend= ft_preprocessing(cfg, data_sel);
    
    
    %% get relevant conditions
    conds =  {'dev_left', 'dev_right'};
    
    for iCondition = 1:length(conds)
        
        xCondition = conds{iCondition};
        
        cond_codes = functions.get_cond_codes(xCondition,epoch);
        
                
        %% get relevant trial indices                       
        ind_rel_trials = find(ismember(data_detrend.trialinfo(:,4),cond_codes));
        
        % Increase sensitivity to non-phase locked signals
        
        % One of the purposes of TFR analysis is to reveal signals that are non-phase locked to stimuli, in contrast to the analysis of
        % evoked fields/potentials. But the phase-locked signals also show up in the TFRs, as they are present in the signal and therefore
        % decomposed by the TF analysis. To enhance the sensitivity to the non-phase locked activity, we can subtract the phase locked activity
        % and redo the analysis.
        % The phase-locked activity is the evoked field/potential. Assuming the average evoked signal are a good representation of the
        % phase-locked signal (i.e., sufficient signal-to-noise ratio), we can subtract it from each trial, and redo the TFR analysis.
        
        % Get time-locked
        cfg = [];
        cfg.trials = ind_rel_trials;
        data_timelock = ft_timelockanalysis(cfg, data_detrend);
      
        data_minus_timelock = data_detrend;
        for i = 1:length(data_detrend.trial)
            data_minus_timelock.trial{i} = data_detrend.trial{i} - data_timelock.avg;
        end

       if strcmp(chooseTFmethod, 'wavelet')
            
            % TFR with Morlet wavelets
            cfg = [];
            cfg.method = 'wavelet';
            cfg.trials = ind_rel_trials; % relevant trials of the current stimuluy type; default: all
            cfg.foi         = 1:1:30;       % Frequencies we want to estimate from 1 Hz to 30 Hz in steps of 1HZ
            cfg.toi         =  -1.5 : 0.05 : 1.5; % Time bins from the beginning to the end of the respective epoch
            cfg.width    = chooseCycle;
            cfg.output =  'pow'; % return the power-spectra
           
            if strcmp(choose_sglTrl, 'yes')
                cfg.keeptrials = 'yes';
            elseif strcmp(choose_sglTrl, 'no')
                cfg.keeptrials = 'no'; % default: no; ''fourier'' requires cfg.keeptrials = ''yes'' and cfg.keeptapers = ''yes'''
                %   cfg.keeptrials = 'yes' or 'no', return individual trials or average (default = 'no')
                % keep trials also relevant when doing e.g. correlation with behavior
            end
            
            if strcmp(logtransform,'yes')
                cfg.keeptrials = 'yes';
            end
            
        end
        
        freq = ft_freqanalysis(cfg, data_detrend);        
        freq.cfg = []; % remove cfg field in freq
        
        freq_minus_timelock = ft_freqanalysis(cfg, data_minus_timelock);
        freq_minus_timelock.cfg = [];
        
        cfg.trials  = 'all';       
        freq_timelock = ft_freqanalysis(cfg, data_timelock);
        freq_timelock.cfg = [];
        
        %% combine data of gradiometer
        % computes the planar gradient magnitude over both directions
        % combining the two gradients at each sensor to a single positive-valued number.
        % This can be done for averaged planar gradient single-trial TFR (i.e. powerspectra).
        
        % note: recording MEG data is done via magnetometer and gradiometer. these different sensor types yield totally different things.
        % note on gradiometer: 2 per position, which point in opposite directions, e.g. one to the front and one to the side.
        % to make sense out of the two values one has to combine the information of the two gradiometers
        % by using ft_combineplanar without additional options  ft_combineplanar([], freq)
        freq  = ft_combineplanar([], freq);
        freq_minus_timelock  = ft_combineplanar([], freq_minus_timelock);
        freq_timelock  = ft_combineplanar([], freq_timelock);
        
        
        %% logtransoform data if option is set to 'yes'
        if strcmp(logtransform,'yes')    
                
%             Log-transform the single-trial power
%             Spectral power is not normally distributed. Although this is in theory not a problem 
%             for the non-parametric statistical test, its sensitivity is usually increased by
%             log-transforming the values in the power spectrum.
            
            
            cfg           = [];
            cfg.parameter = 'powspctrm';
            cfg.operation = 'log10';
            
            freq_logpow    = ft_math(cfg, freq);
            freq_timelock_logpow = ft_math(cfg, freq_timelock);
            freq_minus_timelock_logpow = ft_math(cfg, freq_minus_timelock);
            
            if strcmp(choose_sglTrl, 'no')
                
                % get rid of single trials to save storage space
                cfg = [];
                cfg.trials = 'all';
                cfg.avgoverrpt = 'yes';
                
                freq_logpow = ft_selectdata(cfg,freq_logpow);
                freq_minus_timelock_logpow = ft_selectdata(cfg,freq_minus_timelock_logpow);
                freq_timelock_logpow = ft_selectdata(cfg,freq_timelock_logpow);                
                
            end
        end
        
        
       
       
        %% create folder in which data will be stored if not existent
        if ~exist(outpath_pow, 'dir')
            mkdir(outpath_pow)
        end
        
        %% save individual power data for each condition     
        if sss
              outputfile = fullfile(outpath_pow, [subj_id '_' epoch '_' xCondition  '_sss.mat'] );
        else
            outputfile = fullfile(outpath_pow, [subj_id '_' epoch '_' xCondition  '.mat'] );
        end
        
        if strcmp(logtransform,'yes')
            save(outputfile, 'freq_logpow', 'freq_minus_timelock_logpow','freq_timelock_logpow');%'-v7.3');
            clear xCondition cond_codes ind_rel_trials freq_logpow freq_minus_timelock_logpow data_timelock  data_minus_timelock 'freq_timelock_logpow'
        else
            save(outputfile, 'freq', 'freq_minus_timelock','freq_timelock');%'-v7.3');
            clear xCondition cond_codes ind_rel_trials freq freq_minus_timelock data_timelock  data_minus_timelock 'freq_timelock'
        end
        
    end % cond loop
end % if epoch

end

