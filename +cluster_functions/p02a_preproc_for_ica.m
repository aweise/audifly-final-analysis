function p02a_preproc_for_ica(create_which_files, subj_id, block, sss, ssp, outpath)

% this function is used for preprocessing the raw data for applying ica


%% init obob_ownft...
addpath('/mnt/obob/obob_ownft/');
obob_init_ft;

addpath('/mnt/obob/staff/aweise/data/audifly/scripts/');

artifact_path = '/mnt/obob/staff/aweise/data/audifly/data/meg/preproc/artifacts/';


%% for testing the script --> choose optionally!
doTest = 'no' % 'yes' or 'no'

if strcmp(doTest,'yes')
    
    block = 1;
    
    outpath = '/mnt/obob/staff/aweise/data/audifly/data/meg/preproc/ica/'
    
    sss=true ; % false or true
    
    ssp = false; % false or true
    
    data_type = 'meg';
    subj_ids = functions.get_subject_ids(data_type);
    subj_id = subj_ids{1};
    
    create_which_files = 'createAllFiles';
    
end

fif_name = functions.get_fif_names( subj_id, block, sss ) ;

%% select option which files to create
if strcmp(create_which_files, 'createAllFiles')
    
    disp('create all files anew');
    
elseif strcmp(create_which_files, 'createMissingFiles')
    
    disp('create only missing files');
    
    %% in case you need to run script again because cluster jobs were not successful
    %  run for each subj/block but you want to keep files which were generated
    %  successfully (manually check date to not keep old ones based on a previous script that
    %  has been updated!)
    outpath_spec = [outpath  subj_id '/']
    
    if sss
        final_file = fullfile(outpath_spec, ['file_0' num2str(block) '_sss.mat']);
    else
        final_file = fullfile(outpath_spec, ['file_0' num2str(block) '.mat']);
    end
    
    check_file = final_file;
    if exist(check_file)
        error('file already exists. no need for further processing otherwise file will be overwritten.')
    end
    
end


%% Lowpass-filter continuous data (.fif)
%   Lowpass filtering data: onepass-zerophase, order 3624, kaiser-windowed sinc FIR
%   cutoff (-6 dB) 100 Hz
%   transition width 1.0 Hz, passband 0-99.5 Hz, stopband 100.5-500 Hz
%   max. passband deviation 0.0010 (0.10%), stopband attenuation -60 dB
%   reading and preprocessing trial 1 from 1

%         Filter Type: lowpass
%         Window Type: Kaiser
%         Kaiser window beta – Estimate: Max passband deviation/ ripple: 0.001
%         Filter order – Estimate – Transition bandwidth: maximal das Doppelte vom highpass; Beachte: cut off frequency minus transition band width/2 darf nicht kleiner Null sein!!!!!!! (punkt rechnung vor strichrechnung ;)) -> d.h. für high pass filter von 0.1 Hz: 0.1 – 0.2 / 2
%
%         Filter order: transition band width ist nur bei Highpss wichtig!!! beim lowpass kann man die transition band width frei wählen (jedoch nicht zu groß), z.B. transition band width von 1

%     note: The cutoff frequency is passband edge +/- half the transition bandwidth.

% hdr = ft_read_header(fif_name); % for testing only
% data_test = ft_read_data(fif_name); % for testing only

cfg = [];
cfg.dataset = fif_name;
cfg.trialdef.triallength = Inf; % this is the trick: infinite trial length...
cfg.trialdef.ntrials = 1;       % ...for one trial!

cfg = ft_definetrial(cfg); % do definetrial for continuous data

cfg.channel = {'MEG'  'ECG*' 'EOG*'}; % read MEG channels + all BIO channels (EOG and ECG)
cfg.lpfilter = 'yes';
cfg.lpfreq = 100; % it seems to be optimal for the ICA to also include higher frequencies to optimally identify components to reject
cfg.lpfilttype = 'firws'; % kaiser-windowed sinc FIR
cfg.lpfiltdf = 1; % lowpass transition width
cfg.lpfiltwintype = 'kaiser';

data_lp = ft_preprocessing(cfg); % filter continuous data

%% convert grad field from cm (as stored in raw data) to m
data_lp.grad = ft_convert_units(data_lp.grad, 'm'); % use m!


%%   apply hp-filter
% Highpass filtering data: onepass-zerophase, order 908, kaiser-windowed sinc FIR
% transition width 1.0 Hz, stopband 0-0.5 Hz, passband 1.5-125 Hz
% preprocessing trial 1 from 1

%         Filter order – Estimate – Transition bandwidth: maximal das Doppelte vom highpass; Beachte: cut off frequency minus transition band width/2 darf nicht kleiner Null sein!!!!!!! (punkt rechnung vor strichrechnung ;)) -> d.h. für high pass filter von 1 Hz: 1 – 0.5 / 2
%
%         Filter order: transition band width ist nur bei Highpss wichtig!!! beim lowpass kann man die transition band width frei wählen (jedoch nicht zu groß), z.B. transition band width von 1

%     note: The cutoff frequency is passband edge +/- half the transition bandwidth.

cfg = [];
cfg.hpfilter = 'yes';
cfg.hpfreq = 1;  % note most books recommend shorter than longer filters (so, this also speaks for first lp-filter data, downsampling and than applying hp as its length decreases with that procedure; filter length = filter order + 1;
cfg.hpfilttype = 'firws'; % kaiser-windowed sinc FIR
cfg.hpfiltdf = 1; % transition width
cfg.hpfiltwintype = 'kaiser';
%     cfg.plotfiltresp  = 'yes';

data_hp = ft_preprocessing(cfg, data_lp); % filter continuous data

data_filt = data_hp; clear data_hp; clear data_lp;

% reject bad trials based on bad artifacts as manually identified in a previous
% preprocessing step via ft_databrowser


% Reject bad trials with artifacts based on previous detection
% procedure for MEGMAGs and MEGGRADs
%---------------------------------------------------------------------
% Load previous artifact detection structure of MAGs and GRADs
%---------------------------------------------------------------------
if sss
     filename_artifacts = [sprintf('file0%d_%s', block, 'mag_grad_sss.mat')];
else
    filename_artifacts = [sprintf('file0%d_%s', block, 'mag_grad.mat')];
end
load(fullfile(artifact_path, subj_id, filename_artifacts),  'all_art', 'bad_channels');

% adjust samplerate of artifact to match with the current data!
%------------------------------------------
% preprocessing incl. artifact definition based on sr = 250 Hz while here 1000 Hz are used,
% because this is more optimal for applying ica --> sampling rate
% should be at least 3*n_channels² = 3 * 306 * 306 = 280908; and now we
% have sampling points of > 300 000 (while this would be three times
% lower when using a sr of 250 Hz)
%     xx=all_art.artfctdef.ex_amps_mag_grad.artifact % for testing
all_art.artfctdef.ex_amps_mag_grad.artifact = all_art.artfctdef.ex_amps_mag_grad.artifact * 4;

% Remove bad MAG and GRAD channels
%------------------------------------------
channel_select = functions.make_channel_reject( bad_channels );

cfg = [];
if sss == true
    cfg.channel = 'all'; % select all channels for maxfiltered-data incl. bad channels as they have (automatically) been interpolated!
elseif sss == false % select all but the bad channels when data are non-maxfiltered!
    cfg.channel = channel_select;
end

data_cleaned_mag_grad = ft_selectdata(cfg, data_filt);

% Remove bad trials  based on MAGs and GRADs
%-----------------------------------------
% NOTE all_art origins from downsampled filtered data (i.e. no need for
% adjusting sampling points for artifacts!)
all_art.artfctdef.reject = 'nan'; % do not throw out complete trial (because of contious data = 1 trial only ;-P) AND replace artifact contaminated data via NaNs (that will be irnored by the ICA later!)

data_cleaned = ft_rejectartifact(all_art, data_cleaned_mag_grad); clear data_cleaned_mag_grad;

if ssp
    
    cfg = [];
    cfg.inputfile =  fif_name;
    
    data_cleaned = obob_apply_ssp(cfg, data_cleaned );
    
end

clear  'all_art'  'bad_channels';



%% select only MEG chans (and deselect BioChannels)
cfg = [];
cfg.channel = {'MEG'  '-ECG*'  '-EOG*'}; % read MEG channels minus all BIO channels (EOG and ECG)

data_for_ica = ft_selectdata(cfg, data_cleaned); clear data_cleaned;

%% do ica...
ica_done = false;

if sss
    
    data_nonans = data_for_ica.trial{1};
    data_nonans(:, isnan(data_nonans(1,:)))=[];
    
    n_components = rank (data_nonans * data_nonans');
    
else
    
    n_components = length(data_for_ica.label);
    
end


while ~ica_done
    cfg = [];
    cfg.runica.extended = 1; % for more stable results when having line noise in data
    cfg.runica.stop = 1E-7;
    cfg.method = 'runica';
    cfg.numcomponent = n_components;
    
    ica_components = ft_componentanalysis(cfg, data_for_ica);
    
    ica_done = isreal(ica_components.trial{1});
    n_components = n_components - 1;
end % while
clear data_for_ica;

%% create folder in which data will be stored if not existent
outpath_spec = [outpath  subj_id '/'];

if ~exist(outpath_spec, 'dir')
    mkdir(outpath_spec)
end

% make sure to save files in the correct format to not later end up
% with problems in sorting via dir (e.g. file 1, file 10, file 11, file 12, file 2, file 3, ...)
if sss
    save(fullfile(outpath_spec, ['file_0' num2str(block) '_sss']), 'ica_components', 'subj_id', 'block'); %, '-v7.3'); data will be ~1 to 1.5 GB in size but '-v7.3' not necessary and better be inactive otherwise saving takes much longer!
else
    save(fullfile(outpath_spec, ['file_0' num2str(block)]), 'ica_components', 'subj_id', 'block');% , '-v7.3');
end

run_test = 0;
if run_test == 1
    
    %% inspect components...
    cfg = [];
    cfg.blocksize = 10;
    %     cfg.preproc.lpfilter = 'yes';
    %     cfg.preproc.lpfreq = 40;
    cfg.viewmode = 'component';
    cfg.layout = 'neuromag306mag.lay';
    %     cfg.zlim = 'maxabs';
    
    ft_databrowser(cfg, ica_components);
    
    % define rejected components and write them down manually to later save them,  e.g. like that:
    % rejected_components = [1 3 14 ];
    
end


%% check data again
runTest = 0;
if runTest == 1
    
    cfg = [];
    cfg.layout = 'neuromag306mag.lay';
    cfg.channel = 'MEG'; % use all channels in combination with the appropriate scaling factor for magnetometers
    cfg.magscale = 25; % scale magnetometers so that gradiometers and magnetometers are on similar scales
    %         cfg.gradscale = 0.04; % you can use this instead of cfg.magscale = 25;
    cfg.continuous = 'no'; % data are trial-based
    cfg.viewmode = 'butterfly'; % 'butterfly', 'vertical', 'component';
    cfg.plotevents = 'yes'; % 'no' or 'yes', whether to plot event markers. (default is 'yes')
    %     cfg.preproc.lpfilter = 'yes';
    %     cfg.preproc.lpfreq = 35;
    %     cfg.preproc.hpfilter = 'yes';
    %     cfg.preproc.hpfreq = 1;
    
    ft_databrowser(cfg, data_sound); % (1) data_lp , (2) data_resample, (3) data_filt, (4) data_cleaned
    
    cfg.continuous = 'no';
    cfg.plotevents = 'no'; % 'no' or 'yes'
    ft_databrowser(cfg, data_epoch); % (4) data_alltrl, (5) data_epoch / or data
    
end

