function s03b_svs_lcmv_pow(subj_id, chooseBrainOption, createWhichFiles, leadfieldNormOption, ica, sss, ssp, choose_sensors, chooseTFmethod,regfac,choose_sglTrl,logtransform)

% (C) Annekathrin Weise


%% init obob_ownft...
addpath('/mnt/obob/obob_ownft/');
cfg.package.svs = true;
cfg.package.gm2 = true;
obob_init_ft (cfg);

addpath('/mnt/obob/staff/aweise/data/audifly/scripts/');
addpath('/mnt/obob/staff/aweise/templates/');

%% +++++++++++++++++++++   for testing only   ++++++++++++++++++++++++++
runTest = 'no'; % 'yes' or 'no'

if strcmp(runTest, 'yes')
    
    logtransform = 'yes';
    
    regfac = '5%'; %  regfac = '0%',  regfac = '5%'
    
    choose_sglTrl = 'no';

    createWhichFiles = 'createMissingFiles'; %'createAllFiles';
    
    choose_sensors =    'meggrad' ; % 'all_sensors' or  'meggrad'
    
    chooseTFmethod = 'wavelet'; % 'hanning', 'wavelet'
    
    chooseBrainOption = 'parcel'; % 'parcel' or 'whole'  , 'whole_grey'
    
    leadfieldNormOption = 'no'; % 'yes' or 'no'
    
    ica = true; %  true, false
    
    sss= true ; % false or true; note if sss = true: lamda regularization factor is set to 5% otherwise no regularization factor is used
    
    if sss
        ssp = false;
    else
        ssp = false; % false or true; apply only when not applying sss! do not apply when using ica?
    end
    
    data_type = 'meg';
    subj_ids = functions.get_subject_ids(data_type);
    subj_id = subj_ids{1};
    
end

% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
if strcmp( logtransform,'yes')
     if strcmp(choose_sglTrl, 'yes')
        outpath_pow = ['/mnt/obob/staff/aweise/data/audifly/data/meg/source/' chooseBrainOption '/pow/norm_' leadfieldNormOption '/' choose_sensors '/'   chooseTFmethod '/logtrans_yes/sglTrl/' ];
    elseif strcmp(choose_sglTrl, 'no')
        outpath_pow = ['/mnt/obob/staff/aweise/data/audifly/data/meg/source/' chooseBrainOption '/pow/norm_' leadfieldNormOption '/' choose_sensors '/'   chooseTFmethod '/logtrans_yes/' ];
    end

end


    
if ica
    inpath = '/mnt/obob/staff/aweise/data/audifly/data/meg/preproc/clean_ica/';
end
leadfield_path = ['/mnt/obob/staff/aweise/data/audifly/data/meg/source/' chooseBrainOption '/leadfield/norm_' leadfieldNormOption '/' choose_sensors '/'];

% block = 1; % block number is arbitrary chosen as each block contains same information of respective participant needed for applying ssp
% fif_name = functions.get_fif_names( subj_id, block, sss ) ;
epoch = 'sound';

%% select option which files to create
if strcmp(createWhichFiles, 'createAllFiles')
    
    disp('create all files anew');
    
elseif strcmp(createWhichFiles, 'createMissingFiles')
    
    disp('create only missing files anew');
    
    %% in case you need to run script again because cluster jobs were successfully not for all but only for some files
    %  run for each subj/condition but you want to keep files which were generated
    %  successfully (manually check date to not keep old ones based on a previous script that
    %  has been updated!)
    
    % get relevant condition
%     conds = functions.get_conds(epoch);
%     xCondition = 'dev_left';
    
    if ica
        if sss
            checkFile = fullfile(outpath_pow, [subj_id '_' epoch '_' xCondition  '_sss_ica.mat'] );      
        end    
    end
    
    if exist(checkFile)
        return % if file exist already, do not run script
    end
    
end


%% load / define template grid
if strcmp(chooseBrainOption, 'parcel')
    
    chooseGridType = 'grid_3mm_parcel';
    load parcellations_3mm.mat ;
    
    % template_grid: This is the template_grid you will need to morph to each subjects anatomy to then calculate the leadfields with.
    % parcel_grid: This is the grid needed by the obob_svs_virtualsens2source function.
    % layout: Use this for ft_multiplot*
    % parcel_array: This is used internally
    
elseif strcmp(chooseBrainOption, 'whole_grey')
    
    chooseGridType = 'mni_grid0.0075greyMatter';
    load mni_grid0.0075greyMatter.mat;
    
elseif strcmp(chooseBrainOption, 'whole')
    
    chooseGridType = 'mni_grid0.0075';
    load mni_grid0.0075.mat;
    
end

%% load preprocessed MEG data
if sss
    inputfile = fullfile(inpath, [subj_id '_data_' epoch '_clean_sss']);
    disp('sss data chosen');
end

load(inputfile);
data = data_sound; clear data_sound;

%% select smaller data set for testing only
if strcmp(runTest, 'yes')
    cfg = [];
    cfg.trials = 1:100;
    data = ft_selectdata(cfg,data);
end


%% select the data of interest
yCondition = 'allDev';
cond_codes = functions.get_cond_codes(yCondition,epoch);

% get relevant trial indices
% NOTE that at this stage the trials were not matched with respect to trial
% numbers across conditions (dev_left, dev_right)
ind_rel_trials = find(ismember(data.trialinfo(:,4),cond_codes));

cfg = [];
cfg.trials = ind_rel_trials ;
if strcmp(choose_sensors,'all_sensors')
    cfg.channel = 'MEG';
else
    cfg.channel = choose_sensors;
end

data_sel = ft_selectdata(cfg, data);

%% detrend data to remove low frequency drift      
cfg = [];
cfg.detrend = 'yes';

data_detrend = ft_preprocessing(cfg, data_sel);

%% get subject-specific leadfield (that was calculated and saved before)
if ica
     if sss
        load([leadfield_path subj_id '_' epoch '_leadfield_' chooseGridType  '_sss_ica.mat'], 'subject_leadfield');
     end
end

%% calc timelocked data for estimating spatial filters
cfg = [];
cfg.covariance = 'yes';
cfg.covariancewindow   = [-1 1];
data_timelock = ft_timelockanalysis(cfg, data_detrend);

%% calculate the spatial filters
% the function obob_svs_compute_spat_filters takes timelocked data incl. the covariance matrix. 
% it returns a structure containing the filters
cfg = [];
cfg.grid = subject_leadfield; % forward model
if strcmp(chooseBrainOption, 'parcel')
    cfg.parcel = parcellation; % user-specified resolution (see above)
end
cfg.fixedori = 'yes'; % fixate dipoles 
if sss || ssp
    cfg.regfac = '5%'; % use when data are rank deficient    
end
cfg.parcel_avg     = 'avg_filters'; % default; This averages the spatial filters within
% each parcel and then applies them to the sensor data.

spatial_filters = obob_svs_compute_spat_filters(cfg, data_timelock);


%% project single trial time series of all conditions into source space via parcel approach
% 'svs' stands for 'simple virtual sensor'; it uses a broad-band user
% specified lcmv filter to project single trial time series into source
% space
cfg = [];
cfg.spatial_filter = spatial_filters;

data_lcmv = obob_svs_beamtrials_lcmv(cfg, data_detrend);

if isfield(data_lcmv,'sampleinfo')
    data_lcmv= rmfield(data_lcmv,'sampleinfo');
    disp('data_lcmv.sampleinfo removed as it might cause problems')
end

 
%% do the spectral analysis for the current stimulus type - but only for epoch type 'sound'

% get relevant conditions
conds = {'dev_left', 'dev_right'}; % functions.get_conds(epoch);

for iCondition = 1:length(conds) % only choose deviants!
    
    xCondition = conds{iCondition};
    
    cond_codes = functions.get_cond_codes(xCondition,epoch);
    
    %% get relevant trial indices
    
    ind_rel_trials = find(ismember(data_lcmv.trialinfo(:,4), cond_codes));
    
    % Increase sensitivity to non-phase locked signals
    
    % One of the purposes of TFR analysis is to reveal signals that are non-phase locked to stimuli, in contrast to the analysis of
    % evoked fields/potentials. But the phase-locked signals also show up in the TFRs, as they are present in the signal and therefore
    % decomposed by the TF analysis. To enhance the sensitivity to the non-phase locked activity, we can subtract the phase locked activity
    % and redo the analysis.
    % The phase-locked activity is the evoked field/potential. Assuming the average evoked signal are a good representation of the
    % phase-locked signal (i.e., sufficient signal-to-noise ratio), we can subtract it from each trial, and redo the TFR analysis.
    
    % Get timelocked
    cfg = [];
    cfg.trials = ind_rel_trials;
    data_timelock = ft_timelockanalysis(cfg, data_lcmv);
    
    data_minus_timelock = data_lcmv;
    for i = 1:length(data_lcmv.trial)
        data_minus_timelock.trial{i} = data_lcmv.trial{i} - data_timelock.avg;
    end
    
    if strcmp(chooseTFmethod,'wavelet')
        
           % TFR with Morlet wavelets
            cfg = [];
            cfg.method = 'wavelet';
            cfg.trials = ind_rel_trials; % relevant trials of the current stimuluy type; default: all
            cfg.foi         = 1:1:30;       % Frequencies we want to estimate from 1 Hz to 30 Hz in steps of 1HZ
            cfg.toi         =  -1.5 : 0.05 : 1.5; % Time bins from the beginning to the end of the respective epoch
            cfg.width    = 5;
            cfg.output =  'pow'; % return the power-spectra
            
            if strcmp(choose_sglTrl, 'yes')
                cfg.keeptrials = 'yes';
            elseif strcmp(choose_sglTrl, 'no')
                cfg.keeptrials = 'no'; % default: no; ''fourier'' requires cfg.keeptrials = ''yes'' and cfg.keeptapers = ''yes'''
                %   cfg.keeptrials = 'yes' or 'no', return individual trials or average (default = 'no')
                % keep trials also relevant when doing e.g. correlation with behavior
            end
            
             if strcmp(logtransform,'yes')
                cfg.keeptrials = 'yes';
             end
            
    end
    
    freq = ft_freqanalysis(cfg, data_lcmv);  
    freq.cfg = []; % remove cfg field in freq
    
    freq_minus_timelock = ft_freqanalysis(cfg, data_minus_timelock);
    freq_minus_timelock.cfg = [];
    
    %% logtransoform data if option is set to 'yes'
    if strcmp(logtransform,'yes')
        %             Log-transform the single-trial power
        %             Spectral power is not normally distributed. Although this is in theory not a problem
        %             for the non-parametric statistical test, its sensitivity is usually increased by
        %             log-transforming the values in the power spectrum.
        
        
        cfg           = [];
        cfg.parameter = 'powspctrm';
        cfg.operation = 'log10';
        
        freq_logpow    = ft_math(cfg, freq);
        freq_minus_timelock_logpow = ft_math(cfg, freq_minus_timelock);
        
        
        %% get rid of single trials to save storage space
        cfg = [];
        cfg.trials = 'all';
        cfg.avgoverrpt = 'yes';
        
        freq_logpow = ft_selectdata(cfg,freq_logpow);
        freq_minus_timelock_logpow = ft_selectdata(cfg,freq_minus_timelock_logpow);
    end
    
    %% create folder in which data will be stored if not existent
    if ~exist(outpath_pow, 'dir')
        mkdir(outpath_pow)
    end
    
    %% save individual power data for each condition    
    if ica
        
        if sss
            outputfile = fullfile(outpath_pow, [subj_id '_' epoch '_' xCondition  '_sss_ica.mat'] );
        end
    
    end
    
    if strcmp(logtransform, 'yes')
        save(outputfile,'freq_logpow', 'freq_minus_timelock_logpow');%,'-v7.3');
    end
    
    clear xCondition cond_codes ind_rel_trials freq freq_minus_timelock
end % cond loop


end


