function data_source = obob_svs_beamtrials_lcmv_aw(cfg, data)

% A simple virtual sensor (svs)
% obob_svs_beamtrials_lcmv uses a broad-band user specified lcmv filter to project single trial 
% time series into source space.
%
% Call as:
%   data_source = obob_svs_beamtrials_lcmv(cfg, data)
%
% Input:
% obligatory:
%   cfg.grid     = source grid that must contain the leadfield (in grid.leadfield).
%                  If cfg.parcel is set, the grid and the parcel must correspond
%   data         = ft structure after preprocessing
%
%   Sensor positions (grad or elec) can be specified in the cfg or data
%   structure.
%
% optional:
%   cfg.parcel     = A parcellation structure if you want to get
%                    parcellated output. You can either load the template
%                    (parcellations_3mm.mat) or create one with
%                    obob_svs_create_parcellation. Otherwise leave empty.
%   cfg.mri        = mri (mri_aligned, output of obob_coregister) in order
%                    to compute the leadfield, needed if cfg.parcel is set
%   cfg.channel    = cell array of channel labels to use
%   cfg.hpfilter   = 'yes' or 'no' (default), see NOTE
%   cfg.hpfreq     = high pass filter frequency
%   cfg.hpfilttype = filter type, default: 'but'
%   cfg.lpfilter   = 'yes' or 'no' (default), see NOTE
%   cfg.lpfreq     = low pass filter frequency
%   cfg.lpfilttype = filter type, default: 'but'
%
%   NOTE: filters are only applied to create the beamformer filter (in
%   other words the output is not filtered)
%
%   cfg.cov_win  = [start end] time window to estimate covariance. this 
%                  will be used for filter estimation (!). default = 'all'.
%   cfg.regfac   = regularization factor to be passed on to source analysis
%                  (e.g. '5%'). (optional)
%   cfg.trials   = cell array of trial selection to use. default = 'all'.
%
%   cfg.nai      = If this is set to true or 'yes', the output of the
%                  projection will be normalized by the noise estimate at
%                  the voxels, effectively calculating the neural activity
%                  index. default = false.
%
% Output: 
% data_source    = a preproc structure with source level time series at 
%                  each grid point. can be e.g. passed on to 
%                  ft_timelockanalysis or ft_freqanalysis, i.e. used like 
%                  a sensor level structure.

% Copyright (c) 2014-2017, Nathan Weisz, Julia Frey, Anne Hauswald & Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

%% defaults
ft_checkconfig(cfg, 'required', 'grid');

if ~isfield(cfg.grid, 'leadfield')
  error('You must supply a leadfield in the grid.');
end %if

chanlabels = ft_getopt(cfg, 'channel', 'all');
hpfilter = ft_getopt(cfg, 'hpfilter', 'no', 0);
hpfreq = ft_getopt(cfg, 'hpfreq', []);
hpfilttype = ft_getopt(cfg, 'hpfilttype', 'but', 0);
lpfilter = ft_getopt(cfg, 'lpfilter', 'no', 0);
lpfreq = ft_getopt(cfg, 'lpfreq', []);
lpfilttype = ft_getopt(cfg, 'lpfilttype', 'but', 0);
regfac = ft_getopt(cfg, 'regfac', []);
fixedori = ft_getopt(cfg, 'fixedori', 'yes');
trials = ft_getopt(cfg, 'trials', [1:length(data.trial)]);
nai = ft_getopt(cfg, 'nai', false);
grid=ft_getopt(cfg, 'grid', []);
parcel=ft_getopt(cfg, 'parcel', []);


if ischar(nai)
  if strcmpi(nai, 'yes')
    nai = true;
  else
    nai = false;
  end %if
end %if

if nai
  warning(sprintf(['WARNING! Calculating the Neural Activity Index is a new feature and should thus be considered HIGHLY EXPERIMENTAL!\n' ...
    'Please use it with extreme care and check your results.\n' ...
    'There is also NO GUARANTEE that this feature is already stable in the sense\n' ...
    'that its results might be different in a while!\n\n' ...
    'YOU HAVE BEEN WARNED!']));
end %if

if isfield(cfg, 'cov_win') % show message if covariance window is selected
  covariancewindow=cfg.cov_win;
  disp(['Covariance estimated for specified time window: ' num2str(covariancewindow(1)) ' - ' num2str(covariancewindow(2)) 'sec'])
else
  covariancewindow='all';
end

% input
%--------

% check whether all input is in meters
if ~strcmp(grid.unit, 'm')
  error('Your leadfield is not in meters!');
end %if

if ~isempty (parcel)
    parcel = cfg.parcel.parcel_array;
      for idx_parcel = 1:length(parcel)
        if ~strcmp(parcel{idx_parcel}.unit, 'm')
          error('Your parcellation structure is not in meters!');
        end %if
      end %for
end

sens = ft_fetch_sens(cfg, data);
if ~strcmp(sens.unit, 'm')
  error('Your sensor definition is not in meters!');
end %if

if ft_senstype(sens, 'eeg')
  data.elec = sens;
else
  data.grad = sens;
end %if

%% Select the relevant data 
cfg=[]; 
cfg.trials = trials; 
data = ft_redefinetrial(cfg,data); 

%% check whether leadfield channel order and data sensor order match, if not abort for now
% get sensor indeces of data in lf and lf in data
[~, idA, idB] = intersect(data.label, grid.cfg.channel);
% compare order
lf_data_match = isequal(idA, idB);

if ~lf_data_match % if labels do not match try to find the reason and report
  
  if numel(data.label) ~= numel(grid.cfg.channel)
    error('The number of channels in your data and leadfield are different. Check whether cfg.channel contains the right input.')
  end
  % check if sorted leadfield labels would fit (likely ft_selectdata
  % problem)
  [~, idA, idB] = intersect(data.label, sort(grid.cfg.channel));
  lf_data_sort_match = isequal(idA, idB);
  if lf_data_sort_match
    error(['It seems your data sensors are ordered alphabetically. Try using obob_reorder_channels to undo this and submit again. '...
      'If the channel order in the data and in the leadfield don''t match the output of this function is wrong!'])
  else
    error('There is an unknown mismatch between your data sensors and the leadfield sensors')
  end
end


%% do timelock analysis with inputs an compute covariance

cfg=[];
cfg.channel            = chanlabels;
cfg.preproc.hpfilter   = hpfilter;
cfg.preproc.hpfreq     = hpfreq;
cfg.preproc.hpfilttype = hpfilttype;
cfg.preproc.lpfilter   = lpfilter;
cfg.preproc.lpfreq     = lpfreq;
cfg.preproc.lpfilttype = lpfilttype;
cfg.covariance         = 'yes';
cfg.covariancewindow   = covariancewindow;

data_avg = ft_timelockanalysis(cfg, data);

%% do lcmv beamforming (better: compute filter on averaged data)

cfg=[];
cfg.method          = 'lcmv';
cfg.vol.unit        = 'm'; % th: dirty hack to make this work. as we always provide leadfields, we do not need a vol...
cfg.grid            = grid;
cfg.projectnoise    = 'yes';
cfg.lcmv.keepfilter = 'yes';
cfg.lcmv.fixedori   = fixedori; 
cfg.lcmv.lambda     = regfac;

lcmvall=ft_sourceanalysis(cfg, data_avg);

%% select channels in preprocessed data based on input
cfg=[];
cfg.channel=chanlabels;
data=ft_preprocessing(cfg, data);

%%
cmpfilter = lcmvall.avg;
% here end of the filter calculation
% do parcel 2 filt
if ~isempty (parcel)
    cfg.parcel = parcel;
    cfg.filter = lcmvall.avg;
    cmpfilter  = obob_svs_filt2parcel(cfg);
end

%% apply filter from average lcmv to single trials

beamfilts = cat(1,cmpfilter.filter{:});

if ~isempty (parcel)
    all_noisecoeffs = cmpfilter.noise{:};
else 
    all_noisecoeffs = cmpfilter.noise(lcmvall.inside);
end

% create output structure
%------------------------
data_source = data;

nrpt = length(data.trial); % n of trials

% init progress report
%----------------------
fprintf(['using precomputed filters\n'])
ft_progress('init', 'text');

for i=1:length(data.trial)
 
  % update progress report
  %------------------------
  ft_progress(i/nrpt, 'scanning repetition %d of %d', i, nrpt);
  
  % multiply trials with filter
  %-----------------------------
  data_source.trial{i} = beamfilts*data.trial{i};
  
   if nai% TO DO
     data_source.trial{i} = data_source.trial{i} ./ repmat(all_noisecoeffs, 1, size(data_source.trial{i}, 2));
   end %if
 
end
% close progress report
%----------------------
ft_progress('close');

%% create a pseudo preprocessing structure for the output
% labels
%-------
if ~isempty (parcel)
    %data_source.label    = cellstr(num2str([1:length(cmpfilter.parlabel)]'));
    data_source.label = cmpfilter.parlabel;
else
    data_source.label = cellstr(num2str([1:sum(grid.inside)]'));
end

% remove fields that do not fit
%-------------------------------
data_source = rmfield(data_source, 'cfg');

if isfield(data_source, 'grad')
  data_source = rmfield(data_source, 'grad');
end %if

if isfield(data_source, 'elec')
  data_source = rmfield(data_source, 'elec');
end %if
