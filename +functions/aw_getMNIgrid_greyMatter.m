function [mni_grid] = aw_getMNIgrid_greyMatter(gridRes)

% (C) Julia Frey see obob git wiki)
% adapted by A. Weise

% input: grid resolution in m
% output: mni_grid

% see http://fieldtrip.fcdonders.nl/template/atlas for details.

ft_templatePath = ['/mnt/obob/obob_ownft/external/fieldtrip/template/'];
aw_templatePath = '/mnt/obob/staff/aweise/templates';

% gridRes = 0.015 m --> 1.5 cm; 0.075 m --> 0.75 cm =7.5 mm
% inwardshift = -0.015; % --> -1.5 cm; for testing only

cfg             = [];
cfg.gridres     = gridRes; % use smaller grid resolution (0.005) and constraint to grey matter! (see gitlab wiki how to); 
cfg.inwardshift = inwardshift;
% cfg.plot        = 'yes';

mni_grid   = functions.th_gm2_make_mnigrid(cfg);



% Select grey matter only
% Halving the grid space results in an eightfold increase in time and memory requirements (Barnes et al., 2004).
% So, even though we do have a lot of processing power, you might still want to minimise the number of
% voxels of interest. You can do so by selecting only the grey matter as region of interest, 
% and changing the .inside-field of your template grid accordingly: 


% load atlas
atlas = ft_read_atlas(fullfile(ft_templatePath, 'atlas', 'aal', 'ROI_MNI_V4.nii')); % adapt path name
atlas = ft_convert_units(atlas,'m'); % assure that atlas and grid have the same units

% create a mask with grey matter
cfg = [];
cfg.atlas = atlas;
cfg.roi = atlas.tissuelabel; % select the grey matter
cfg.inputcoord = 'mni';
greyMatterMask = ft_volumelookup(cfg, mni_grid); 

% replace .inside-field with grey matter mask
mni_grid.inside(mni_grid.inside == 1) = 0; 
mni_grid.inside(greyMatterMask) = 1;

mni_grid_greyMatter = mni_grid;

save([aw_templatePath '/mni_grid' nameGridRes 'greyMatter.mat' ], 'mni_grid_greyMatter');

% After this step, the .inside-field of your template grid should contain less voxels that are equal to 1: 
% 
% ATTENTION: Depending on where you have created the grid, you might get different numbers
% of 'inside' voxels due to a rounding error. This is the case in particular for (very)
% small-grained grids. For instance, with the 5mm-grid, I get: 
% 
% 
% 11807 inside-voxels computed locally (on my Mac)
% 
% 11810 inside-voxels computed on my personal virtual machine
% 
% 11813 inside-voxels computed on the condor

% 
% If you compute the grid on the fly for doing statistics/plotting using
% pre-computed data, this will result in an error if you have computed the 
% source analysis on some server, and you try to do the statistics locally.
% So, you might want to save the grey-matter-only grid after having created
% it for doing statistics/plotting later on.