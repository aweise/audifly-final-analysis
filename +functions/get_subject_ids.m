function [subj_ids] = get_subject_ids(data_type)


% (C) Anne Weise

% input: data_type:
%           (1) 'meg' (including behavioral data and meg data)

% output: subj_ids


%% analyses for all subjects with good MEG data
if strcmp(data_type, 'meg') 
    subj_ids = {'19831031krab', '19990219glgn' , '19971028mrhs', '19950901ethm', '19800616mrgu', '19960722einm', '19810726gdzn', '19741212hlhn', '19891222gbhl','19930506urhe', '19801021mrhl', '19920208dnkp' , '19980530iibr', '19830421rsur', '19960930iahr', '19921003crbu', '19770617ansh', '19750206hlbn','19990902eihl', '19961026efhu', '19930822blbb', '19960130smrs', '19951120mrbr', '19911001ighl', '19940629anbd', '19901106mrgb', '19870420mnhi', '19961106sbgi', '19900319sewn', '19860503dgsh', '19860618crwi' };  
end


% note: '19901106mrgb': block 4: no maxfiltering possible due to too many
% bad channels


