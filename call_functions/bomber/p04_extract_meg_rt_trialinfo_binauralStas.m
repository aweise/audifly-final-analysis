
clear all global;
close all;

%% init obob_ownft...
addpath('/mnt/obob/obob_ownft/');
obob_init_ft;

%% define vars
data_type = 'meg';
subj_ids = functions.get_subject_ids(data_type); 
trialinfo_path = '/mnt/obob/staff/aweise/data/audifly/data/meg/trialinfo/sta_binaural/';


trial_onset_code = 201;
sound_onset_code= [2,3,4,5,20,120,220,30,130,230,40,140,240,50,150,250];
novels_right = [3, 5];
novels_left = [1,2];
sounds_deviant = [2,3,4,5];
targets_right = [4,5,40,140,240,50,150,250];
targets_left = [2,3,20,120,220,30,130,230];
sta_bef_dev = [120,130,140,150];
sta_without_sta_after_dev = [20, 30, 40, 50, 120,130,140,150];

target_onset_code = [88,99];
rt_onset_datapixx_code = {'STI013', 'STI014'};
response_code = [111, 221, 112, 222, 113, 223]; % correct, incorrect, no response for left and right target, respectively
response_code_correct = [111, 221];
response_code_incorrect = [112, 222];
response_code_noresponse  = [113, 223];

datapixx_codes = [5013,5014];
% datapixx_down = 5013;
% datapixx_up = 5014;

valid_range_soa_onset_oddball = [998:1:1002];

n_blocks = 7; % number experimental blocks
sss = false;
nr_subject_id =31


%% subj loop
for iSubj = nr_subject_id:length(subj_ids)

    subj_id = subj_ids{iSubj};
    subj_id_ext = ['subj_' subj_id]; 
    
    for iFile = 1:n_blocks

        summary_trials = [];
        trial_matrix = [];
            
        fif_name = functions.get_fif_names( subj_id, iFile, sss);   
        
        % read events
        hdr = ft_read_header(fif_name);

        events = ft_read_event(fif_name, 'header', hdr);

        events = ft_filter_event(events, 'type', {'Trigger', 'STI013', 'STI014'});   %  'STI013' -> codes for right target;  'STI014'  --> codes for left target

        
       % trigger for right sound preceing target has same value as datapixx response trigger (=5)
        % to avoid confusion we first re-code this vipixxtrigger
        for iEvent = 1:length(events)

            curr_event_type = events(iEvent).type;
            
            if strcmp(curr_event_type,'STI013') 
                events(iEvent).value =  5013;
            elseif strcmp(curr_event_type,'STI014') 
              events(iEvent).value =  5014;
            end
            
        end
        
        % we recode trigger that cannot be trial onset trigger (though having the same code) but are
        % rather an artefact (of the preceding trial onset trigger)
        for iEvent = 1:length(events)
            
            curr_event_value = events(iEvent).value;
            
            if curr_event_value == trial_onset_code
                % make sure that a trial onset trigger is not closely followed by another trial onset trigger - if so, the current trigger is an artifact (and may actually overwrite a sound trigger - will be checked and recoded later again) and will be recoded
                if events(iEvent + 1).value == trial_onset_code
                    events(iEvent + 1).value = 2010;
                elseif events(iEvent + 2).value == trial_onset_code
                    events(iEvent + 2).value = 2010;
                end
            end
            
        end
        
        
        if strcmp(subj_id, '19831031krab') && iFile == 5
            events(362).value = 201; % manually checked via isi_trialonset_oddball: 142634-141633 = 1001 :)!
        elseif strcmp(subj_id, '19950901ethm') && iFile == 1
            events(612).value = 2020; % manually checked ; trigger was 20 before and only 3 ms after trial onset - had to be recoded!
        elseif strcmp(subj_id, '19950901ethm') && iFile == 2
            events(246).value = 201; %  manually checked via isi_trialonset_oddball:101297-100297=1000 :)
        elseif strcmp(subj_id, '19950901ethm') && iFile == 6
            events(417).value = 201; %  manually checked via isi_trialonset_oddball:164730-163730=1000 :)
        elseif strcmp(subj_id, '19800616mrgu') && iFile == 2
            events(523).value = 2020; % manually checked ; trigger was 20 before and only 3 ms after trial onset - had to be recoded!
        elseif strcmp(subj_id, '19800616mrgu') && iFile == 7
            events(739).value = 2020; % manually checked ; trigger was 20 before and only 3 ms after trial onset - had to be recoded!
        elseif strcmp(subj_id, '19960722einm') && iFile == 6
            events(277).value = 201;  %  manually checked via isi_trialonset_oddball:108466-107465=1001 :)
        elseif strcmp(subj_id,   '19810726gdzn') && iFile == 1
            events(456).value = 201;  %  manually checked via isi_trialonset_oddball:188536-187535=1001 :)
        elseif strcmp(subj_id,   '19810726gdzn') && iFile == 2
            events(272).value = 201;  %  manually checked via isi_trialonset_oddball!
        elseif strcmp(subj_id,   '19810726gdzn') && iFile == 5
            events(576).value = 2020; % manually checked ; trigger was 20 before and only 5 ms after trial onset - had to be recoded!
        elseif strcmp(subj_id,   '19810726gdzn') && iFile == 6
            events(681).value = 201;  %  manually checked via isi_trialonset_oddball!
        elseif strcmp(subj_id,    '19741212hlhn') && iFile == 4
            events(702).value = 2020; % manually checked ; trigger was 20 before and only 5 ms after trial onset - had to be recoded!
        elseif strcmp(subj_id,    '19741212hlhn') && iFile == 6
            events(154).value = 201; %  %  manually checked via isi_trialonset_oddball
        elseif strcmp(subj_id,     '19891222gbhl') && iFile == 1
            events(142).value = 2020; % manually checked ; trigger was 20 before and only 6 ms after trial onset - had to be recoded!
        elseif strcmp(subj_id,     '19891222gbhl') && iFile == 3
            events(292).value = 201; %  manually checked via isi_trialonset_oddball
        elseif strcmp(subj_id,      '19930506urhe') && iFile == 1
            events(311).value = 201; %  manually checked via isi_trialonset_oddball
        elseif strcmp(subj_id,      '19930506urhe') && iFile == 6
            events(282).value = 201; %  manually checked via isi_trialonset_oddball
        elseif strcmp(subj_id,      '19930506urhe') && iFile == 7
            events(246).value = 201; %  manually checked via isi_trialonset_oddball
        elseif strcmp(subj_id,      '19801021mrhl') && iFile == 1
            events(22).value = 2020; % manually checked ; trigger was 20 before and only 6 ms after trial onset - had to be recoded!
        elseif strcmp(subj_id,      '19801021mrhl') && iFile == 2
            events(703).value = 201;  %  manually checked via isi_trialonset_oddball
        elseif strcmp(subj_id,      '19801021mrhl') && iFile == 4
            events(763).value = 201;  %  manually checked via isi_trialonset_oddball
        elseif strcmp(subj_id,     '19920208dnkp') && iFile == 1
            events(659).value = 2020; % manually checked ; trigger was 20 before and only 5 ms after trial onset - had to be recoded
        elseif strcmp(subj_id,     '19920208dnkp') && iFile == 2
            events(106).value = 201; %  manually checked via isi_trialonset_oddball!end
        elseif strcmp(subj_id,     '19920208dnkp') && iFile == 3
            events(126).value = 201; %  manually checked via isi_trialonset_oddball!end
        elseif strcmp(subj_id,     '19920208dnkp') && iFile == 4
            events(587).value = 201; %  manually checked via isi_trialonset_oddball!end
        elseif strcmp(subj_id,     '19920208dnkp') && iFile == 5
            events(458).value = 201; %  manually checked via isi_trialonset_oddball!end
        elseif strcmp(subj_id,     '19920208dnkp') && iFile == 7
            events(188).value = 201; %  manually checked via isi_trialonset_oddball!end
        elseif strcmp(subj_id,     '19980530iibr') && iFile == 2
            events(705).value = 201; %  manually checked via isi_trialonset_oddball!end
        elseif strcmp(subj_id,     '19980530iibr') && iFile == 4
            events(272).value = 201; %  manually checked via isi_trialonset_oddball!end
        elseif strcmp(subj_id,     '19980530iibr') && iFile == 6
            events(497).value = 201; %  manually checked via isi_trialonset_oddball!end
        elseif strcmp(subj_id,     '19980530iibr') && iFile == 7
            events(326).value = 201; %  manually checked via isi_trialonset_oddball!end
        elseif strcmp(subj_id,    '19830421rsur') && iFile == 1
            events(632).value = 201; %  manually checked via isi_trialonset_oddball!end
        elseif strcmp(subj_id,    '19830421rsur') && iFile == 3
            events(601).value = 201; %  manually checked via isi_trialonset_oddball!end
        elseif strcmp(subj_id,    '19830421rsur') && iFile == 4
            events(131).value = 201; %  manually checked via isi_trialonset_oddball!end
        elseif strcmp(subj_id,    '19830421rsur') && iFile == 5
            events(26).value = 201; %  manually checked via isi_trialonset_oddball!end
        elseif strcmp(subj_id,    '19830421rsur') && iFile == 7
            events(153).value = 201; %  manually checked via isi_trialonset_oddball!end
        elseif strcmp(subj_id,    '19960930iahr') && iFile == 3
            events(177).value = 2020; % manually checked ; trigger was 20 before and only few ms after trial onset - had to be recoded!
        elseif strcmp(subj_id,    '19960930iahr') && iFile == 4
            events(279).value = 201; %  manually checked via isi_trialonset_oddball!end
        elseif strcmp(subj_id,    '19960930iahr') && iFile == 5
            events(107).value = 201; %  manually checked via isi_trialonset_oddball!end
        elseif strcmp(subj_id,    '19921003crbu') && iFile == 2
            events(167).value = 2020; % manually checked ; trigger was 20 before and only few ms after trial onset - had to be recoded!
        elseif strcmp(subj_id,    '19921003crbu') && iFile ==3
            events(301).value = 201;
        elseif strcmp(subj_id,    '19921003crbu') && iFile ==6
            events(752).value = 201;
        elseif strcmp(subj_id,    '19921003crbu') && iFile ==7
            events(482).value = 201;
        elseif strcmp(subj_id,    '19770617ansh') && iFile ==4
            events(521).value = 201;
        elseif strcmp(subj_id,    '19770617ansh') && iFile ==5
            events(612).value = 201;
        elseif strcmp(subj_id,    '19770617ansh') && iFile ==7
            events(27).value = 2020;
        elseif strcmp(subj_id,    '19750206hlbn') && iFile ==1
            events(388).value = 2020;
        elseif strcmp(subj_id,    '19750206hlbn') && iFile ==4
            events(236).value = 201;
        elseif strcmp(subj_id,    '19750206hlbn') && iFile ==5
            events(106).value = 201;
        elseif strcmp(subj_id,    '19750206hlbn') && iFile ==6
            events(591).value = 201;
        elseif strcmp(subj_id, '19990902eihl') && iFile ==2
            events(128).value = 201;
        elseif strcmp(subj_id, '19990902eihl') && iFile ==3
            events(323).value = 201;
        elseif strcmp(subj_id, '19990902eihl') && iFile ==7
            events(588).value = 201;
        elseif strcmp(subj_id,   '19961026efhu') && iFile ==1
            events(71).value = 201;
        elseif strcmp(subj_id,   '19961026efhu') && iFile ==3
            events(111).value = 201;
        elseif strcmp(subj_id,   '19961026efhu') && iFile ==7
            events(497).value = 2020;
            events(498)=[]; % strange trigger that should not be here (=2010)
        elseif strcmp(subj_id,   '19961026efhu') && iFile ==6
            events(541).value = 201;
        elseif strcmp(subj_id,    '19930822blbb') && iFile ==6
            events(392).value = 2020;
        elseif strcmp(subj_id,    '19960130smrs') && iFile ==2
            events(126).value = 201;
            events(288).value = 201;
            events(529).value = 201;
            events(253).value = 2020;
        elseif strcmp(subj_id,    '19960130smrs') && iFile == 3
            events(781).value = 201;
        elseif strcmp(subj_id,    '19960130smrs') && iFile == 6
            events(141).value = 201;
        elseif strcmp(subj_id,    '19960130smrs') && iFile == 7
            events(61).value = 201;
            events(127).value = 2020;
        elseif strcmp(subj_id,      '19951120mrbr') && iFile == 1
            events(86).value = 201;
        elseif strcmp(subj_id,      '19951120mrbr') && iFile == 3
            events(588).value = 2020;
         elseif strcmp(subj_id,      '19951120mrbr') && iFile == 5
            events(199).value = 2020;
        elseif strcmp(subj_id,      '19911001ighl') && iFile == 1
            events(337).value = 201;
            events(590).value = 201;
        elseif strcmp(subj_id,      '19911001ighl') && iFile == 3
            events(22).value = 2020;                   
        elseif strcmp(subj_id,      '19911001ighl') && iFile == 5
            events(522).value = 201;
        elseif strcmp(subj_id,      '19911001ighl') && iFile == 6
            events(652).value = 201;
        elseif strcmp(subj_id,      '19911001ighl') && iFile == 7
            events(486).value = 201;
        elseif strcmp(subj_id,        '19901106mrgb') && iFile == 2
            events(766).value = 201;        
         elseif strcmp(subj_id,        '19901106mrgb') && iFile == 3
            events(571).value = 201;  
        elseif strcmp(subj_id,        '19901106mrgb') && iFile == 4
            events(212).value = 2020;
        elseif strcmp(subj_id,        '19901106mrgb') && iFile == 5
            events(232).value = 201;
            events(784).value = 2020;
        elseif strcmp(subj_id,        '19901106mrgb') && iFile == 7
            events(782).value = 2020;
        elseif strcmp(subj_id,        '19870420mnhi') && iFile == 7
            events(181).value = 201;
        elseif strcmp(subj_id,      '19961106sbgi') && iFile == 1
            events(525).value = 2020;
        elseif strcmp(subj_id,      '19961106sbgi') && iFile == 2
            events(181).value = 201;
        elseif strcmp(subj_id,      '19961106sbgi') && iFile == 6
            events(311).value = 201;
        elseif strcmp(subj_id,     '19900319sewn') && iFile == 7
            events(46).value = 201;            
            events(298).value = 2020;
         elseif strcmp(subj_id,  '19940629anbd') && iFile == 1
            events(472).value = 201;  
            events(708).value = 201;  
         elseif strcmp(subj_id,  '19940629anbd') && iFile == 3
            events(431).value = 201;  
         elseif strcmp(subj_id,  '19940629anbd') && iFile == 5
            events(401).value = 201;  
         elseif strcmp(subj_id,  '19940629anbd') && iFile == 7
            events(442).value = 201;  
        elseif strcmp(subj_id,  '19860503dgsh') && iFile == 1
            events(598).value = 2020;  
        elseif strcmp(subj_id,  '19860503dgsh') && iFile == 4
            events(422).value = 2020;  
        elseif strcmp(subj_id,  '19860503dgsh') && iFile == 6
            events(271).value = 201;  
        elseif strcmp(subj_id,  '19860618crwi') && iFile == 3
            events(121).value = 201; 
        elseif strcmp(subj_id,  '19860618crwi') && iFile == 4
            events(237).value = 201;
            events(403).value = 201;
            events(710).value = 201;
        elseif strcmp(subj_id,  '19860618crwi') && iFile == 6
            events(176).value = 201;
        end
        
        %         if strcmp(subj_id, '19950901ethm') && iFile == 6
        %             events(739).value = 2020; % manually checked ; trigger was 20 before and only 3 ms after trial onset - had to be recoded!
        if  strcmp(subj_id,   '19810726gdzn') && iFile == 2
            events(589).value = 201;  %  manually checked via isi_trialonset_oddball!
        elseif strcmp(subj_id,      '19930506urhe') && iFile == 6
            events(608).value = 201; %  manually checked via isi_trialonset_oddball
        elseif strcmp(subj_id,      '19930506urhe') && iFile == 7
            events(783).value = 201; %  manually checked via isi_trialonset_oddball
        elseif strcmp(subj_id,     '19920208dnkp') && iFile == 2
            events(751).value = 201;%  manually checked via isi_trialonset_oddball!
        elseif strcmp(subj_id,     '19920208dnkp') && iFile == 3
            events(427).value = 201; %  manually checked via isi_trialonset_oddball!end
        elseif strcmp(subj_id,     '19920208dnkp') && iFile == 4
            events(747).value = 201; %  manually checked via isi_trialonset_oddball!end
        elseif strcmp(subj_id,     '19920208dnkp') && iFile == 5
            events(282).value = 2020; % manually checked ; trigger was 20 before and only 6 ms after trial onset - had to be recoded!
       elseif strcmp(subj_id,     '19920208dnkp') && iFile == 7
            events(793).value = 201; %  manually checked via isi_trialonset_oddball!end
        elseif strcmp(subj_id,    '19830421rsur') && iFile == 1
            events(17).value = 2020; % manually checked ; trigger was 20 before and only few ms after trial onset - had to be recoded!
       elseif strcmp(subj_id,    '19830421rsur') && iFile == 5
            events(742).value = 201; %  manually checked via isi_trialonset_oddball!end
       elseif strcmp(subj_id,    '19830421rsur') && iFile == 6
            events(16).value = 201; %  manually checked via isi_trialonset_oddball!end
      elseif strcmp(subj_id,    '19960930iahr') && iFile == 4
            events(295).value = 201; %  manually checked via isi_trialonset_oddball!end
       elseif strcmp(subj_id,    '19921003crbu') && iFile ==3
            events(522).value = 201;
       elseif strcmp(subj_id, '19990902eihl') && iFile ==2
            events(609).value = 201;
        elseif strcmp(subj_id,   '19961026efhu') && iFile ==3
            events(126).value = 201;
        elseif strcmp(subj_id,   '19961026efhu') && iFile ==6
            events(652).value = 2020;
            events(653) = []; %strange trigger 2010 to be deleted
        end
            
        if strcmp(subj_id,     '19920208dnkp') && iFile == 4
            events(512).value = 201; %  manually checked via isi_trialonset_oddball!end
          elseif strcmp(subj_id,     '19920208dnkp') && iFile == 7
            events(12).value = 2020; % manually checked ; trigger was 20 before and only 5 ms after trial onset - had to be recoded!
       elseif strcmp(subj_id,   '19961026efhu') && iFile ==3
            events(448).value = 201;
        
        end
        
        event_trialStartTrigger = ft_filter_event(events, 'type', 'Trigger', 'value', trial_onset_code);

        all_blocktrials = length(event_trialStartTrigger);

        idx_events = 0;
        idx_trials = 0;

        relevant_events = length(events);

        while (idx_events+1) <= length(events)

            idx_events = idx_events + 1;
            
            % do this test only once to check whether there are too few
            % trial onsets in the data (because the value coding trial
            % onset was replaced by a wrong one)
            if  idx_events == 1
                all_block_triggers_test=vertcat(events.value);
                nStartTrialTrigger = length(find(all_block_triggers_test(:) == trial_onset_code)) ;
                ind_start_trial_trigger = find(all_block_triggers_test(:)==trial_onset_code); % should be 4 different triggers between 2 start-trial-trigger
                diff_ind_start_trials =  diff( ind_start_trial_trigger); % calculate the nr of elements between two trial onset trigger
                strange_diff_ind = find(  diff_ind_start_trials > 6) % find the strange ones among them!
%                 strange_diff_ind_toomany = find(  diff_ind_start_trials < 5) %
%             
                idx_percent = 100*strange_diff_ind/all_blocktrials;
                indx_strange_event =  length(events) *idx_percent /100
                if strcmp(subj_id,      '19801021mrhl') && iFile == 7
                   warning('too early button press! - trial structure manually checked');
                else
                    if strange_diff_ind > 0
                        error(['check for missing trial onset marker close to ' num2str(indx_strange_event)  'in events and replace it in the data if possible']);
                    end
                end
                
                
            end
           
            % check for trial onset code
            if events(idx_events).value ~= trial_onset_code                
                continue;
            else
                idx_trials = idx_trials + 1;
                timing.trialonset = events(idx_events).sample;
            end

            % check that response was not given following the start of the 
            % next trial (if so: replace sound code by '666' to indicate
            % invalid trial)
            trigger_type = events(idx_events + 1).type;
            if strcmp(trigger_type, rt_onset_datapixx_code(1)) || strcmp(trigger_type, rt_onset_datapixx_code(2))
                % summary_trials(end+1, :) = [idx_trials, iFile, trial_onset_code, timing.trialonset, sound_trigger, timing.sound, target_trigger, timing.target, response_trigger, response_correct, rt,  response_trigger_2nd, sound_deviant, sound_right, target_right, snd_target_locations_congruent];
                summary_trials(end+1, :) = [idx_trials, iFile, trial_onset_code, timing.trialonset, 0, 0, 0,0, 0,0 , 0,  0, 0, 0, 0, 0 ];

                % trial_matrix(end+1, :) = [idx_trials, iFile, trial_onset_code, timing.sound, sound_trigger, sound_deviant, sound_right, target_right, snd_target_locations_congruent, response_trigger, response_correct, rt];
                trial_matrix(end+1, :) = [idx_trials, iFile, trial_onset_code, 0, 666, 0, 0, 0, 0, 0, 0, 0, 0, 0];

                continue
            end
            
                        
            % check for sound code
            sound_trigger = events(idx_events + 1).value;
            if ~ismember(sound_trigger, sound_onset_code)
                 % check whether for unknown reason, another (maybe undefined) trigger (i.e. no response code 5013 or 5014) occurs
                % a few ms after cue onset and before the actual sound
                % trigger (no reason to throw trial away!)
                trigger_next_event = events(idx_events+2).value; % this should be a sound
                samples_trial_onset = events(idx_events).sample; % this should be sound onset
                samples_next_event = events(idx_events + 2).sample; % should be samples of sound
                if strcmp(events(idx_events+1).type,'Trigger') & ismember(trigger_next_event, sound_onset_code) & ismember(samples_next_event -  samples_trial_onset, valid_range_soa_onset_oddball)
                    idx_events = idx_events + 1;
                     timing.sound = events(idx_events + 1).sample;
                elseif ismember(events(idx_events + 1).value, datapixx_codes ) % check whether the response was given before the target of the current trial shows up
                    % if so: replace the sound code by the code 666 to indicate invalid trial
                        
                        % summary_trials(end+1, :) = [idx_trials, iFile, trial_onset_code, timing.trialonset, sound_trigger, timing.sound, target_trigger, timing.target, response_trigger, response_correct, rt,  response_trigger_2nd, sound_deviant, sound_right, target_right, snd_target_locations_congruent];
                        summary_trials(end+1, :) = [idx_trials, iFile, trial_onset_code, timing.trialonset, 666, 0, 0,0, 0,666 , 0,  0, 0, 0, 0, 0 ];
                        
                        % trial_matrix(end+1, :) = [idx_trials, iFile, trial_onset_code, timing.sound, sound_trigger, sound_deviant, sound_right, target_right, snd_target_locations_congruent, response_trigger, response_correct, rt];
                        trial_matrix(end+1, :) = [idx_trials, iFile, trial_onset_code, 0, 666, 0, 0, 0, 0, 0, 666, 0, 0, 0];

                        continue
                else
                    error('trigger does not code for sound onset. check why!')
                end
            else
                timing.sound = events(idx_events + 1).sample;
            end
            

            % check for target code
            target_trigger = events(idx_events + 2).value;   % should either be 88 or 99
            if ~ismember(target_trigger, target_onset_code)
                if strcmp(events(idx_events + 2).type, rt_onset_datapixx_code(1)) || strcmp(events(idx_events + 2).type, rt_onset_datapixx_code(2))
                    % summary_trials(end+1, :) = [idx_trials, iFile, trial_onset_code, trial_onset_code, timing.trialonset, sound_trigger, timing.sound, target_trigger, timing.target, response_trigger, response_correct, rt,  response_trigger_2nd, sound_deviant, sound_right, target_right, snd_target_locations_congruent];
                    summary_trials(end+1, :) = [idx_trials, iFile, trial_onset_code, timing.trialonset, sound_trigger, timing.sound, target_trigger, timing.target, 0, 0, 0,  0, 0, 0, 0, 0 ];

                    % trial_matrix(end+1, :) = [idx_trials, iFile, trial_onset_code, timing.sound, sound_trigger, sound_deviant, sound_right, target_right, snd_target_locations_congruent, response_trigger, response_correct, rt];
                    trial_matrix(end+1, :) = [idx_trials, iFile, trial_onset_code, timing.sound, sound_trigger, 0, 0, 0, 0, 0, 0, 0, target_trigger, 0];
                    continue
                else
                    disp('++++++++++++++++++++++++++');
                    disp(['check event ' num2str(idx_events)]);
                    disp('');
                    error('trigger does not code for target onset. check why via checking idx_events in events!')
                end
            else
                timing.target = events(idx_events + 2).sample;
            end

            % check for rt code
            rt_trigger = events(idx_events + 3).type;
            response_codex = events(idx_events + 3).value;
            if ~strcmp(rt_trigger, rt_onset_datapixx_code) % if no response: continue with event loop
                rt = 0;
                if strcmp(rt_trigger, trial_onset_code)
                    %                     summary_trials = [summary_trials; idx_trials, iFile, trial_onset_code, trial_onset_code, timing.trialonset, sound_trigger, timing.sound, target_trigger, timing.target, response_trigger, response_correct, rt,  response_trigger_2nd ];
                    summary_trials(end+1, :) = [idx_trials, iFile, trial_onset_code, trial_onset_code, timing.trialonset, sound_trigger, timing.sound, target_trigger, timing.target, 0, 0, rt,  0, 0, 0, 0, 0 ];

                    % trial_matrix(end+1, :) = [idx_trials, iFile, trial_onset_code, timing.sound, sound_trigger, sound_deviant, sound_right, target_right, snd_target_locations_congruent, response_trigger, response_correct, rt];
                    trial_matrix(end+1, :) = [idx_trials, iFile, trial_onset_code, timing.sound, sound_trigger, 0, 0, 0, 0, 0, 0, rt, target_trigger, timing.target]; % rt = 0

                    continue
                elseif ismember(response_codex, response_code)
                    % summary_trials(end+1, :) = [idx_trials, iFile, trial_onset_code, timing.trialonset, sound_trigger, timing.sound, target_trigger, timing.target, response_trigger, response_correct, rt,  response_trigger_2nd, sound_deviant, sound_right, target_right, snd_target_locations_congruent];
                    summary_trials(end+1, :) = [idx_trials, iFile, trial_onset_code, timing.trialonset, sound_trigger, timing.sound, target_trigger, timing.target, 0, 0, rt,  0, 0, 0, 0, 0 ];

                    % trial_matrix(end+1, :) = [idx_trials, timing.sound, sound_trigger, sound_deviant, sound_right, target_right, snd_target_locations_congruent, response_trigger, response_correct, rt];
                    trial_matrix(end+1, :) = [idx_trials, iFile, trial_onset_code, timing.sound, sound_trigger, 0, 0, 0, 0, 0, 0, rt,target_trigger, timing.target]; % rt = 0
                    continue
                else
                    error('trigger does not code for rt onset. check why!')
                end
            else % otherwise save rt
                timing.response = events(idx_events + 3).sample;
                rt = timing.response - timing.target;
            end

            % only use trials in which responses were given in response window
            if rt <= 100
                rt = 0;
            elseif rt > 1000
                rt = 0;
            end

            % check whether response is correct or false
            response_trigger = events(idx_events + 4).value;  % response code: correct or false
            if ~ismember(response_trigger, response_code)
                trigger_type = events(idx_events + 4).type;
                if strcmp(trigger_type, rt_onset_datapixx_code(1)) || strcmp(trigger_type, rt_onset_datapixx_code(2)) % if participant responded twice
                    % summary_trials(end+1, :) = [idx_trials, iFile, trial_onset_code, timing.trialonset, sound_trigger, timing.sound, target_trigger, timing.target, response_trigger, response_correct, rt,  response_trigger_2nd, sound_deviant, sound_right, target_right, snd_target_locations_congruent];
                    summary_trials(end+1,:) = [idx_trials, iFile, trial_onset_code, timing.trialonset, sound_trigger, timing.sound, target_trigger, timing.target, response_trigger, 0, rt,  0, 0, 0, 0, 0 ];

                    % trial_matrix(end+1, :) = [idx_trials, iFile, trial_onset_code, timing.sound, sound_trigger, sound_deviant, sound_right, target_right, snd_target_locations_congruent, response_trigger, response_correct, rt];
                    trial_matrix(end+1,:) = [idx_trials, iFile, trial_onset_code, timing.sound, sound_trigger, 0, 0, 0, 0, response_trigger, 0, 0, target_trigger, timing.target];
                    continue;
                else
                    error('trigger does not code for response code. check why!')
                end
            else
                if ismember(response_trigger, response_code_correct)
                    response_correct = 1;
                elseif ismember(response_trigger, response_code_incorrect) || ismember(response_trigger, response_code_noresponse)
                    response_correct = 0;
                    rt = 0;
                end

                if idx_trials ~= all_blocktrials % if it is NOT the last trial in the file / block
                    response_trigger_2nd = events(idx_events + 5).value;
                    if ismember(response_trigger_2nd, response_code)
                        warning('participant responded two times. trial invalid.')
                        response_correct = 0;
                    end
                end
            end

            % when trial structure is fine, check remaining stuff: sounds_right, sounds_deviant, targets_right, snd_target_locations_congruent
            if ismember(sound_trigger, novels_right)
                sound_right = true; % code 1
            else
                sound_right = false; % code 0
            end

            if ismember(sound_trigger, sounds_deviant)
                sound_deviant = true; % code 1
            else
                sound_deviant = false; % code 0
            end

            if ismember(sound_trigger, targets_right) % note: sound trigger also codes for location of target!
                target_right = true; % code 1
            else
                target_right = false; % code 0
            end

            if sound_right == target_right
                snd_target_locations_congruent = true;
            else
                snd_target_locations_congruent = false;
            end

            % save summary of trials for control purposes
            summary_trials(end+1, :) = [idx_trials, iFile, trial_onset_code, timing.trialonset, sound_trigger, timing.sound, target_trigger, timing.target, response_trigger, response_correct, rt,  response_trigger_2nd, sound_deviant, sound_right, target_right, snd_target_locations_congruent];

            trial_matrix(end+1, :) = [idx_trials, iFile, trial_onset_code, timing.sound, sound_trigger, sound_deviant, sound_right, target_right, snd_target_locations_congruent, response_trigger, response_correct, rt, target_trigger,timing.target];

        end % while loop

        summary_trials_all{iFile} = summary_trials;
        trialinfo{iFile} = trial_matrix;

    end % file / block loop


    %% save output
    output_filename = [subj_id '_trialinfo'];
    if ~exist(trialinfo_path,'dir')
        mkdir(trialinfo_path)
    end
    save(fullfile(trialinfo_path, output_filename), 'summary_trials_all', 'trialinfo');

end
