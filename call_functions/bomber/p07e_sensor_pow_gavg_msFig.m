% plot TF sensor effects

clear all global;
close all;

% init obob_ownft...
addpath('/mnt/obob/obob_ownft/');

cfg=[];
obob_init_ft;

addpath('/mnt/obob/staff/aweise/data/audifly/scripts/');
addpath('/mnt/obob/staff/aweise/templates/');

%% % general graphics, this will apply to any figure you open
% (groot is the default figure object).
set(groot, ...
    'DefaultFigureColor', 'w', ...
    'DefaultAxesLineWidth', 0.8, ...
    'DefaultAxesXColor', 'k', ...
    'DefaultAxesYColor', 'k', ...
    'DefaultAxesFontUnits', 'points', ...
    'DefaultAxesFontSize', 14, ...
    'DefaultAxesFontName', 'Calibri', ...
    'DefaultLineLineWidth', 1, ...
    'DefaultTextFontUnits', 'Points', ...
    'DefaultTextFontSize', 14, ...
    'DefaultTextFontName', 'Calibri', ...
    'DefaultAxesBox', 'off', ...
    'DefaultAxesTickLength', [0.02 0.025]);

% set the tickdirs to go out - need this specific order
set(groot, 'DefaultAxesTickDir', 'out');
set(groot, 'DefaultAxesTickDirMode', 'manual');

% perceptually great colormap
colorcetmap= functions.colorcet('BWRA');
colorcetmap_hot = colorcetmap(129:end,:);
colorcetmap_cold = colorcetmap(1:128,:);
% colormap(colorcetmap); % Plot colormap

%    ------------------- FIX PARAMETER! --------------------------

logtransform = 'yes';
sss = true;
windowType = 'wavelet';
chooseCycles = 5; 

%    ------------------- SPECIFY PARAMETER! --------------------------

induced = false; 

minusAvgTimelock = true; 

choose_roi = 'left'; % 'left', 'right'

%    ----------------------------------------------------------------

if strcmp(logtransform,'yes')
    inpath = [ '/mnt/obob/staff/aweise/data/audifly/data/meg/sensor/pow/all_sensors/' windowType  '/' num2str(chooseCycles) 'cycles/logtrans_yes/loglast/gavg/'];
end

if induced
    if strcmp(logtransform,'yes')
        inpath_stats_montecarlo= ['/mnt/obob/staff/aweise/data/audifly/data/meg/sensor/pow/all_sensors/wavelet/5cycles/logtrans_yes/loglast/induced/stat_ericmaris/montecarlo/10000/'];
        picpath = [ '/mnt/obob/staff/aweise/data/audifly/data/meg/sensor/pow/all_sensors/' windowType  '/' num2str(chooseCycles) 'cycles/logtrans_yes/loglast/induced/ms/pics/'];
    end
elseif minusAvgTimelock
      if strcmp(logtransform,'yes')
          inpath_stats_montecarlo = ['/mnt/obob/staff/aweise/data/audifly/data/meg/sensor/pow/all_sensors/wavelet/5cycles/logtrans_yes/loglast/minusAvgTimelock/stat_ericmaris/montecarlo/10000/'];
           picpath = [ '/mnt/obob/staff/aweise/data/audifly/data/meg/sensor/pow/all_sensors/' windowType  '/' num2str(chooseCycles) 'cycles/logtrans_yes/loglast/minusAvgTimelock/ms/pics/'];     
      end
end


if induced
    if sss
        % load gavg data
        load([inpath 'gavgN31_sound_dev_left_sss.mat']);
        gavg.devL = data_gavg; clear data_gavg;
        
        load([inpath 'gavgN31_sound_dev_right_sss.mat']);
        gavg.devR = data_gavg; clear data_gavg;
        
    end
elseif minusAvgTimelock
    if sss
        % load gavg data
        load([inpath 'gavgN31_sound_dev_left_sss.mat']);
        gavg.devL = data_gavg_minus_timelockavg; clear data_gavg_minus_timelockavg;
        
        load([inpath 'gavgN31_sound_dev_right_sss.mat']);
        gavg.devR = data_gavg_minus_timelockavg; clear data_gavg_minus_timelockavg;
    end
end
      
    
% calc diff of gavg
if strcmp(logtransform,'yes')
    gavg.devL_devR = gavg.devL;
    gavg.devL_devR.powspctrm = gavg.devL.powspctrm - gavg.devR.powspctrm ;
    
    gavg.devR_devL = gavg.devR;
    gavg.devR_devL.powspctrm = gavg.devR.powspctrm - gavg.devL.powspctrm ;    
end


%% get the best channels from the cluster statistic
if strcmp(choose_roi , 'left')
      good_chan = {'MEG1842+1843','MEG1822+1823'};

elseif strcmp(choose_roi , 'right')
     good_chan =  { 'MEG2232+2233','MEG2212+2213' };     
end
% set variabes for the topoplot
startFreqTopo = 8;
endFreqTopo = 14;
startTimeTopo = 0.2;
endTimeTopo = 0.6;
good_chan_topo = {'MEG1842+1843','MEG1822+1823', 'MEG2232+2233','MEG2212+2213' };

% ********************************************************************************
% set variables for the time frequency plot
startTime = 0;
endTime = 0.7;
stepsTime = 0.1;
startFreq = 5;
endFreq = 30;
stepsFreq = 4;

singleplotName = ['singleplot_gavgN31_devL_devR_' choose_roi '_' num2str(length(good_chan)) 'chans_' num2str(startFreq) '_' num2str(endFreq) 'Hz_'  num2str(startTime) '_' num2str(endTime) 's_sss'];

topoplotName_gavg =['topoplot_gavgN31_devL_devR_'  num2str(startFreqTopo) '_' num2str(endFreqTopo) 'Hz_'  num2str(startTimeTopo) '_' num2str(endTimeTopo) 's_sss'];

%% plot topoplot  
% select relevant time range
close all;
cfg = [];
cfg.latency = [startTimeTopo endTimeTopo];
cfg.frequency = [startFreqTopo endFreqTopo];


gavg_topo = ft_selectdata(cfg, gavg.devL_devR );


% make plot
cfg = [];
cfg.colorbar = 'yes';
load('neuromag306cmb_helmet_modLabels');
cfg.layout	=    layout; 
cfg.parameter = 'powspctrm';
cfg.comment   = 'no';
cfg.style =  'straight_imsat'; 
cfg.gridscale = 300;
cfg.renderer = 'painters';
cfg.colormap = 'parula';
cfg.zlim = [-0.045 0.045];
cfg.colorbar =  'SouthOutside';
cfg.highlightsize      = 10;
cfg.highlightchannel   = good_chan_topo;
cfg.highlightsymbol    = '*';
cfg.highlight  = 'on'; 
cfg.parameter = 'powspctrm';

ft_hastoolbox('brewermap', 1);         % ensure this toolbox is on the path

cfg.colormap = colormap(flipud(brewermap(64,'RdBu'))); % change the colormap;
ft_topoplotTFR(cfg, gavg_topo); 

picpath_col = [picpath 'brewermap/'];
if ~exist(picpath_col,'dir')
    mkdir(picpath_col);
end
printfilename = fullfile(picpath_col, [topoplotName_gavg '.png']);
print(printfilename, '-dpng');
printfilename = fullfile(picpath_col, [topoplotName_gavg '.svg']);
print(printfilename, '-dsvg');


%% select relevant time range for single TF plot
cfg = [];
cfg.latency = [startTime endTime];
cfg.frequency = [startFreq endFreq];
cfg.channel = good_chan; 
cfg.avgoverchan = 'yes';
gavg_sel.devL_devR = ft_selectdata(cfg, gavg.devL_devR );
 

%% make nice paper plot
data = gavg_sel.devL_devR;
dep_var = squeeze(data.powspctrm);

figure();
imagesc(data.time, data.freq, dep_var);
xlim([startTime endTime]); % to hide the NaN values at the edges of the TFR
axis xy; % to have the y-axis extend upwards

% Label the principal axes:
xlabel('Time (s)');
ylabel('Frequency (Hz)');

% Set colour limits symmetric around zero:
clim = 0.06;%max(abs(dep_var(:)));
caxis([-clim clim]);

% add a colour bar, and label it:
h = colorbar();
ylabel(h, 'power');

% The finer time and frequency axes:
tim_interp = linspace(startTime, endTime, 512);
freq_interp = linspace(startFreq, endFreq, 512);

% We need to make a full time/frequency grid of both the original and
% interpolated coordinates. Matlab's meshgrid() does this for us:
[tim_grid_orig, freq_grid_orig] = meshgrid(data.time, data.freq);
[tim_grid_interp, freq_grid_interp] = meshgrid(tim_interp, freq_interp);

% And interpolate:
dep_var_interp = interp2(tim_grid_orig, freq_grid_orig, dep_var,...
    tim_grid_interp, freq_grid_interp, 'spline');

% figure('units','normalized','outerposition',[0 0 1 1]); % full size figure
imagesc(tim_interp, freq_interp, dep_var_interp);
xlim([startTime endTime]); %xtick(startTime:0.1:endTime);
axis xy;
xlabel('Time (s)');
ylabel('Frequency (Hz)');
clim = 0.06;%max(abs(dep_var(:)));
caxis([-clim clim]);
h = colorbar();
ylabel(h, 'log-transformed power');

% Let's also add a line at t = 0s for clarity:
hold on;
plot(zeros(size(freq_interp)), freq_interp, 'k:');


ft_hastoolbox('brewermap', 1);         % ensure this toolbox is on the path
colormap(flipud(brewermap(64,'RdBu'))) % change the colormap


picpath_col = [picpath 'brewermap/'];
if ~exist(picpath_col,'dir')
    mkdir(picpath_col);
end
printfilename = fullfile(picpath_col, [singleplotName '.png']);
% print(printfilename, '-dpng');
printfilename = fullfile(picpath_col, [ singleplotName '.svg' ]);
% print(printfilename, '-dsvg');

