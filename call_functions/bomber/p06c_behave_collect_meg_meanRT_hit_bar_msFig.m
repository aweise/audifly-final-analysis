

clear all global;
close all;

% general graphics, this will apply to any figure you open
% (groot is the default figure object).
set(groot, ...
'DefaultFigureColor', 'w', ...
'DefaultAxesLineWidth', 0.5, ...
'DefaultAxesXColor', 'k', ...
'DefaultAxesYColor', 'k', ...
'DefaultAxesFontUnits', 'points', ...
'DefaultAxesFontSize', 11, ...
'DefaultAxesFontName', 'TimesNewRoman', ...
'DefaultLineLineWidth', 1, ...
'DefaultTextFontUnits', 'Points', ...
'DefaultTextFontSize', 11, ...
'DefaultTextFontName', 'TimesNewRoman', ...
'DefaultAxesBox', 'off', ...
'DefaultAxesTickLength', [0.02 0.025]);
 
% set the tickdirs to go out - need this specific order
set(groot, 'DefaultAxesTickDir', 'out');
set(groot, 'DefaultAxesTickDirMode', 'manual');

%% paths
addpath('/mnt/obob/staff/aweise/toolbox/Tools_anneurai/plotting/cbrewer/');
addpath('/mnt/obob/staff/aweise/toolbox/Tools_anneurai/plotting/violinPlot/');
addpath('/mnt/obob/staff/aweise/toolbox/Tools_anneurai/plotting/');
res_path = '/mnt/obob/staff/aweise/data/audifly/data/meg/trialinfo/sta_binaural/res/';

outPath = [res_path 'pics/sta_vs_dev/'];
if ~exist(outPath,'dir')
    mkdir(outPath)
end


%% paths and variables
addpath('/mnt/obob/staff/aweise/data/audifly/scripts/');
trialinfo_sorted_path = '/mnt/obob/staff/aweise/data/audifly/data/meg/trialinfo/sta_binaural/sorted/';
res_path = '/mnt/obob/staff/aweise/data/audifly/data/meg/trialinfo/sta_binaural/res/';

if ~exist(res_path, 'dir')
    mkdir(res_path)
end

%%
data_type ='meg';
subj_ids = functions.get_subject_ids(data_type);

nSubjects = length(subj_ids);

input_filename = ['res_devL_devR_devLR_vs_sta_N' num2str(nSubjects)];
load(fullfile(res_path, input_filename),  'mean_reaction_time_coll', 'detection_rate_coll' );


% bar plots: dev both target locations vs. sta
close all;
figure('units','normalized','outerposition',[0 0 1 1]);
% set(gcf, 'PaperPositionMode', 'auto');

dat(:, 1) = mean_reaction_time_coll.devL;
dat(:, 2) = mean_reaction_time_coll.sta_b_devL;

mean_devL = mean(dat(:, 1) )
SEM_devL = std(dat(:, 1))/sqrt(length(dat(:, 1)))

mean_sta_b_devL= mean(dat(:, 2) )
SEM_sta_b_devL = std(dat(:, 2))/sqrt(length(dat(:, 2)))


% define colors
colors = cbrewer('qual', 'Set1', 10);

% close all 
subplot(3,9,1); 
hold on;
%  each bar has different color
for b = 1:size(dat, 2)
    bar(b, mean(dat(:,b)), 'FaceColor',  colors(b, : ), 'EdgeColor', 'none', 'BarWidth', 0.6);
end
 
% show SEM 
h = ploterr(1:2, mean(dat), [], std(dat)/sqrt(length(dat)), 'k.', 'abshhxy', 0);
set(h(1), 'marker', 'none'); 
 
set(gca, 'xtick', [1 2], 'xticklabel', {'Deviant Left', 'Standard'}, ...
    'xticklabelrotation', 35, 'xlim', [0.5 2.5]);
ylabel('RT in ms'); xlabel('Condition');
set(gca, 'ytick', [100 200 300 400], 'ylim', [0 400]);



dat(:, 1) = mean_reaction_time_coll.devR;
dat(:, 2) = mean_reaction_time_coll.sta_b_devR;

mean_devR = mean(dat(:, 1) )
SEM_devR = std(dat(:, 1))/sqrt(length(dat(:, 1)))

mean_sta_b_devR= mean(dat(:, 2) )
SEM_sta_b_devR = std(dat(:, 2))/sqrt(length(dat(:, 2)))


mean_devLR = (mean_devL + mean_devR)/2
SEM_devLR = (SEM_devL + SEM_devR)/2

mean_sta_b_devLR = (mean_sta_b_devL + mean_sta_b_devR)/2
SEM_sta_b_devLR = (SEM_sta_b_devL + SEM_sta_b_devR)/2


% define colors
colors = cbrewer('qual', 'Set1', 10);

 
% subplot(1,2,2); 
subplot(3,9,2);
hold on;
%  each bar has different color
for b = 1:size(dat, 2)
    bar(b, mean(dat(:,b)), 'FaceColor',  colors(b, : ), 'EdgeColor', 'none', 'BarWidth', 0.6);
end
 
% show SEM 
h = ploterr(1:2, mean(dat), [], std(dat)/sqrt(length(dat)), 'k.', 'abshhxy', 0);
set(h(1), 'marker', 'none'); 
 
set(gca, 'xtick', [1 2], 'xticklabel', {'Deviant Right', 'Standard'}, ...
     'xticklabelrotation', 35, 'xlim', [0.5 2.5]);
ylabel('RT in ms'); xlabel('Condition');
set(gca, 'ytick', [100 200 300 400], 'ylim', [0 400]);


% bar plots - hit rate: dev both target locations vs. sta
dat(:, 1) = detection_rate_coll.devL;
dat(:, 2) = detection_rate_coll.sta_b_devL;

mean_devL = mean(dat(:, 1) )
SEM_devL = std(dat(:, 1))/sqrt(length(dat(:, 1)))

mean_sta_b_devL= mean(dat(:, 2) )
SEM_sta_b_devL = std(dat(:, 2))/sqrt(length(dat(:, 2)))

% define colors
colors = cbrewer('qual', 'Set1', 10);

% close all 
% subplot(1,2,1);
subplot(3,9,4);
hold on;
%  each bar has different color
for b = 1:size(dat, 2)
    bar(b, mean(dat(:,b)), 'FaceColor',  colors(b, : ), 'EdgeColor', 'none', 'BarWidth', 0.6);
end
 
% show SEM 
h = ploterr(1:2, mean(dat), [], std(dat)/sqrt(length(dat)), 'k.', 'abshhxy', 0);
set(h(1), 'marker', 'none'); 
 
set(gca, 'xtick', [1 2], 'xticklabel', {'Deviant Left', 'Standard'}, ...
    'xticklabelrotation', 35, 'xlim', [0.5 2.5]);
ylabel('Hit rate'); xlabel('Condition');
set(gca, 'ytick', [0 0.5 1], 'ylim', [0 1]);

%
dat(:, 1) = detection_rate_coll.devR;
dat(:, 2) = detection_rate_coll.sta_b_devR;

mean_devR = mean(dat(:, 1) )
SEM_devR = std(dat(:, 1))/sqrt(length(dat(:, 1)))

mean_sta_b_devR= mean(dat(:, 2) )
SEM_sta_b_devR = std(dat(:, 2))/sqrt(length(dat(:, 2)))

mean_devLR = (mean_devL + mean_devR)/2
SEM_devLR = (SEM_devL + SEM_devR)/2

mean_sta_b_devLR = (mean_sta_b_devL + mean_sta_b_devR)/2
SEM_sta_b_devLR = (SEM_sta_b_devL + SEM_sta_b_devR)/2


% define colors
colors = cbrewer('qual', 'Set1', 10);

 subplot(3,9,5);
% subplot(1,2,2); 
% % subpot(3,4,2)
hold on;
%  each bar has different color
for b = 1:size(dat, 2)
    bar(b, mean(dat(:,b)), 'FaceColor',  colors(b, : ), 'EdgeColor', 'none', 'BarWidth', 0.6);
end
 
% show SEM 
h = ploterr(1:2, mean(dat), [], std(dat)/sqrt(length(dat)), 'k.', 'abshhxy', 0);
set(h(1), 'marker', 'none'); 
 
set(gca, 'xtick', [1 2], 'xticklabel', {'Deviant Right', 'Standard'}, ...
    'xticklabelrotation', 35, 'xlim', [0.5 2.5]);
ylabel('Hit rate'); xlabel('Condition');
set(gca, 'ytick', [0 0.5 1], 'ylim', [0 1]);


%
input_filename = ['res_devL_devR_devLR_con_vs_incon_N' num2str(nSubjects)];
load(fullfile(res_path, input_filename),   'mean_reaction_time_coll' , 'detection_rate_coll');

% bar plots
dat(:, 1) = mean_reaction_time_coll.devLcon;
dat(:, 2) = mean_reaction_time_coll.devLincon;

mean_devLcon = mean(dat(:, 1) )
SEM_devLcon = std(dat(:, 1))/sqrt(length(dat(:, 1)))

mean_devLincon= mean(dat(:, 2) )
SEM_devLincon = std(dat(:, 2))/sqrt(length(dat(:, 2)))

% define colors
colors = cbrewer('qual', 'Set1', 10);

% close all 
% subplot(1,2,1); 
 subplot(3,9,10);
hold on;
%  each bar has different color
for b = 1:size(dat, 2)
    bar(b, mean(dat(:,b)), 'FaceColor',  colors(b, : ), 'EdgeColor', 'none', 'BarWidth', 0.6);
end
 
% show SEM 
h = ploterr(1:2, mean(dat), [], std(dat)/sqrt(length(dat)), 'k.', 'abshhxy', 0);
set(h(1), 'marker', 'none'); 
 
set(gca, 'xtick', [1 2], 'xticklabel', {'DevLCon', 'DevLIncon'}, ...
   'xticklabelrotation', 35,  'xlim', [0.5 2.5]);
ylabel('RT in ms'); xlabel('Condition');
set(gca, 'ytick', [100 200 300 400], 'ylim', [0 400]);

% if these data are paired, show the differences
% plot(dat', '.k-', 'linewidth', 0.2, 'markersize', 2);
 
dat(:, 1) = mean_reaction_time_coll.devRcon;
dat(:, 2) = mean_reaction_time_coll.devRincon;

mean_devRcon = mean(dat(:, 1) )
SEM_devRcon = std(dat(:, 1))/sqrt(length(dat(:, 1)))

mean_devRincon= mean(dat(:, 2) )
SEM_devRincon = std(dat(:, 2))/sqrt(length(dat(:, 2)))

mean_devCon = (mean_devLcon + mean_devRcon)/2
SEM_devCon = (SEM_devLcon + SEM_devRcon)/2

mean_devIncon = (mean_devLincon + mean_devRincon)/2
SEM_devIncon = (SEM_devLincon + SEM_devRincon)/2


% define colors
colors = cbrewer('qual', 'Set1', 10);

 
subplot(3,9,11);
hold on;
%  each bar has different color
for b = 1:size(dat, 2)
    bar(b, mean(dat(:,b)), 'FaceColor',  colors(b, : ), 'EdgeColor', 'none', 'BarWidth', 0.6);
end
 
% show SEM 
h = ploterr(1:2, mean(dat), [], std(dat)/sqrt(length(dat)), 'k.', 'abshhxy', 0);
set(h(1), 'marker', 'none'); 
 
set(gca, 'xtick', [1 2], 'xticklabel', {'DevRCon', 'DevRIncon'}, ...
    'xticklabelrotation', 35, 'xlim', [0.5 2.5]);
ylabel('RT in ms'); xlabel('Condition');
set(gca, 'ytick', [100 200 300 400], 'ylim', [0 400]);


% bar plots - hit rate: 
dat(:, 1) = detection_rate_coll.devLcon;
dat(:, 2) = detection_rate_coll.devLincon;


mean_devLcon = mean(dat(:, 1) )
SEM_devLcon = std(dat(:, 1))/sqrt(length(dat(:, 1)))

mean_devLincon= mean(dat(:, 2) )
SEM_devLincon = std(dat(:, 2))/sqrt(length(dat(:, 2)))



% define colors
colors = cbrewer('qual', 'Set1', 10);

subplot(3,9,13);

hold on;
%  each bar has different color
for b = 1:size(dat, 2)
    bar(b, mean(dat(:,b)), 'FaceColor',  colors(b, : ), 'EdgeColor', 'none', 'BarWidth', 0.6);
end
 
% show SEM 
h = ploterr(1:2, mean(dat), [], std(dat)/sqrt(length(dat)), 'k.', 'abshhxy', 0);
set(h(1), 'marker', 'none'); 
 
set(gca, 'xtick', [1 2], 'xticklabel', {'DevLCon', 'DevLIncon'}, ...
    'xticklabelrotation', 35, 'xlim', [0.5 2.5]);
ylabel('Hit rate'); xlabel('Condition');
set(gca, 'ytick', [0 0.5 1], 'ylim', [0 1]);

%
dat(:, 1) = detection_rate_coll.devRcon;
dat(:, 2) = detection_rate_coll.devRincon;

mean_devRcon = mean(dat(:, 1) )
SEM_devRcon = std(dat(:, 1))/sqrt(length(dat(:, 1)))

mean_devRincon= mean(dat(:, 2) )
SEM_devRincon = std(dat(:, 2))/sqrt(length(dat(:, 2)))


mean_devCon = (mean_devLcon + mean_devRcon)/2
SEM_devCon = (SEM_devLcon + SEM_devRcon)/2

mean_devIncon = (mean_devLincon + mean_devRincon)/2
SEM_devIncon = (SEM_devLincon + SEM_devRincon)/2

% define colors
colors = cbrewer('qual', 'Set1', 10);

subplot(3,9,14); 
hold on;
%  each bar has different color
for b = 1:size(dat, 2)
    bar(b, mean(dat(:,b)), 'FaceColor',  colors(b, : ), 'EdgeColor', 'none', 'BarWidth', 0.6);
end
 
% show SEM 
h = ploterr(1:2, mean(dat), [], std(dat)/sqrt(length(dat)), 'k.', 'abshhxy', 0);
set(h(1), 'marker', 'none'); 
 
set(gca, 'xtick', [1 2], 'xticklabel', {'DevRCon', 'DevRIncon'}, ...
    'xticklabelrotation', 35, 'xlim', [0.5 2.5]);
ylabel('Hit rate'); xlabel('Condition');
set(gca, 'ytick', [0 0.5 1], 'ylim', [0 1]);



% print figure 
% printfilename = fullfile(outPath, ['msFig_barPlot_hit_rt_dev_sta_con_incon_' data_type ] );
% print(printfilename, '-dpng');
% print( '-painters',printfilename, '-dsvg');
