%% clear
clear all global
close all

%NOTE:  % subj_id = subj_ids{26}; block = 4 only! --> no maxfiltering possible,


%% init obob_ownft
addpath('/mnt/obob/obob_ownft/');

cfg = [];

obob_init_ft(cfg);

%% set variables...
sss = true; %  update: 3/4/2019: adopted to identifiy components for maxfiltered data
data_type = 'meg';
subj_ids = functions.get_subject_ids(data_type);

%% DEFINE DATA TO USE
% ______________________________________________________________________
% disp('******************************************************************');
% disp('*');
% warning('your input required');
% disp('*');
% disp('******************************************************************');
% prompt = 'Define subj_id and block to use! [y]: ';
% str = input(prompt,'s');
% if isempty(str)
%     str = 'Y';
% end

%% ------------------------------------------
subj_id = subj_ids{31};%'19951120mrbr'; %  
block = 1
% too many bad channels
disp(['block0' num2str(block)]);
rejected_components = [];

%  set paths and load data....

inpath = ['/mnt/obob/staff/aweise/data/audifly/data/meg/preproc/ica/' subj_id '/'];
outpath = ['/mnt/obob/staff/aweise/data/audifly/data/meg/preproc/ica/' subj_id '/'];

if sss
    load(fullfile(inpath, ['file_0' num2str(block) '_sss']), 'ica_components', 'subj_id', 'block');
else
    load(fullfile(inpath, ['file_0' num2str(block)]), 'ica_components', 'subj_id', 'block');
end

% inspect components...
cfg = [];
cfg.blocksize = 20; % 20 seconds of data segments to look at ( decrease to 10 or xxx in case this is more convenient)
cfg.viewmode = 'component';
cfg.layout = 'neuromag306mag.lay';

ft_databrowser(cfg, ica_components);
% set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]); % get fullscreen figure of databrowser

% % In case you want to plot the power spectrum to get a better idea of the potential artifact do:
% cfg = [];
% cfg.path = outpath;
% cfg.prefix = ['file_0' num2str(block) '_rej_comp'];
% cfg.layout = 'neuromag306mag.lay';
% 
% % [rej_comp] = functions.jd_ft_icabrowser_adopted_for_aw(cfg, ica_components);
% functions.jd_ft_icabrowser_adopted_for_aw(cfg, ica_components);

% define components to be rejected and write them down manually to save them
% disp('******************************************************************');
% disp('*');
% warning('your input required');
% disp('*');
% disp('******************************************************************');
% prompt = 'Write down independent component you identified![y]: ';
% str = input(prompt,'s');
% if isempty(str)
%     str = 'Y';
% end

%%
rejected_components = []; % e.g. [1, 5];
disp('compontents to be rejected defined');

% save data...
if ~exist(outpath,'dir')
    mkdir(outpath);
end

if sss
     save(fullfile(outpath, ['file_0' num2str(block) '_sss_rej_comp']), 'rejected_components', 'subj_id', 'block');
else
    save(fullfile(outpath, ['file_0' num2str(block) '_rej_comp']), 'rejected_components', 'subj_id', 'block');
end
disp('data saved');

% delete components to be on the save side and not save it for the next block / subj!
% disp('******************************************************************');
% disp('*');
% warning('your input required');
% disp('*');
% disp('******************************************************************');
% prompt = 'Delete components to be on the save side and NOT save it for the next block / subj![y]: ';
% str = input(prompt,'s');
% if isempty(str)
%     str = 'Y';
% end

disp('******************************************************************');
disp('*');
warning('components saved. please, delete now and continue with nextblock/subj!');
disp('*');
disp('******************************************************************');
