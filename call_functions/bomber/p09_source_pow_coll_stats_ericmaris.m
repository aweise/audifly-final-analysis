clear all global;

% init obob_ownft...
addpath('/mnt/obob/obob_ownft_old/');

cfg=[];
cfg.package.svs=true;
cfg.package.gm2 = true;
obob_init_ft(cfg);

addpath('/mnt/obob/staff/aweise/toolbox/cbrewer/');
addpath('/mnt/obob/staff/aweise/data/audifly/scripts/');
addpath('/mnt/obob/staff/aweise/templates/');

chooseBrainOption =  'parcel'; 
load('aw_parcelLayout_3mm');


%% +++++++++++++++++++++++++++++++++++++++++++++

%    ------------------- FIX PARAMETER! --------------------------
logtransform = 'yes';
regfac = '5%'; 
chooseTFmethod = 'wavelet';
timeLimits = [0.35 0.6]; 
freqLimits = [8 14];
freqBand = 'alphaFreq';
chooseHemisphere = 'bothHemis';
pValue = 0.05;
leadfieldNormOption =  'no'; 
ica = true;
sss = true;
ssp = false;
choose_sensors =  'meggrad'; 
data_type =     'meg';
subj_ids = functions.get_subject_ids(data_type);
nSubjects = length(subj_ids);
contrastType = 'devL_devR'; 
analysisType =  'pow'
epoch = 'sound';  
avgovertime = 'yes'; % 'yes' or 'no'
avgoverfreq = 'yes';  % 'yes' or 'no'
brainAreas = [];
doPlot = 0;

statMethod = 'montecarlo'; 

if strcmp(statMethod, 'montecarlo')
    nRandomizations = 10000
    if nRandomizations ~= 10000
        warning(['number of randomization is set to: ' num2str(nRandomizations) ]);
    end
end

%    ------------------- SPECIFY PARAMETER! --------------------------
chooseMinusAvgTimelock = 'induced'; % 'induced', 'minusAvgTimelock';


%%
datapath = ['/mnt/obob/staff/aweise/data/audifly/data/meg/source/' chooseBrainOption '/' analysisType '/norm_' leadfieldNormOption '/' choose_sensors '/' chooseTFmethod '/logtrans_yes/'  ];
    
if strcmp(statMethod, 'montecarlo')
    outPath = [datapath  'stat_ericmaris/' statMethod '/' num2str(nRandomizations) '/' ];
end

if strcmp(chooseMinusAvgTimelock,'induced')
     outPath = [ outPath 'induced/'];
elseif strcmp(chooseMinusAvgTimelock,'minusAvgTimelock')
    outPath = [ outPath 'minusAvgTimelock/'];
end

if ~exist(outPath, 'dir')
    mkdir(outPath)
end

% create folder in which data will be stored if not existent
picsPath = [outPath '/pics/'];
if ~exist(picsPath, 'dir')
    mkdir(picsPath)
end
close all;


if strcmp(contrastType, 'devL_devR')
    contrastConds =  {'dev_left', 'dev_right' };
end


%% load data
for iCond = 1:length(contrastConds)
    
    xCond = contrastConds{iCond};
    
    data = cell(1, nSubjects); % prepares empty matrix
    
    for iSub = 1:nSubjects
        
        % load individual power data (averaged over trials) for each condition into empty matrix
        subj = subj_ids{iSub};
        inputfile = fullfile(datapath, [subj '_' epoch '_' xCond  '_sss_ica.mat'] );
        load(inputfile);  
        
        % collect data of all subjs
        
        if strcmp(chooseMinusAvgTimelock, 'minusAvgTimelock')
            data_minus_timelockavg{iSub} =  freq_minus_timelock_logpow;
        elseif strcmp(chooseMinusAvgTimelock, 'induced')
            data{iSub} = freq_logpow;
        end
            
    end
    
    if strcmp(chooseMinusAvgTimelock, 'minusAvgTimelock')
        contrast_conds.(xCond) = data_minus_timelockavg;
    elseif strcmp(chooseMinusAvgTimelock, 'induced')
        contrast_conds.(xCond) = data;    
    end
end


% normalize power and create structure with zero values for test statistic
for iSubj = 1:nSubjects
        devL_norm{iSubj} = contrast_conds.dev_left{iSubj};
        devR_norm{iSubj} = contrast_conds.dev_right{iSubj};        
end

%% create design matrix based on number of subjects and number of conditions
% N x numobservations: design matrix (for examples/advice, please see the Fieldtrip wiki,
%                                     especially cluster-permutation tutorial and the 'walkthrough' design-matrix section)
% ivar = 2 %  conditions
% uvar = 1 %  subject groups

study_design = [1:nSubjects, 1:nSubjects; ones(1,nSubjects),ones(1,nSubjects)*2];

%% get mni grid of 3 mm resolution (note: if you change the grid [resolution]: change it also for the analysis steps before)
load parcellations_3mm.mat; % layout for parcellation approach

%% generate neighbour structure for montecarlo statistic

grad = [];
grad.chanpos = parcellation.parcel_grid.pos;
grad.label = parcellation.parcel_grid.label;

cfg = [];
cfg.method        = 'triangulation';
cfg.grad = grad;
cfg.feedback = 'no'; % 'yes' if you want to see the plot of the neighbour structure or 'no'

channel_neighbours = ft_prepare_neighbours(cfg);
    

%% prepare cfg for source stats    
cfg = [];
cfg.channel = 'all';
cfg.latency = timeLimits;
cfg.frequency = freqLimits;
cfg.avgovertime = avgovertime;
cfg.avgoverfreq = avgoverfreq;
cfg.parameter   = 'powspctrm';
cfg.method      = 'montecarlo';
cfg.correctm    = 'cluster';
cfg.neighbours  = channel_neighbours;
cfg.design      = study_design;
cfg.uvar                = 1; % unit variables, i.e. subject groups
cfg.ivar                = 2; % independent variables, i.e. conditions
cfg.clusteralpha     = 0.05;
cfg.clusterstatistic = 'maxsum';
cfg.alpha            = 0.05;
cfg.numrandomization = nRandomizations; %
cfg.minnbchan        = 0;
cfg.statistic   = 'depsamplesT';
cfg.tail        = 0;
cfg.correcttail      = 'prob';
cfg.clustertail        = 0;


%% run stat function
data_stat = ft_freqstatistics(cfg, devL_norm{:}, devR_norm{:});


%%  remove cfg otherwise stat becomes extremely large (> xx GB range)
data_stat = rmfield(data_stat, 'cfg');


%% create stat field in which only significant T values are stored
data_stat.stat_masked = data_stat.stat .* data_stat.mask;
    
if isempty(data_stat.stat_masked(:,:,:))
    warning('no significant cluster found')
end

checkForSigNeg = find(data_stat.stat_masked(:,:,:) < 0) ; % get indices of significant effect
checkForSigPos = find(data_stat.stat_masked(:,:,:) > 0) ; % get indices of significant effect


if isfield(data_stat,'posclusters')
    posClusterPvalue = getfield(data_stat.posclusters, 'prob')
    if posClusterPvalue <= 0.05
        data_stat.stat_posClus = data_stat.stat .* (data_stat.posclusterslabelmat==1); % 1 marks the first cluster
    end
end

if isfield(data_stat, 'negclusters')
    negClusterPvalue = getfield(data_stat.negclusters, 'prob') % gives error message if field does not exist
    if negClusterPvalue <= 0.05
        data_stat.stat_negClus = data_stat.stat .* (data_stat.negclusterslabelmat==1); % 1 marks the first cluster
    end
end


data_stat.ind_chan_sig = find(data_stat.mask ~= 0);
data_stat.label_sign_chan = data_stat.label(data_stat.ind_chan_sig);

data_stat.ind_chan_posClus = find(data_stat.stat_posClus ~= 0);
data_stat.label_posClus_chan = data_stat.label(data_stat.ind_chan_posClus);

data_stat.ind_chan_negClus = find(data_stat.stat_negClus ~= 0);
data_stat.label_negClus_chan = data_stat.label(data_stat.ind_chan_negClus);


%% save data
outputfile = fullfile(outPath, ['data_stat_allN' num2str(nSubjects) '_sss_ica_' chooseHemisphere '_'  contrastType '_' choose_sensors '_' epoch '_' num2str(freqLimits(1)) '_' num2str(freqLimits(2)) 'Hz_'  num2str(timeLimits(1)) '_' num2str(timeLimits(2)) 's_avgTime' avgovertime '_avgFreq' avgoverfreq '.mat'] );     
save(outputfile,'data_stat');


