clear all global;
close all;

%% init obob_ownft...
addpath('/mnt/obob/obob_ownft/');
obob_init_ft;

addpath('/mnt/obob/staff/aweise/data/audifly/scripts/');
outpath = '/mnt/obob/staff/aweise/data/audifly/data/polhemus/';
mkdir(outpath);

data_type = 'meg';
subj_ids = functions.get_subject_ids(data_type); 
sss = true; 
block = 1;
 
for iSubj = 1 : length(subj_ids)
    
    subj_id =  subj_ids{iSubj };   
    
    fif_name = functions.get_fif_names( subj_id, block, sss);   
    
    headshape = ft_read_headshape(fif_name);
    headshape = ft_convert_units(headshape,'m') ;
    
    grad = ft_read_sens(fif_name, 'fileformat','neuromag_fif','senstype','meg','coordsys','head');
    grad = ft_convert_units(grad, 'm'); % use m!

    headshape_name = ['headshape_' subj_id '.mat'];
    
    save([outpath headshape_name], 'headshape', 'grad');
end