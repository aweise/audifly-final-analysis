
clear all global;

% init obob_ownft...
addpath('/mnt/obob/obob_ownft/');

cfg=[];
obob_init_ft;

addpath('/mnt/obob/staff/aweise/data/audifly/scripts/');
addpath('/mnt/obob/staff/aweise/templates/');

% general graphics, this will apply to any figure you open
% (groot is the default figure object).
set(groot, ...
    'DefaultFigureColor', 'w', ...
    'DefaultAxesLineWidth', 0.8, ...
    'DefaultAxesXColor', 'k', ...
    'DefaultAxesYColor', 'k', ...
    'DefaultAxesFontUnits', 'points', ...
    'DefaultAxesFontSize', 14, ...
    'DefaultAxesFontName', 'Calibri', ...
    'DefaultLineLineWidth', 2, ...
    'DefaultTextFontUnits', 'Points', ...
    'DefaultTextFontSize', 14, ...
    'DefaultTextFontName', 'Calibri', ...
    'DefaultAxesBox', 'off', ...
    'DefaultAxesTickLength', [0.02 0.025]);

% set the tickdirs to go out - need this specific order
set(groot, 'DefaultAxesTickDir', 'out');
set(groot, 'DefaultAxesTickDirMode', 'manual');

%% +++++++++++++++++++++++++++++++++++++++++++++

%    ------------------- FIX PARAMETER! --------------------------
logtransform = 'yes';
sss = true; 
chooseCycles = 5; 
chooseTFmethod = 'wavelet'; 
chooseMinusAvgTimelock=  'induced';

cond_names =  {'dev_left', 'dev_right'}; 

analysisWin = [0.2 0.6];

sel_sensors_left =  {'MEG1842+1843','MEG1822+1823'};
sel_sensors_right =  { 'MEG2232+2233','MEG2212+2213' };

choose_sensors = 'MEG1842_1822_2232_2212';

timeLimits = [-0.7 0.7]; % for plotting
freqLimits = [8 14];

avgovertime = 'no';

avgoverfreq = 'yes';

avgoverchan = 'yes';

analysisType =  'pow'
epoch = 'sound';  

data_type =  'meg';
subj_ids = functions.get_subject_ids(data_type);
nSubjects = length(subj_ids);


%%  +++++++++++++++++++++++++++++++++++++++++++++
if strcmp(logtransform, 'yes')
    datapath = ['/mnt/obob/staff/aweise/data/audifly/data/meg/sensor/' analysisType '/all_sensors/' chooseTFmethod '/' num2str(chooseCycles) 'cycles/logtrans_yes/loglast/' ];
end

%% create folder in which data will be stored if not existent
outPath = [datapath '/' chooseMinusAvgTimelock '/left_vs_right/pics_data/'];

if ~exist(outPath, 'dir')
    mkdir(outPath)
end


%% collect data of all subj
for iCond = 1:length(cond_names)
    
    xCond = cond_names{iCond};
    
    data = cell(1, nSubjects); % prepares empty matrix
    
    for iSub = 1:nSubjects
        
        % load individual power data (averaged over trials) for each condition into empty matrix
        subj = subj_ids{iSub};
        
        if sss
            inputfile = fullfile(datapath, [subj '_' epoch '_' xCond  '_sss.mat'] );            
        end
        
        load(inputfile);
        
        % collect data of all subjs
        if strcmp(chooseMinusAvgTimelock, 'induced')
            if strcmp(logtransform, 'yes')
                data{1,iSub} = freq_logpow;
            else
                data{1,iSub} = freq;
            end
        end
        
        % select data for plotting
        cfg = [];
        cfg.latency = timeLimits; 
        cfg.avgovertime = avgovertime;
        cfg.avgoverfreq =avgoverfreq;
        cfg.avgoverchan = avgoverchan;
        cfg.frequency= freqLimits;
        
        
        % process data                 
        cfg.channel = sel_sensors_left;
        
        data_plot_tmp.(xCond).left{iSub} = ft_selectdata(cfg,data{iSub} );
        data_plot_coll.(xCond).left(iSub,:) = data_plot_tmp.(xCond).left{iSub}.powspctrm;
        
        % ***************************************************
        
        cfg.channel = sel_sensors_right;
        
        data_plot_tmp.(xCond).right{iSub} = ft_selectdata(cfg,data{iSub} );
        data_plot_coll.(xCond).right(iSub,:) = data_plot_tmp.(xCond).right{iSub}.powspctrm;
      
    end
    
    %% calculate grand average for plotting   
     % process bsl data
     data_plot_gavg.(xCond).left(1,:) = mean(data_plot_coll.(xCond).left);
     
     data_plot_gavg.(xCond).right(1,:) = mean(data_plot_coll.(xCond).right);
    
end
 

%% plot data
timevect = [-0.700000000000000,-0.648000000000000,-0.600000000000000,-0.548000000000000,-0.500000000000000,-0.448000000000000,-0.400000000000000,-0.348000000000000,-0.300000000000000,-0.248000000000000,-0.200000000000000,-0.148000000000000,-0.100000000000000,-0.0480000000000000,0,0.0480000000000000,0.100000000000000,0.148000000000000,0.200000000000000,0.248000000000000,0.300000000000000,0.348000000000000,0.400000000000000,0.448000000000000,0.500000000000000,0.548000000000000,0.600000000000000,0.648000000000000,0.700000000000000];

figure (2);

 
subplot(1,2,1); 
hold on
plot(timevect,data_plot_gavg.dev_left.left(1,:), 'r', 'LineWidth',2)
plot(timevect,data_plot_gavg.dev_right.left(1,:), 'b', 'LineWidth',2);
xlim([-0.7 0.7]);
ylim([-20.8 -20.6]);
    

subplot(1,2,2); 
hold on
plot(timevect,data_plot_gavg.dev_left.right(1,:), 'b', 'LineWidth',2)
plot(timevect,data_plot_gavg.dev_right.right(1,:), 'r', 'LineWidth',2);
xlim([-0.7 0.7]);
ylim([-20.8 -20.6]);

printfilename = fullfile(outPath, ['left_vs_right_inc_vs_decN31_' choose_sensors '.png']);
% print(printfilename, '-dpng');
printfilename = fullfile(outPath, [ 'left_vs_right_inc_vs_decN31_' choose_sensors '.svg' ]);
% print(printfilename, '-dsvg');

