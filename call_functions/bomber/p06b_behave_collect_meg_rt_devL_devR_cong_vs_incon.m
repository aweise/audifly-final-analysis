clear all global;
close all;

%% paths
addpath('/mnt/obob/staff/aweise/data/audifly/scripts/call_functions/bomber');
addpath('/mnt/obob/staff/aweise/data/audifly/scripts/');

addpath('/mnt/obob/staff/aweise/toolbox/Tools_anneurai/plotting/cbrewer/');
addpath('/mnt/obob/staff/aweise/toolbox/Tools_anneurai/plotting/violinPlot/');
addpath('/mnt/obob/staff/aweise/toolbox/Tools_anneurai/plotting/');

trialinfo_sorted_path = '/mnt/obob/staff/aweise/data/audifly/data/meg/trialinfo/sta_binaural/sorted/';
trialinfo_path = '/mnt/obob/staff/aweise/data/audifly/data/meg/trialinfo/sta_binaural/';
res_path = '/mnt/obob/staff/aweise/data/audifly/data/meg/trialinfo/sta_binaural/res/';

if ~exist(res_path, 'dir')
    mkdir(res_path)
end

outPath = [res_path 'pics/sta_vs_dev/'];
if ~exist(outPath,'dir')
    mkdir(outPath)
end

% define subjects
data_type = 'meg';
subj_ids = functions.get_subject_ids(data_type);
nr_subject_id = 1;

nSubjects = length(subj_ids);

response_correct = 1;
sta_bef_dev = [120,130,140,150]; 
devL_con = [2]; % codes for deviants in congruent condition
devL_incon = [4]; % codes for deviants in incongruent condition
devR_con = [5]; % codes for deviants in congruent condition
devR_incon = [3]; % codes for deviants in incongruent condition
devLR_con = [2,5]; % codes for deviants in congruent condition
devLR_incon = [3,4]; % codes for deviants in incongruent condition

% devL = [2,4];
% devR = [3,5];
% devLR = [2,3,4,5];
% max_trials_oneLocation = 56;
% max_trials_bothLocations = 112;
min_rt = 100;
max_rt = 800;

vars = {'devLincon', 'devLcon', 'devRincon', 'devRcon', 'devLRincon', 'devLRcon'};

doPlot = 0;


%% re-structure data for behavioral analysis
% subj loop
for iSubj = nr_subject_id:length(subj_ids)
    
    subj_id = subj_ids{iSubj};
    subj_id_ext = ['subj_' subj_id];
    
    % load data
    input_filename = [subj_id '_trialinfo'];
    load([trialinfo_path input_filename]);
    
    % concatenate data of all blocks
    nBlocks = length(trialinfo);
    data_all = [];
    for iBlock = 1:nBlocks
        curr_data = trialinfo{iBlock};
        data_all = [data_all; curr_data];
    end
       
    ind_devLcon = find(ismember(data_all(:,5), devL_con));
    data.devLcon = data_all(ind_devLcon,:);
    
    ind_devLincon = find(ismember(data_all(:,5), devL_incon));
    data.devLincon = data_all(ind_devLincon,:);
    
    
    ind_devRcon = find(ismember(data_all(:,5), devR_con));
    data.devRcon = data_all(ind_devRcon,:);
   
    ind_devRincon = find(ismember(data_all(:,5), devR_incon));
    data.devRincon = data_all(ind_devRincon,:);
   
    ind_devLRcon = find(ismember(data_all(:,5), devLR_con));
    data.devLRcon = data_all(ind_devLRcon,:);
    
    ind_devLRincon = find(ismember(data_all(:,5), devLR_incon));
    data.devLRincon = data_all(ind_devLRincon,:);
    
   
    
    
    for iVar = 1:length(vars)
        % column 5: sound_code,
        % column 11: response_correct, % 1 = correct; 0 = incorrect; also is set to 0 if RT = 0 (because RT outside response time window, etc)
        % column 12: reaction_time
        % column 10: response code
        
        curr_var = vars{iVar}        
        % find suspicious trials and delete from analysis matrix
        ind_suspicious_trials = find(data.(curr_var)(:,12) == 0);%RT
        for iTr = 1:length(ind_suspicious_trials)
            curr_Tr = ind_suspicious_trials(iTr );
            % RT must not be zero when there was a correct response
            % (in that case RT had been coded in a previous analysis step to zero)
            % and those trials need to be removed from analysis here
            if data.(curr_var)(curr_Tr,11) == response_correct % if HIT for suspicious trial (i.e. with RT = zero): delete trial from analysis matrix!
                data.(curr_var)(curr_Tr,:)
                disp([ num2str(curr_Tr) 'will be deleted']);
                data.(curr_var)(curr_Tr,:)=[];
                ind_suspicious_trials = ind_suspicious_trials - 1;
            end
        end
        
        % if response code is missing [111,112,221,222] 
        % delete trial from analysis matrix!
        ind_suspicious_trials = find(data.(curr_var)(:,10) == 0);
        for iTr = 1:length(ind_suspicious_trials)
            curr_Tr = ind_suspicious_trials(iTr );
            if data.(curr_var)(curr_Tr,10) == 0 
                data.(curr_var)(curr_Tr,:)
                disp([ num2str(curr_Tr) 'will be deleted']);
                data.(curr_var)(curr_Tr,:)=[];
                ind_suspicious_trials = ind_suspicious_trials - 1;
            end
        end
               
       
        nr_trials = length(data.(curr_var)(:,11)); % all trial incl. incorrect responses
        
        indices_response_correct = find(data.(curr_var)(:,11) == response_correct);
        correct_responses = data.(curr_var)(indices_response_correct,:); % take RTs only from correct responses
        indices_correct_rt_win = find(correct_responses(:,12) > min_rt & correct_responses(:,12) < max_rt);
        
        reaction_times.(curr_var).(subj_id_ext) = correct_responses(indices_correct_rt_win,12);
        
        mean_reaction_time.(curr_var).(subj_id_ext) = mean(reaction_times.(curr_var).(subj_id_ext) );
        mean_reaction_time_coll.(curr_var)(iSubj) = mean(reaction_times.(curr_var).(subj_id_ext));
        
        
        nresponse_correct_rt_win = length(indices_correct_rt_win);
        detection_rate.(curr_var).(subj_id_ext)  = nresponse_correct_rt_win/nr_trials;
        detection_rate_coll.(curr_var)(iSubj)  = nresponse_correct_rt_win/nr_trials;
        
        nIncorr_responses = nr_trials - nresponse_correct_rt_win;
        
        fa_rate.(curr_var).(subj_id_ext)  = nIncorr_responses/nr_trials;
        fa_rate_coll.(curr_var)(iSubj)  = nIncorr_responses/nr_trials;
        
    end % subj loop
    
end

rtdiff_mean_coll.devL = mean_reaction_time_coll.devLcon - mean_reaction_time_coll.devLincon;

rtdiff_mean_coll.devR = mean_reaction_time_coll.devRcon - mean_reaction_time_coll.devRincon;

rtdiff_mean_coll.devLR = mean_reaction_time_coll.devLRcon - mean_reaction_time_coll.devLRincon;

output_filename = ['res_devL_devR_devLR_con_vs_incon_N' num2str(nSubjects)];
save(fullfile(res_path, output_filename),  'rtdiff_mean_coll', 'detection_rate_coll', 'fa_rate_coll', 'mean_reaction_time_coll' );







