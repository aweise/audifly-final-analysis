clear all global;
close all;

% init obob_ownft...
addpath('/mnt/obob/obob_ownft_old/');

cfg=[];
cfg.package.svs=true;
cfg.package.gm2 = true;
obob_init_ft(cfg);

addpath('/mnt/obob/staff/aweise/toolbox/cbrewer/');
addpath('/mnt/obob/staff/aweise/data/audifly/scripts/');
addpath('/mnt/obob/staff/aweise/templates/');

chooseBrainOption =  'parcel';

%% +++++++++++++++++++++++++++++++++++++++++++++

%    ------------------- FIX PARAMETER! --------------------------
logtransform = 'yes';
regfac = '5%'; 
chooseTFmethod = 'wavelet';
timeLimits = [0.2 0.6];
freqLimits = [8 14];
chooseHemisphere = 'bothHemis'; 
pValue = 0.05;
leadfieldNormOption =  'no';  
ica = true;
sss = true;
ssp = false;
choose_sensors =  'meggrad'; 
data_type =     'meg';
subj_ids = functions.get_subject_ids(data_type);
nSubjects = length(subj_ids);
contrastType = 'devL_devR'; 
analysisType =  'pow'
epoch = 'sound';  
avgovertime = 'yes'; 
avgoverfreq = 'yes';  
brainAreas = [];


statMethod = 'montecarlo'; % 'montecarlo',  'analytic'

if strcmp(statMethod, 'montecarlo')
    nRandomizations = 10000
    if nRandomizations ~= 10000
        warning(['number of randomization is set to: ' num2str(nRandomizations) ]);
    end
end


color_for_figures = 'brewermap'

%    ------------------- SPECIFY PARAMETER! --------------------------
chooseMinusAvgTimelock =  'induced'; %'induced';  'minusAvgTimelock';

%%
if strcmp(logtransform , 'yes')
    datapath = ['/mnt/obob/staff/aweise/data/audifly/data/meg/source/' chooseBrainOption '/' analysisType '/norm_' leadfieldNormOption '/' choose_sensors '/' chooseTFmethod '/logtrans_yes/'  ];
end

outPath = [datapath  'stat_ericmaris/' statMethod '/' num2str(nRandomizations) '/' ];

outPath = [outPath chooseMinusAvgTimelock '/' ];

if ~exist(outPath, 'dir')
    mkdir(outPath)
end

% create folder in which data will be stored if not existent
picsPath = [outPath '/pics/' color_for_figures '/'];
if ~exist(picsPath, 'dir')
    mkdir(picsPath)
end

contrastConds =  {'dev_left', 'dev_right' };
   

%% load data
inputfile = fullfile(outPath, ['data_stat_allN' num2str(nSubjects) '_sss_ica_' chooseHemisphere '_'  contrastType '_' choose_sensors '_' epoch '_' num2str(freqLimits(1)) '_' num2str(freqLimits(2)) 'Hz_'  num2str(timeLimits(1)) '_' num2str(timeLimits(2)) 's_avgTime' avgovertime '_avgFreq' avgoverfreq '.mat'] );
        
load(inputfile,'data_stat');


%% show sign channels
data_stat.label_posClus_chan
data_stat.label_negClus_chan 

data_stat.sort_posClus = data_stat.label;
data_stat.sort_posClus(:,2) = num2cell(data_stat.stat_posClus);

data_stat.sort_negClus = data_stat.label;
data_stat.sort_negClus(:,2) = num2cell(data_stat.stat_negClus);

%% show max and min values
maxVal = max(data_stat.stat_masked(data_stat.stat_masked>0)) % induced 5.1019; non-phase-locked:  4.8773

minVal = min(data_stat.stat_masked(data_stat.stat_masked<0)) % induced: -4.3395; non-phase-locked: -4.0921
 
 
%% plot cluster

% interpolate parcels on a mri
load parcellations_3mm.mat;
load('standard_mri_segmented');

%     load atlas
%     load atlas_parcel333.mat
%     atlas.tissuelabel = atlas.brick0label;
atlas = ft_read_atlas('/mnt/obob/obob_ownft/external/fieldtrip/template/atlas/aal/ROI_MNI_V4.nii'); % this atlas works nicely instead of parcellation atlas!


cfg = [];
cfg.sourcegrid = parcellation.parcel_grid;
cfg.parameter = 'stat_masked';
cfg.mri = mri_seg.bss; % A brain, scull, scalp segmentation
cfg.latency = timeLimits; % make sure to use the range in which sig cluster was found (check via multiplot)
cfg.frequency = freqLimits;% make sure to use the range in which sig cluster was found (check via multiplot)

data_parc_interpol = obob_svs_virtualsens2source(cfg, data_stat);
data_parc_interpol.coordsys = 'mni';
src = data_parc_interpol;


% Please note that I am loading standard_mri_segmented instead of standard_mri here.
% thomas created a segmented version of both the standard mri as well as the better standard mri.
% Both exist in two versions:
% mri_seg.bss: A brain, scull, scalp segmentation
% mri_seg.gwc: A gray matter, white matter and csf segmentation
% If you feed an mri like this to obob_svs_virtualsens2source, it will provide you with a
% new field called brain_mask which is a mask you can use when plotting like this:

if strcmp(statMethod,'montecarlo')
    src.mask=src.stat_masked.*(src.brain_mask > 0);
end

%% make ugly plot
cfg = [];
cfg.funparameter = 'stat_masked';
cfg.maskparameter = 'mask';
cfg.atlas = atlas;
% cfg.location  = 'max';
cfg.location  = 'min';

ft_sourceplot(cfg, src);

%% make nice source plot
close all;

picn_name = [ 'sourceplot_devL_devR_' statMethod '_' chooseHemisphere '_' num2str(freqLimits(1)) '_' num2str(freqLimits(2)) 'Hz_' num2str(timeLimits(1)) '_' num2str(timeLimits(2)) 's_sss' num2str(sss) '_ica' num2str(ica)];

dpi=300;
cfg = [];
if strcmp(color_for_figures, 'colorcetmap')
   cfg.funcolormap = colorcetmap; 
end
cfg.funparameter = 'stat_masked';
cfg.maskparameter = 'mask';
cfg.method =  'surface';
cfg.camlight = 'no';
cfg.projmethod = 'nearest'; %'project';


% (1) make figures for left hemisphere

cfg.surffile = 'surface_white_left.mat';
ft_sourceplot(cfg, src);
if strcmp(color_for_figures, 'brewermap')
   ft_hastoolbox('brewermap', 1);         % ensure this toolbox is on the path
   colormap(flipud(brewermap(64,'RdBu'))) % change the colormap
end

functions.plot_caret_style(fullfile(picsPath, [picn_name '_lefthem_ft_temporal']), dpi, [], 'left');
png_name = [picsPath picn_name '_lefthem_ft_temporal_left.png'];
eval(['print -dpng -r' num2str(300) ' ' png_name]);



%%
view(90,0);
camlight(camlight('left'), 'right');
png_name = [picsPath picn_name '_lefthem_ft_temporal_right.png'];
eval(['print -dpng -r' num2str(300) ' ' png_name]);

%%
close all;
cfg.surffile = 'surface_white_left.mat';
ft_sourceplot(cfg, src);
material([0.3,0.9,0.1,10,1]);
view(-74,10);
camlight(camlight('right'), 'left');
if strcmp(color_for_figures, 'brewermap')
   ft_hastoolbox('brewermap', 1);         % ensure this toolbox is on the path
   colormap(flipud(brewermap(64,'RdBu'))) % change the colormap
end
png_name = [picsPath picn_name '_lefthem_ft_occipital_left.png'];
eval(['print -dpng -r' num2str(300) ' ' png_name]);

%%
close all;
cfg.surffile = 'surface_white_left.mat';
ft_sourceplot(cfg, src);
material([0.3,0.9,0.1,10,1]);
view(74,10);
camlight(camlight('left'), 'right');
if strcmp(color_for_figures, 'brewermap')
   ft_hastoolbox('brewermap', 1);         % ensure this toolbox is on the path
   colormap(flipud(brewermap(64,'RdBu'))) % change the colormap
end
png_name = [picsPath picn_name '_lefthem_ft_occipital_right.png'];
eval(['print -dpng -r' num2str(300) ' ' png_name]);



%%
% (2) make figures for right hemisphere
close all;
cfg.surffile = 'surface_white_right.mat';
ft_sourceplot(cfg, src);
if strcmp(color_for_figures, 'brewermap')
   ft_hastoolbox('brewermap', 1);         % ensure this toolbox is on the path
   colormap(flipud(brewermap(64,'RdBu'))) % change the colormap
end
functions.plot_caret_style(fullfile(picsPath, [picn_name '_righthem_ft_temporal']), dpi, [], 'right');
png_name = [picsPath picn_name '_righthem_ft_temporal_right.png'];
eval(['print -dpng -r' num2str(300) ' ' png_name]);

%%
view(-90,0);

camlight(camlight('right'), 'left');
png_name = [picsPath picn_name '_righthem_ft_temporal_left.png'];
eval(['print -dpng -r' num2str(300) ' ' png_name]);

%%
close all
cfg.surffile = 'surface_white_right.mat';
ft_sourceplot(cfg, src);
if strcmp(color_for_figures, 'brewermap')
   ft_hastoolbox('brewermap', 1);         % ensure this toolbox is on the path
   colormap(flipud(brewermap(64,'RdBu'))) % change the colormap
end
material([0.3,0.9,0.1,10,1]);
view(74,10);
camlight(camlight('left'), 'right');
png_name = [picsPath picn_name '_righthem_ft_occipital_right.png'];
eval(['print -dpng -r' num2str(300) ' ' png_name]);

%%
close all
cfg.surffile = 'surface_white_right.mat';
ft_sourceplot(cfg, src);
if strcmp(color_for_figures, 'brewermap')
   ft_hastoolbox('brewermap', 1);         % ensure this toolbox is on the path
   colormap(flipud(brewermap(64,'RdBu'))) % change the colormap
end
material([0.3,0.9,0.1,10,1]);
view(-74,10);
camlight(camlight('right'), 'left');
png_name = [picsPath picn_name '_lefthem_ft_occipital_left.png'];
eval(['print -dpng -r' num2str(300) ' ' png_name]);



    
