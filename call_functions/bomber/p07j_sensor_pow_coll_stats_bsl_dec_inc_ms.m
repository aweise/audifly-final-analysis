clear all global;

% init obob_ownft...
addpath('/mnt/obob/obob_ownft/');

cfg=[];
obob_init_ft;

addpath('/mnt/obob/staff/aweise/data/audifly/scripts/');
addpath('/mnt/obob/staff/aweise/templates/');

load('aw_neuromag306layout.mat');

%% +++++++++++++++++++++++++++++++++++++++++++++

%    ------------------- FIX PARAMETER! --------------------------
logtransform = 'yes';

sss = true; 

chooseCycles = 5; 

chooseTFmethod = 'wavelet'; 

chooseMinusAvgTimelock= 'induced';

one_tailed_ttest = false;

freqLimits = [8 14];

timeLimits_bsl = [-0.4 -0.2];
timeLimits_dev = [0.2 0.6];

analysisType =  'pow'
epoch = 'sound';  
avgovertime ='no'; 
avgoverfreq = 'yes'; 

statMethod =    'montecarlo';

if strcmp(statMethod, 'montecarlo')
    nRandomizations = 10000
    if nRandomizations ~= 10000
        warning(['number of randomization is set to: ' num2str(nRandomizations) ]);
    end
end

choose_sensors =  'MEGGRAD'; 

sel_sensors_left =  {'MEG1842+1843','MEG1822+1823'}; 
sel_sensors_right =  { 'MEG2232+2233','MEG2212+2213' };

data_type = 'meg' ;
subj_ids = functions.get_subject_ids(data_type);
nSubjects = length(subj_ids);

%    ------------------- SPECIFY PARAMETER! --------------------------

xCond  =  'dev_left'; %  'dev_left' 'dev_right'

chooseROI = 'left'; % 'left', 'right'

if strcmp(chooseROI, 'left') 
    choose_sensors_stat =  sel_sensors_left;    
    chooseChannel ='MEG1842_1822';
elseif strcmp(chooseROI, 'right')
    chooseChannel ='MEG2232_2212';
    choose_sensors_stat = sel_sensors_right;
end

%%  +++++++++++++++++++++++++++++++++++++++++++++

if strcmp(logtransform,'yes')
    datapath = ['/mnt/obob/staff/aweise/data/audifly/data/meg/sensor/' analysisType '/all_sensors/' chooseTFmethod '/' num2str(chooseCycles) 'cycles/logtrans_yes/loglast/' ];
end

if strcmp(statMethod, 'montecarlo')
    outPath = [datapath  chooseMinusAvgTimelock  '/stat_ericmaris/' statMethod '/' num2str(nRandomizations) '/' ];
end

if one_tailed_ttest
    outPath = [outPath 'one_tailed/'];
else
    outPath = [outPath 'two_tailed/'];
end

picPath = [outPath 'pics'];
if ~exist(picPath, 'dir')
    mkdir(picPath)
end

singleplotName = ['singleplot_statN31_' xCond  '_bsl_' chooseROI '_' num2str(length(choose_sensors_stat)) 'chans_' num2str(freqLimits(1)) '_' num2str(freqLimits(2)) 'Hz_'  num2str(timeLimits_dev(1)) '_' num2str(timeLimits_dev(2)) 's_sss'];
   

%% collect data of all subj
data = cell(1, nSubjects); % prepares empty matrix

for iSub = 1:nSubjects
    
    % load individual power data (averaged over trials) for each condition into empty matrix
    subj = subj_ids{iSub};
    
    if sss
        inputfile = fullfile(datapath, [subj '_' epoch '_' xCond  '_sss.mat'] );
    end
    
    load(inputfile);
    
    % collect data of all subjs
    
     data{iSub} = freq_logpow;        
    
end

%% select data and create ft structure for baseline activity
for iSubj = 1:nSubjects
    
    % create ft structure for bsl data that contains the mean power of the bsl interval for each sample point present in the data_tmp structure
    cfg = [];
    cfg.latency = timeLimits_bsl ;
    cfg.frequency= freqLimits;
    cfg.avgovertime = 'yes';
    cfg.avgoverfreq ='yes';
    cfg.channel =  choose_sensors_stat;
    cfg.avgoverchan = 'yes';
    
    data_tmp_bsl{iSubj} = ft_selectdata(cfg, data{iSubj} );
    
    % select relevant data to use for cluster based permutation test
    cfg = [];
    cfg.latency = timeLimits_dev ;
    cfg.frequency= freqLimits;
    cfg.avgovertime = 'no';
    cfg.avgoverfreq ='yes';
    cfg.channel =  choose_sensors_stat;
    cfg.avgoverchan = 'yes';
    
    data_dev{iSubj} = ft_selectdata(cfg, data{iSubj} );
    
    data_bsl{iSubj} = data_dev{iSubj}; 
    length_timevect = length(data_dev{iSubj}.time); % define how many time points in data_dev
    
    % fill the mean bsl power value for each time point and the
    % corresponding channel to get a comparable ft structure as the dev
    % data
    for iTime = 1:length_timevect
        data_bsl{iSubj}.powspctrm(:,:,iTime) = data_tmp_bsl{iSubj}.powspctrm;
    end
    
    data_bsl{iSubj}.time = data_dev{iSubj}.time;
    
end


%% create design matrix based on number of subjects and number of conditions
% N x numobservations: design matrix (for examples/advice, please see the Fieldtrip wiki,
%                                     especially cluster-permutation tutorial and the 'walkthrough' design-matrix section)
% ivar = 2 %  conditions
% uvar = 1 %  subject groups
study_design = [1:nSubjects, 1:nSubjects; ones(1,nSubjects),ones(1,nSubjects)*2];

%% define parameters to analyse the different sensor types (for neuromag:
%  gradiometers and magnetometers)
if strcmp(choose_sensors,'MEGGRAD') == 1
    
    cfg_sensors = [];
    cfg_sensors.types =       'grad';
    cfg_sensors.neighbTemp =  'neuromag306cmb_neighb';
    cfg_sensors.channels =    'MEGGRAD';
    cfg_sensors.layout	=    'neuromag306cmb'; % layout; %
 
end

%% create neighbours (necessary if you specify cfg.correctm='cluster' for ft_freq_statistics)

cfg = [];
cfg.method        = 'template'; % 'distance', 'triangulation' or 'template';
cfg.template = cfg_sensors.neighbTemp; %'neuromag306mag_neighb' or 'neuromag30cmb_neighb'

neighbours = ft_prepare_neighbours(cfg); % neighbourhood structure, see FT_PREPARE_NEIGHBOURS;


%% prepare cfg for stats

cfg = [];
cfg.avgovertime = avgovertime;
cfg.avgoverfreq = avgoverfreq;
cfg.parameter   = 'powspctrm';
cfg.method      = 'montecarlo';
cfg.correctm    = 'cluster';
cfg.neighbours  = neighbours;
cfg.design      = study_design;
cfg.uvar                = 1; % unit variables, i.e. subject groups
cfg.ivar                = 2; % independent variables, i.e. conditions
cfg.clusteralpha     = 0.05;
cfg.clusterstatistic = 'maxsum';
cfg.alpha            = 0.05;
cfg.numrandomization = nRandomizations;
cfg.statistic   = 'depsamplesT';
cfg.tail        = 0;
cfg.correcttail      = 'prob';
cfg.clustertail        = 0;


%% run stat function
freq_stat = ft_freqstatistics(cfg, data_dev{:}, data_bsl{:});

%%  remove cfg otherwise stat becomes extremely large (> xx GB range)
freq_stat = rmfield(freq_stat, 'cfg');

%% plot it
if strcmp(avgovertime,'no')
    close all;
    figure(1);
    plot(freq_stat.time, squeeze(freq_stat.stat));
    hold on;
    plot(freq_stat.time, squeeze(freq_stat.mask), 'r');
    
    printfilename = fullfile(picPath, [singleplotName '.png']);
    print(printfilename, '-dpng');   
end

%% create folder in which data will be stored if not existent
if ~exist(outPath, 'dir')
    mkdir(outPath)
end

%% save data
if strcmp(analysisType,'pow')
    if sss
        outputfile = fullfile(outPath, ['freq_stat_allN' num2str(nSubjects) '_' xCond '_vs_bsl_roi_' chooseROI '_' epoch '_' num2str(freqLimits(1)) '_' num2str(freqLimits(2)) 'Hz_'  num2str(timeLimits_dev(1)) '_' num2str(timeLimits_dev(2)) 's_avgTimeno_avgFreqyes_sss.mat'] );
     end
end
save(outputfile,'freq_stat');
