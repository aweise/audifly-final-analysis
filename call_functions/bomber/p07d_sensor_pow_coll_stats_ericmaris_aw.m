
clear all global;

% init obob_ownft...
addpath('/mnt/obob/obob_ownft/');

cfg=[];
obob_init_ft;

addpath('/mnt/obob/staff/aweise/data/audifly/scripts/');
addpath('/mnt/obob/staff/aweise/templates/');

load('aw_neuromag306layout.mat');
%% +++++++++++++++++++++++++++++++++++++++++++++

%    ------------------- FIX PARAMETER! --------------------------
logtransform = 'yes';

sss = true; 

chooseCycles = 5; 

chooseTFmethod = 'wavelet'; 

contrastType =   'devL_devR'; 

freqLimits = [8 14];             
timeLimits = [0.2 0.6];

analysisType =  'pow'
epoch = 'sound';  
avgovertime ='no'; 
avgoverfreq = 'no';  

statMethod = 'montecarlo'

if strcmp(statMethod, 'montecarlo')
    nRandomizations = 10000
    if nRandomizations ~= 10000
        warning(['number of randomization is set to: ' num2str(nRandomizations) ]);
    end
end

choose_sensors =  'MEGGRAD'; % 'megmag' ,'meggrad'

choose_sensors_stat =  'MEGGRAD';% 'MEG_right'; %'MEG_left';%'MEG_right'; % 'MEG_left', 'MEG_right' 'MEGGRAD';%{'MEG1912+1913', 'MEG2012+MEG2013', 'MEG2042+2043'};

data_type = 'meg' ; 
subj_ids = functions.get_subject_ids(data_type);
nSubjects = length(subj_ids);

loglast = true; % do 1st combine planar and then do logtransform of power

%    ------------------- SPECIFY PARAMETER! --------------------------

chooseMinusAvgTimelock='induced'; % 'minusAvgTimelock' or 'induced' 

%%  +++++++++++++++++++++++++++++++++++++++++++++

if strcmp(logtransform, 'yes')
    if loglast
        datapath = ['/mnt/obob/staff/aweise/data/audifly/data/meg/sensor/' analysisType '/all_sensors/' chooseTFmethod '/' num2str(chooseCycles) 'cycles/logtrans_yes/loglast/' ];    
    end
end

if strcmp(statMethod, 'montecarlo')
    if strcmp(choose_sensors, choose_sensors_stat)
        outPath = [datapath  chooseMinusAvgTimelock  '/stat_ericmaris/' statMethod '/' num2str(nRandomizations) '/' ];
    end
end

if strcmp(contrastType, 'devL_devR') 
    contrastConds =  {'dev_left', 'dev_right' };
end


%% collect data of all subj
for iCond = 1:length(contrastConds)
    
    xCond = contrastConds{iCond};
    
    data = cell(1, nSubjects); % prepares empty matrix
    
    for iSub = 1:nSubjects
        
        % load individual power data (averaged over trials) for each condition into empty matrix
        subj = subj_ids{iSub};
        
        if sss
            inputfile = fullfile(datapath, [subj '_' epoch '_' xCond  '_sss.mat'] );
        end
        
        load(inputfile);
        
        % collect data of all subjs
        if strcmp(logtransform, 'yes')
            if strcmp(chooseMinusAvgTimelock, 'minusAvgTimelock')
                data_minus_timelockavg{1,iSub} =  freq_minus_timelock_logpow;
            elseif strcmp(chooseMinusAvgTimelock, 'induced')
                data{1,iSub} = freq_logpow;
            elseif strcmp(chooseMinusAvgTimelock, 'timelock')
                data_timelockavg{1,iSub} =  freq_timelock_logpow;
            end
        end
        
        
        if strcmp(chooseMinusAvgTimelock, 'minusAvgTimelock')
            contrast_conds.(xCond) = data_minus_timelockavg;
        elseif strcmp(chooseMinusAvgTimelock, 'induced')
            contrast_conds.(xCond) = data;
        elseif strcmp(chooseMinusAvgTimelock, 'timelock')
            contrast_conds.(xCond) = data_timelockavg;
        end
    end
end
contrast_conds

% normalize power
for iSubj = 1:nSubjects
    
    if strcmp(contrastType, 'devL_devR')
        
        if strcmp(logtransform, 'yes')
            
            devL_norm{iSubj} = contrast_conds.dev_left{iSubj};
            
            cfg = [];
            cfg.channel = choose_sensors;
            
            devL_norm{iSubj} = ft_selectdata(cfg,devL_norm{iSubj} );
            
            
            devR_norm{iSubj} = contrast_conds.dev_right{iSubj};
            
            cfg = [];
            cfg.channel = choose_sensors;
            
            devR_norm{iSubj} = ft_selectdata(cfg,devR_norm{iSubj} );
            
        
        end
    end
end




%% create design matrix based on number of subjects and number of conditions
% N x numobservations: design matrix (for examples/advice, please see the Fieldtrip wiki,
%                                     especially cluster-permutation tutorial and the 'walkthrough' design-matrix section)
% ivar = 2 %  conditions
% uvar = 1 %  subject groups
study_design = [1:nSubjects, 1:nSubjects; ones(1,nSubjects),ones(1,nSubjects)*2];

%% define parameters to analyse the different sensor types (for neuromag:
%  gradiometers and magnetometers)
if strcmp(choose_sensors,'MEGGRAD') == 1
    
    cfg_sensors = [];
    cfg_sensors.types =       'grad';
    cfg_sensors.neighbTemp =  'neuromag306cmb_neighb';
    cfg_sensors.channels =    'MEGGRAD';
    cfg_sensors.layout	=    'neuromag306cmb'; % layout; %
    
    
end

%% create neighbours (necessary if you specify cfg.correctm='cluster' for ft_freq_statistics)

cfg = [];
cfg.method        = 'template'; 
cfg.template = cfg_sensors.neighbTemp; 

neighbours = ft_prepare_neighbours(cfg); 


%% prepare cfg for stats
cfg = [];
cfg.channel = choose_sensors_stat;
cfg.latency = timeLimits;
cfg.frequency = freqLimits;
cfg.avgovertime = avgovertime;
cfg.avgoverfreq = avgoverfreq;
cfg.parameter   = 'powspctrm';
cfg.method      = 'montecarlo';
cfg.correctm    = 'cluster';
cfg.neighbours  = neighbours;
cfg.design      = study_design;
cfg.uvar                = 1; % unit variables, i.e. subject groups
cfg.ivar                = 2; % independent variables, i.e. conditions
cfg.clusteralpha     = 0.05;
cfg.clusterstatistic = 'maxsum';
cfg.alpha            = 0.05;
cfg.numrandomization = nRandomizations;
cfg.minnbchan        = 2;
cfg.statistic   = 'depsamplesT';
cfg.tail        = 0;
cfg.correcttail      = 'prob';     
cfg.clustertail        = 0;


%% run stat function
if strcmp(analysisType, 'pow')
    
    if strcmp(contrastType, 'devL_devR')
        freq_stat = ft_freqstatistics(cfg, devL_norm{:}, devR_norm{:});        
    end
    
end

%%  remove cfg otherwise stat becomes extremely large (> xx GB range)
freq_stat = rmfield(freq_stat, 'cfg');

%% create folder in which data will be stored if not existent
if ~exist(outPath, 'dir')
    mkdir(outPath)
end

%% create stat field in which only significant T values are stored
freq_stat.stat_masked = freq_stat.stat .* freq_stat.mask;

if isempty(freq_stat.stat_masked(:,:,:))
    warning('no significant cluster found')
end

% checkForSigNeg = find(freq_stat.stat_masked(:,:,:) < 0) ; % get indices of significant effect
checkForSigPos = find(freq_stat.stat_masked(:,:,:) > 0) ; % get indices of significant effect

if strcmp(statMethod,'montecarlo')
    
    if isfield(freq_stat, 'posclusters')
        posClusterPvalue = getfield(freq_stat.posclusters, 'prob')
        if posClusterPvalue <= 0.05
            freq_stat.stat_posClus = freq_stat.stat .* (freq_stat.posclusterslabelmat==1); % 1 marks the first cluster
        end
    end
    
    if isfield(freq_stat, 'negclusters')
        negClusterPvalue = getfield(freq_stat.negclusters, 'prob') % gives error message if field does not exist
        if negClusterPvalue <= 0.05
            freq_stat.stat_negClus = freq_stat.stat .* (freq_stat.negclusterslabelmat==1); % 1 marks the first cluster
        end
    end
end

%% save data
if strcmp(analysisType,'pow')
    if sss
        outputfile = fullfile(outPath, ['freq_stat_allN' num2str(nSubjects) '_' contrastType '_' epoch '_' num2str(freqLimits(1)) '_' num2str(freqLimits(2)) 'Hz_'  num2str(timeLimits(1)) '_' num2str(timeLimits(2)) 's_avgTime' avgovertime '_avgFreq' avgoverfreq '_sss.mat'] );
    end
end
% save(outputfile,'freq_stat');

