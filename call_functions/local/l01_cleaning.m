% data cleaning

% for each step (subj, block, channel, etc) you need to start srcipt from
% the top manually!

% output: artefacts

% NOTE: the preprocessing steps that are done here such as filtering, etc.
% are NOT stored as meg data! there will be no harm to the
% data; it simply serves the purpose of cleaning the data from bad/dead
% channels and massive artifacts that cannot be dealt with by the
% beamformer

% note that the beamformer will take care of train noise (16 to 17.3 Hz,
% eye blinks, heart beats, etc.)

clear all global;
close all;



%% init obob_ownft (from my local matlab!)...
% addpath('/mnt/obob/obob_ownft/');
addpath('/datadisk/uni/matlab_adds/obob_ownft/');
obob_init_ft;

addpath('/mnt/obob/staff/aweise/data/audifly/scripts/');

%% vars
% inpath_filt = '/mnt/obob/staff/aweise/data/audifly/data/meg/preproc/filtered/' 
inpath_filt = '/datadisk/uni/experiments/obob/audifly/data/meg/preproc/filtered/'

% outpath_artifacts = '/mnt/obob/staff/aweise/data/audifly/data/meg/preproc/artifacts/';
outpath_artifacts = '/datadisk/uni/experiments/obob/audifly/data/meg/preproc/artifacts/';

data_type = 'meg';
subj_ids = functions.get_subject_ids(data_type);
sss = true;
expert = true;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% define manually subject nr to clean:
iSub = 31        
subj_id = subj_ids{iSub}

% define manually block nr to clean and load data
block = 7 % of 7; change file number manually and then do 'run section'


if ~expert
    disp('******************************************************************');
    disp('*');
    warning('your input required');
    disp('*');
    disp('******************************************************************');
    prompt = 'If aplicable: Remove the manual_bad_channels in this script that you entered for the previous subject [y]: ';
    str = input(prompt,'s');
    if isempty(str)
        str = 'Y';
    end
    
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if sss
    infile_name = sprintf('file_%02d_sss.mat', block);
else
    infile_name = sprintf('file_%02d.mat', block);
end
load(fullfile(inpath_filt, subj_id, infile_name), 'data_filt');

% select channels (as chosen above) and filter data
cfg = [];
% cfg.channel = channels;
cfg.hpfilter = 'yes';
cfg.hpfreq = 1;
cfg.bsfilter = 'yes';
cfg.bsfreq = [16 17.3];
data_filt = ft_preprocessing(cfg,data_filt);

% exclude dead channels
data_without_deadchannels = functions.th_exclude_deadchannels(data_filt);

x = match_str(data_filt.label, data_without_deadchannels.label);
x_bool = true(size(data_filt.label));
x_bool(x) = false;

dead_channels = data_filt.label(x_bool);

% if we are at block > 1, load last artifact data to already exclude bad channels
known_bad_channels = dead_channels;
if block > 1
    if sss
        tmp_fname = sprintf('file%02d_%s_sss.mat', block-1, 'mag_grad');
    else
        tmp_fname = sprintf('file%02d_%s.mat', block-1, 'mag_grad');
    end
    tmp = load(fullfile(outpath_artifacts, subj_id, tmp_fname));
    
  known_bad_channels = unique(functions.concat_cellarrays(known_bad_channels, tmp.bad_channels));

end %if

% reject bad channels...
cfg = [];
cfg.channel = functions.make_channel_reject(known_bad_channels); % e.g. {'all','-MEG1312'}

data_filt = ft_preprocessing(cfg, data_filt);  

% do gaetan style search for removing bad channels....
% (1) arbitrary form chunks as obob_rejectvisual requires trial structure while
% you are working with continuous data
nbchunks = 20;% number of data chunks - in the current case about 15 sec
trldur   = data_filt.time{1}(end)/nbchunks-0.001;% duration of each data chunk (-1ms: ensure to get all the chunks, but this will cut [0.001*nbchunks]sec at the end of the dataset)

cfg = [];
cfg.length  = trldur;
cfg.overlap = 0;

data_chunked = ft_redefinetrial(cfg, data_filt);


% inspect only MEG channels (excluding bio channels such as eog and ecg)
% via obob_rejectvisual 
cfg = [];
cfg.channel = 'MEG';

data_sel_chans = ft_preprocessing(cfg, data_chunked);
reject_bad_channels = {};


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% look for bad MEG channels and write them manually down!...
% with the current settings you will see the MEG channels in butterfly mode (excluding bio channels). that is,
% channels are plotted on the top of each other (and eog and ecg are
% missing here as this is less optimal to identify bad MEG channels);
%
% also note that for each segment / 20-sec block that you are looking at the
% color of the individual channel changes;
% that is across data segments / 20-sec blocks the colors of one and the
% same channel may differ!

% IMPORTANT:
% if you manually detect bad MEG channels in this step via ft_databrowser, make
% sure to list them in "manual_bad_channels" (see below); if you are done with one of 
% many possible blocks of one single subject you may leave the bad channels
% from several blocks in here; however, make sure you delete
% these channels for the next subject!

% disp('******************************************************************');
% disp('*');
% warning('your input required');
% disp('*');
% disp('******************************************************************');
% prompt = 'Write down bad channels you may identify in the next step in manual_bad_channels![y]: ';
% str = input(prompt,'s');
% if isempty(str)
%     str = 'Y';
% end

% +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%
manual_bad_channels = {};
%
bad_channels = unique(functions.concat_cellarrays(known_bad_channels, manual_bad_channels, reject_bad_channels));

% +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

bio_chans = {'EOG001', 'EOG002', 'ECG003'};
bio_chans = bio_chans';
bad_chans_bio_chans = [bad_channels; bio_chans];

cfg = [];
cfg.channel = functions.make_channel_reject(bad_chans_bio_chans); % e.g. {'all' '-MEG1234'}
cfg.blocksize = 15;
cfg.plotevents = 'no';
cfg.gradscale      = 0.04;  % factor to compensate different scale between GRAD and MAG 
cfg.layout         = 'neuromag306mag.lay'; 
cfg.preproc.hpfreq   = 1;
cfg.preproc.hpfilter = 'yes'; 
% cfg.ylim             = [-5e-12 5e-12];
cfg.ylim             = [-1e-11 1e-11];

close all;
ft_databrowser(cfg, data_filt); 
% set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]); % get fullscreen figure of databrowser

% % make sure to not forget to write down the manually detected channels and apply the code to become effective
% disp('******************************************************************');
% disp('*');
% warning('your input required');
% disp('*');
% disp('******************************************************************');
% prompt = 'If applicable, manually enter bad channels in "manual_bad_channels" \n and run the code coated via "++++++++" again to make sure that those \n channels are stored for the corresponding block![y]: ';
% str = input(prompt,'s');
% if isempty(str)
%     str = 'Y';
% end
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% do automatic search for excessive amplitudes...
all_art = [];

cfg = [];
cfg.artfctdef.zvalue.channel = functions.make_channel_reject(bad_channels);
cfg.artfctdef.zvalue.cutoff = 60; % th: 60
cfg.artfctdef.zvalue.interactive = 'no';
cfg.artfctdef.zvalue.rectify = 'yes';
cfg.artfctdef.zvalue.bsfilter = 'yes';
cfg.artfctdef.zvalue.bsfreq = [8 12];
cfg.artfctdef.zvalue.artpadding = .2;

[cfg_art, artifact] = ft_artifact_zvalue(cfg, data_filt);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% browse through data to improve automatic artifact detection....
% mark the artifacts completely if this has not been automatically achieved; 
% make sure to not mark too much of the "clean" data as we want to keep as
% many data as possible for the analysis!
% if necessary mark artifacts that have not been identified automatically;
%
% NOTE 1: here the data contain the bio channels (EOG, ECG) - this is to
% better identify artifacts (recall: heart beat artifacts and eye artifacts
% do not need to be removed as the ica and beamformer will take care of it);
%
% NOTE 2: if you want to see the topo of certain data, you need to rely on
% the layout of magnetometer ('neuromag306all.lay') as in these data you have 
% gradiometers that are NOT combined and which is why it does not make sense 
% to look at their topo

% disp('******************************************************************');
% disp('*');
% warning('your input required');
% disp('*');
% disp('******************************************************************');
% prompt = 'Check the automatically detected artifacts and if necessary mark them properly![y]: ';
% str = input(prompt,'s');
% if isempty(str)
%     str = 'Y';
% end


cfg = [];
cfg.channel = functions.make_channel_reject(bad_channels);
cfg.blocksize = 15;
cfg.plotevents = 'no';
% cfg.megscale       = 1;
cfg.gradscale      = 0.04; % factor to compensate different scale between GRAD and MAG 
cfg.eogscale       = 1e-7; % 5e-8;
cfg.ecgscale       = 1e-8;
cfg.layout         = 'neuromag306mag.lay'; % only for topo plot needed
cfg.preproc.hpfreq   = 1;
cfg.preproc.hpfilter = 'yes'; 
cfg.ylim             = [-1e-11 1e-11];
cfg.artfctdef.zvalue.artifact = artifact; 
cfg.selectfeature = 'zvalue';
cfg.continuous = 'yes';
% cfg.viewmode    = 'vertical'; 
% cfg.channelclamped = {'EOG001', 'EOG002', 'ECG003'}; % clamped channels at the end of the list - relevant in view mode 'vertical'

close all;
cfg_all_art = ft_databrowser(cfg, data_filt);
% set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]); % get fullscreen figure of databrowser

% create new artifact structure...
all_art.artfctdef.(sprintf('ex_amps_%s', 'mag_grad')) = cfg_all_art.artfctdef.zvalue;
% cfg_all_art.artfctdef.zvalue.artifact =[]

% save...
if sss
    outfile_name = sprintf('file%02d_%s_sss.mat', block, 'mag_grad');
end
mkdir(fullfile(outpath_artifacts, subj_id));
save(fullfile(outpath_artifacts, subj_id, outfile_name), 'all_art', 'bad_channels', 'subj_id', 'block');
disp('******************************************************************');
disp('*');
disp('artifacts and bad channels have been saved');
disp('*');
disp('******************************************************************');



  