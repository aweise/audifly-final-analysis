%% clear...
clear all global
close all

%% init obob_ownft...
addpath('/mnt/obob/obob_ownft/');
cfg = [];
obob_init_ft(cfg);

addpath('/mnt/obob/staff/aweise/data/audifly/scripts/');


%% define vars
headshape_path = '/mnt/obob/staff/aweise/data/audifly/data/polhemus/'; % data stored in this folder obtained via p08_source_extract_grad_headshape
mri_path = '/mnt/obob/staff/aweise/data/audifly/data/mri/';
coregistration_path = '/mnt/obob/staff/aweise/data/audifly/data/coregistration/';
mkdir(coregistration_path);

data_type = 'meg';
subj_ids = functions.get_subject_ids(data_type); 
subj_id =   '19831031krab'% subj_ids{1}; % '19831031krab'

headshape_name = ['headshape_' subj_id '.mat']; % recorded via polhemus
load([headshape_path headshape_name], 'grad', 'headshape');

mri_file = functions.rdir(fullfile('/mnt/obob/staff/aweise/data/audifly/data/mri/', sprintf('%s*', subj_id), '*.nii')); %  or - if applicable - use 'co*.nii'
if strcmp(subj_id,'xx') 
    mri_file = functions.rdir(fullfile('/mnt/obob/staff/aweise/data/audifly/data/mri/', sprintf('%s*', subj_id), '*.dcm')); 
    warning('This is a MRI recorded in Trento. For this MRI left is right and right is left! So, make sure to mark the left and right fiducial at the appropriate positions.');
end

if isempty(mri_file)
    mri_filename = [];
    sss=true;
    block = 01;
    fif_name = functions.get_fif_names( subj_id, block, sss );
    %warning('You need to work on the bomber! When you coregister without individual MRI, you need to use as input for cfg.headshape the full path of the raw data (*.fif)');
else
    mri_filename = mri_file.name;    
    mri_data = ft_read_mri(mri_filename);
end

%% do co-registration
cfg = [];
if isempty(mri_file)
    cfg.headshape = fif_name;
else
    cfg.headshape = headshape; % not working via local windows: 'O:\mnt\sinuhe\data_raw\aw_audifly\subject_subject\180403\19800616mrgu_block01.fif';
end
cfg.mrifile = mri_filename;
cfg.sens = grad;

[mri_aligned, headshape, hdm, mri_segmented] = obob_coregister(cfg);

mri_aligned = ft_convert_units(mri_aligned,'m');    
headshape = ft_convert_units(headshape,'m');  
hdm = ft_convert_units(hdm,'m');  
mri_segmented = ft_convert_units(mri_segmented,'m');   

% Outputs:
%%%%%%%%%%
% mri_aligned   : mri structure co-registered to MEG space
%                         or final morphed MRI (in case of non individual MRI available)
% shape            : headshape (without outliers)
% hdm              : single shell volume conductor model
% mri_segmented : mriF segmented.
       
% save....
save(fullfile(coregistration_path, sprintf('%s_%s.mat', 'coregister', subj_id)), 'grad', 'mri_aligned', 'mri_segmented', 'headshape', 'hdm');