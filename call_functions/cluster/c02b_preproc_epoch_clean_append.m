
% (C) A. Weise

%% clear workspace
clear all global;
close all;

%% init obob_ownft...
addpath('/mnt/obob/obob_ownft');

cfg = [];
cfg.package.hnc_condor = true;

obob_init_ft(cfg);

%% define paths
addpath('/mnt/obob/staff/aweise/data/audifly/scripts/');

%% analyses for all subjects with good MEG data 
data_type = 'meg';
subj_ids = functions.get_subject_ids(data_type); 
% subj_ids = subj_ids(1:5);

ica =  true;
if ica
    sss = {true}; % true or false
    ssp = {false}; % per definition false (for now)
else
    sss = { true};  % false or true
    ssp = {false};  % false or true
end
createWhichFiles = 'createAllFiles' ; % 'createAllFiles' or 'createMissingFiles' 
    
% ******************************************************

if ica
    outpath = '/mnt/obob/staff/aweise/data/audifly/data/meg/preproc/clean_ica/';
else
    outpath = '/mnt/obob/staff/aweise/data/audifly/data/meg/preproc/clean/';
end

logpath = [outpath 'log/'];
if ~exist( 'logpath', 'dir')
    mkdir(logpath)
end

%% configure the job cluster
cfg = [];
cfg.mem = '5G';        % The amount of RAM required to run one job in the
%                         jobcluster. In "T", "G", "M" or "K".
cfg.adust_mem = true;   % default option;  job that gets held
%                         because it exceeded its requested memory will be
%                         resubmitted automatically after a couple of minutes
%                         with updated memory requirements
cfg.jobsdir = logpath;

condor_struct = obob_condor_create(cfg);

%% add jobs, i.e. call function: qnt_02_cluster_recenter_append
condor_struct = obob_condor_addjob_cell(condor_struct, 'cluster_functions.p02b_epoch_clean_append', createWhichFiles, subj_ids, ica, sss, ssp, outpath);

%% submit job
obob_condor_submit(condor_struct);






