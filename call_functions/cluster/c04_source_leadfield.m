% (C) A. Weise

%% clear workspace
clear all global;
close all;

%% init obob_ownft...
addpath('/mnt/obob/obob_ownft');

cfg = [];
cfg.package.hnc_condor = true;

obob_init_ft(cfg);

%% define paths
addpath('/mnt/obob/staff/aweise/data/audifly/scripts/');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
createWhichFiles = 'createAllFiles'; % 'createMissingFiles', 'createAllFiles'

sss =  true; 
ica = true; 
chooseNormalizeOption = {'no'};  
chooseBrainOption = { 'parcel'} 
chooseSensors = {'meggrad'}; 
data_type = 'meg'; 
subj_ids = functions.get_subject_ids(data_type); 
epochType = {'sound'}; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
logpath = '/mnt/obob/staff/aweise/data/audifly/data/meg/source/log/leadfield/';
  
if ~exist( 'logpath', 'dir')
    mkdir(logpath)
end

% ******************************************************

%% configure the job cluster
cfg = [];
cfg.mem = '4G';        % The amount of RAM required to run one job in the
%                         jobcluster. In "T", "G", "M" or "K".
cfg.adust_mem = true;   % default option;  job that gets held
%                         because it exceeded its requested memory will be
%                         resubmitted automatically after a couple of minutes
%                         with updated memory requirements
cfg.jobsdir = logpath;

condor_struct = obob_condor_create(cfg);

%% add jobs, i.e. call function: xyc
condor_struct = obob_condor_addjob_cell(condor_struct, 'cluster_functions.s02_subjLeadfield',  createWhichFiles, subj_ids, ica, sss, chooseNormalizeOption, chooseSensors, chooseBrainOption, epochType);
                                                                                                                                                       
%% submit job
obob_condor_submit(condor_struct);






