% (C) A. Weise

%% clear workspace
clear all global;
close all;

%% init obob_ownft...
addpath('/mnt/obob/obob_ownft');

cfg = [];
cfg.package.hnc_condor = true;

obob_init_ft(cfg);

%% define paths
addpath('/mnt/obob/staff/aweise/data/audifly/scripts/');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
logtransform = 'yes';

 regfac = { '5%'};

createWhichFiles = 'createAllFiles'; % 'createMissingFiles'; 'createAllFiles';

chooseBrainOption = {'parcel'};
data_type = 'meg';
subj_ids = functions.get_subject_ids(data_type);
% subj_ids = subj_ids(1);

leadfieldNormOption = {  'no' }; 

ica  = true; 
sss = true; 

if sss
    ssp = false; % per default false 
else
    ssp = false; 
end

choose_sglTrl = {'no'}; 

choose_sensors = { 'meggrad'}; 

chooseTFmethod = {'wavelet'};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
logpath = '/mnt/obob/staff/aweise/data/audifly/data/meg/source/log/pow/';
 
if ~exist( logpath, 'dir')
    mkdir(logpath)
end

% ******************************************************

%% configure the job cluster
cfg = [];
cfg.mem = '18G';        % The amount of RAM required to run one job in the
%                         jobcluster. In "T", "G", "M" or "K".
cfg.adust_mem = true;   % default option;  job that gets held
%                         because it exceeded its requested memory will be
%                         resubmitted automatically after a couple of minutes
%                         with updated memory requirements
cfg.jobsdir = logpath;

condor_struct = obob_condor_create(cfg);

%% add jobs, i.e. call function: xyc
condor_struct = obob_condor_addjob_cell(condor_struct, 'cluster_functions.s03b_svs_lcmv_pow', subj_ids, chooseBrainOption,createWhichFiles, leadfieldNormOption, ica, sss, ssp, choose_sensors,chooseTFmethod,regfac,choose_sglTrl,logtransform);
                                                                                                                          
%% submit job
obob_condor_submit(condor_struct);






