% (C) A. Weise

%% clear workspace
clear all global;
close all;

%% init obob_ownft...
addpath('/mnt/obob/obob_ownft');

cfg = [];
cfg.package.hnc_condor = true;

obob_init_ft(cfg);

%% define paths
addpath('/mnt/obob/staff/aweise/data/audifly/scripts/');
outpath = '/mnt/obob/staff/aweise/data/audifly/data/meg/preproc/filtered/';

logpath = [outpath 'log/'];
if ~exist( 'logpath', 'dir')
    mkdir(logpath)
end

%% analyses for all subjects with good MEG data 
create_which_files = 'createAllFiles'; % 'createAllFiles' or 'createMissingFiles'
data_type = 'meg';
sss = true;  
subj_ids = functions.get_subject_ids(data_type); 
            
n_blocks =   {1 2 3 4 5 6 7}; 

% NOTE: subj_ids = {'19901106mrgb'}; n_blocks =   {4}; too many bad
% channels - no maxfiltering possible for that block

% ******************************************************

%% configure the job cluster
cfg = [];
cfg.mem = '3.5G';        % The amount of RAM required to run one job in the
%                         jobcluster. In "T", "G", "M" or "K".
cfg.adust_mem = true;   % default option;  job that gets held
%                         because it exceeded its requested memory will be
%                         resubmitted automatically after a couple of minutes
%                         with updated memory requirements
cfg.jobsdir = logpath;

condor_struct = obob_condor_create(cfg);

%% add jobs, i.e. call function: qnt_02_cluster_recenter_append
condor_struct = obob_condor_addjob_cell(condor_struct, 'cluster_functions.p01_filter', subj_ids, n_blocks, create_which_files, sss, outpath);

%% submit job
obob_condor_submit(condor_struct);













